// var loadJSSSSY = function(implementationCode){
//     var scriptTag = document.createElement('script');
//     scriptTag.src = 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js';
//     scriptTag.onload = implementationCode;
//     scriptTag.onreadystatechange = implementationCode;
//     document.head.appendChild(scriptTag);
// };

// var loadCORS = function(){
//     var scriptTag = document.createElement('script');
//     scriptTag.src = 'https://gist.githubusercontent.com/leovarmak/956b1c33e65543ea095c07471c737640/raw/7e3f1e772dee5c17d200fad06a01db5869f9f29a/jquery.ajax-cross-origin.min.js';
//     document.head.appendChild(scriptTag);
// }

// window.jQuery || loadJSSSSY();
// loadCORS();

var mapSource = [
    {
        from: "One Piece Episode of East Blue Luffy and His 4 Crewmate's Big Adventure",
        imdb: '',
        season: 0,
        to: 'ONE PIECE: EPISODE OF EAST BLUE',
        sources: []
    },
    {
        from: '',
        imdb: 'tt3878998',
        season: 0,
        to: 'Dragon Ball Z-',
        sources: []
    },
    {
        from: '',
        imdb: 'tt0214341',
        season: 0,
        to: 'Dragon Ball Z',
        sources: []
    },
    {
        from: '',
        imdb: 'tt4644488',
        season: 0,
        to: 'Dragon Ball Super',
        sources: []
    },
    {
        from: '',
        imdb: 'tt7961060',
        season: 0,
        to: 'Dragon Ball Super Movie: Broly',
        sources: []
    },
    {
        from: '',
        imdb: 'tt1409055',
        season: 0,
        to: 'Dragon Ball Kai 2014',
        sources: []
    },
    {
        from: 'Birds of Prey (and the Fantabulous Emancipation of One Harley Quinn)',
        imdb: '',
        season: 0,
        to: 'Birds of Prey',
        sources: []
    },
    {
        from: '',
        imdb: 'tt0112175',
        season: 0,
        to: 'Spider-Man: The Animated Series',
        sources: []
    },
    {
        from: 'Naruto Shippūden',
        imdb: '',
        season: 0,
        to: 'Naruto Shippuuden',
        sources: [
            'https://gogoanime.pro/anime/naruto-shippuden-dub-00zr'
        ]
    },
    {
        from: 'Naruto Shippuden the Movie',
        imdb: '',
        season: 0,
        to: 'Naruto: Shippuuden Movie 1',
        sources: []
    },
    {
        from: 'Naruto Shippuden the Movie: Bonds',
        imdb: '',
        season: 0,
        to: 'Naruto: Shippuuden Movie 2',
        sources: []
    },
    {
        from: 'Naruto Shippuden the Movie: The Will of Fire',
        imdb: '',
        season: 0,
        to: 'Naruto: Shippuuden Movie 3',
        sources: []
    },
    {
        from: 'Naruto Shippuden the Movie: The Lost Tower',
        imdb: '',
        season: 0,
        to: 'Naruto: Shippuuden Movie 4',
        sources: []
    },
    {
        from: 'Naruto Shippuden the Movie: Blood Prison',
        imdb: '',
        season: 0,
        to: 'Naruto: Shippuuden Movie 5',
        sources: []
    },
    {
        from: 'Naruto Shippuden the Movie: Road to Ninja',
        imdb: '',
        season: 0,
        to: 'Naruto: Shippuuden Movie 6',
        sources: []
    },
    {
        from: 'The Last: Naruto the Movie',
        imdb: '',
        season: 0,
        to: 'Naruto: Shippuuden Movie 7',
        sources: []
    },
    {
        from: '',
        imdb: 'tt4508902',
        season: 2,
        to: 'One Punch Man 2nd Season',
        sources: []
    },
    {
        from: '',
        imdb: 'tt5626028',
        season: 1,
        to: 'Boku no Hero Academia',
        sources: []
    },
    {
        from: '',
        imdb: 'tt5626028',
        season: 2,
        to: 'Boku no Hero Academia 2nd Season',
        sources: []
    },
    {
        from: '',
        imdb: 'tt5626028',
        season: 3,
        to: 'Boku no Hero Academia 3rd Season',
        sources: []
    },
    {
        from: '',
        imdb: 'tt5626028',
        season: 4,
        to: 'Boku no Hero Academia 4th Season',
        sources: []
    },
    {
        from: '',
        imdb: 'tt7745068',
        season: 0,
        to: 'Boku no Hero Academia the Movie: Futari no Hero',
        sources: []
    },
    {
        from: '',
        imdb: 'tt11589858',
        season: 0,
        to: 'Boku no Hero Academia: All Might - Rising The Animation',
        sources: []
    },
    {
        from: '',
        imdb: 'tt11107074',
        season: 0,
        to: 'My Hero Academia: Heroes Rising',
        sources: [
            'https://gogoanime.pe/boku-no-hero-academia-the-movie-2-heroesrising',
            'https://gogoanime.pe/boku-no-hero-academia-the-movie-2-heroesrising-dub',
            'https://gogoanime.pro/anime/boku-no-hero-academia-the-movie-2-heroesrising-dub-7611',
        ]
    },
    {
        from: '',
        imdb: 'tt2098220',
        season: 0,
        to: '',
        sources: [
            'https://gogoanime.pe/hunter-x-hunter-2011',
            'https://gogoanime.pe/hunter-x-hunter-dub'
        ]
    },
    {
        from: '',
        imdb: 'tt0426719',
        season: 0,
        to: '',
        sources: [
            'https://gogoanime.pe/hunter-x-hunter'
        ]
    },
    {
        from: '',
        imdb: 'tt2918988',
        season: 0,
        to: '',
        sources: [
            'https://gogoanime.pe/hunter-x-hunter-phantom-rouge',
            'https://gogoanime.pe/hunter-x-hunter-movie-1-phantom-rouge-dub'
        ]
    },
    {
        from: '',
        imdb: 'tt3198698',
        season: 0,
        to: '',
        sources: [
            'https://gogoanime.pe/hunter-x-hunter-movie-2-the-last-mission-dub',
            'https://gogoanime.pe/hunter-x-hunter-the-last-mission'
        ]
    },
    {
        from: '',
        imdb: 'tt4774186',
        season: 0,
        to: '',
        sources: [
            'https://gogoanime.pe/megaman-x-the-day-of-sigma-dub',
            'https://gogoanime.pe/megaman-maverick-hunter-x-the-day-of-sigma'
        ]
    },
    {
        from: '',
        imdb: 'tt0159175',
        season: 5,
        to: 'LUPIN III: PART 5',
        sources: []
    },
    {
        from: '',
        imdb: 'tt0159175',
        season: 4,
        to: 'Lupin III: Part IV',
        sources: []
    },
    {
        from: '',
        imdb: 'tt0159175',
        season: 3,
        to: 'LUPIN III SERIES 3',
        sources: []
    },
    {
        from: '',
        imdb: 'tt0159175',
        season: 2,
        to: 'Lupin III Series 2',
        sources: []
    },
    {
        from: '',
        imdb: 'tt0159175',
        season: 1,
        to: 'Lupin III',
        sources: []
    },
    {
        from: '',
        imdb: 'tt7814574',
        season: 0,
        to: 'ITO JUNJI: COLLECTION',
        sources: []
    },
    {
        from: '',
        imdb: 'tt3469050',
        season: 0,
        to: '90 Day Fiance',
        sources: []
    },
    {
        from: '',
        imdb: 'tt12228334',
        season: 0,
        to: 'El Prince',
        sources: []
    },
    {
        from: 'Tom and Jerry The Classic Collection',
        imdb: '',
        season: 1,
        to: '',
        sources: [
            'https://solarmovie.mom/solarmovie/tom-and-jerry-complete-classic-collection-32BB/',
            'https://cmovies.ac/film/tom-and-jerry-complete-classic-collection'
        ]
    },
    {
        from: '',
        imdb: 'tt6357658',
        season: 2,
        to: 'Baki',
        sources: [
            'https://srsone.top/show/baki-s03e02/season/3'
        ]
    },
    {
        from: '',
        imdb: 'tt6357658',
        season: 0,
        to: 'Baki',
        sources: []
    },
    {
        from: '',
        imdb: 'tt0417299',
        season: 3,
        to: 'Avatar: The Last Airbender',
        sources: [
            'https://srsone.top/show/avatar-the-last-airbender-s03e20/season/3',
            'https://5movies.to/tv/avatar-the-last-airbender-303/',
            'https://cmovies.ac/film/avatar-the-last-airbender-book-3-fire-kiv',
            'https://solarmovie.mom/solarmovie/avatar-the-last-airbender-book-3-fire-30CA/'
        ]
    },
    {
        from: '',
        imdb: 'tt0417299',
        season: 2,
        to: 'Avatar: The Last Airbender',
        sources: [
            'https://srsone.top/show/avatar-the-last-airbender-s02e19/season/2',
            'https://5movies.to/tv/avatar-the-last-airbender-303/',
            'https://cmovies.ac/film/avatar-the-last-airbender-book-2-earth-oks',
            'https://solarmovie.mom/solarmovie/avatar-the-last-airbender-book-2-earth-30Cz/'
        ]
    },
    {
        from: '',
        imdb: 'tt0417299',
        season: 1,
        to: 'Avatar: The Last Airbender',
        sources: [
            'https://srsone.top/show/avatar-the-last-airbender-s01e11/season/1',
            'https://5movies.to/tv/avatar-the-last-airbender-303/',
            'https://cmovies.ac/film/avatar-the-last-airbender-book-1-water-hxj',
            'https://solarmovie.mom/solarmovie/avatar-the-last-airbender-book-1-water-30Cy/'
        ]
    },
    {
        from: '',
        imdb: 'tt0345219',
        season: 0,
        to: 'Sex and zen II',
        sources: [
            'https://tinyzonetv.to/watch-movie/watch-sex-and-zen-ii-1996-free-6588'
        ]
    },
    {
        from: '',
        imdb: 'tt0133196',
        season: 0,
        to: 'Sex and zen 3',
        sources: [
            'https://ww4.0123movie.net/movie/18-sex-and-zen-3-16836.html'
        ]
    },
    {
        from: '',
        imdb: 'tt1365048',
        season: 0,
        to: '3-D Sex and Zen: Extreme Ecstasy',
        sources: [
            'https://tinyzonetv.to/movie/watch-3d-sex-and-zen-extreme-ecstasy-2011-free-14070',
            'https://fmovies.to/film/3d-sex-and-zen-extreme-ecstasy.0455',
            'https://ww4.0123movie.net/movie/18-3d-sex-and-zen-extreme-ecstasy-16837.html'
        ]
    },
    {
        from: '',
        imdb: 'tt11645760',
        season: 1,
        to: '',
        sources: [
            'https://solarmovie.mom/solarmovie/digimon-adventure-2020-season-1-35FZ/',
            'https://123movies.autos/film/digimon-adventure-2020-season-1',
            'https://srsone.top/show/digimon-adventure-s01e04/season/1'
        ]
    },
    {
        from: 'Through Night and Day',
        imdb: 'tt9279666',
        season: 0,
        to: 'Through Night and Day',
        sources: [
            'https://pinoymoviess.com/watch-through-night-and-day-2018/'
        ]
    },
    {
        from: '',
        imdb: 'tt7718110',
        season: 1,
        to: 'Hightown',
        sources: [],
        ignores: /(srsone\.top)/ig
    },
    {
        from: '',
        imdb: 'tt1996447',
        season: 0,
        to: '',
        sources: [
            'https://www.dailymotion.com/video/xpkgt7'
        ]
    },
    {
        from: '',
        imdb: 'tt2846410',
        season: 0,
        to: '',
        sources: [
            'https://www.dailymotion.com/video/x28m2i4'
        ]
    },
    {
        from: '',
        imdb: 'tt9799984',
        season: 0,
        to: '',
        sources: [
            'https://www.dailymotion.com/video/x7cbl5a'
        ]
    },
    {
        from: '',
        imdb: 'tt0666194',
        season: 0,
        to: '',
        sources: [
            'https://www.dailymotion.com/video/x5rik0j'
        ]
    },
    {
        from: '',
        imdb: 'tt3058844',
        season: 0,
        to: '',
        sources: [
            'https://www.dailymotion.com/video/x75hf08'
        ]
    },
    {
        from: '',
        imdb: 'tt0070908',
        season: 0,
        to: '',
        sources: [
            'https://www.dailymotion.com/video/x2446yg'
        ]
    },
    {
        from: '',
        imdb: 'tt2339980',
        season: 0,
        to: 'Kaijudo: Rise of the Duel Masters',
        sources: []
    },
    {
        from: 'One Day At A Time',
        imdb: 'tt5339440',
        season: 4,
        to: 'One Day At A Time',
        sources: [
            'https://123movies.autos/film/one-day-at-a-time-season-4-2020'
        ]
    },
    {
        from: 'One Day At A Time',
        imdb: 'tt0072554',
        season: 3,
        to: 'One Day At A Time',
        sources: [
            'https://123movies.autos/film/one-day-at-a-time-season-31984'
        ]
    },
    {
        from: 'One Day At A Time',
        imdb: 'tt0072554',
        season: 2,
        to: 'One Day At A Time',
        sources: [
            'https://123movies.autos/film/one-day-at-a-time-season-21984'
        ]
    },
    {
        from: 'One Day At A Time',
        imdb: 'tt0072554',
        season: 1,
        to: 'One Day At A Time',
        sources: [
            'https://123movies.autos/film/one-day-at-a-time-season-11984'
        ]
    },
    {
        from: 'Gate',
        imdb: 'tt4958580',
        season: 1,
        to: 'Gate',
        sources: [
            'https://gogoanime.pro/anime/gate-dub-oply',
            'https://gogoanime.pro/anime/gate-pj4'
        ]
    },
    {
        from: 'Gate',
        imdb: 'tt4958580',
        season: 2,
        to: 'Gate',
        sources: [
            'https://gogoanime.pro/anime/gate-dub-070r',
            'https://gogoanime.pro/anime/gate-rj6n'
        ]
    },
    {
        from: '',
        imdb: 'tt2348803',
        season: 1,
        to: '',
        sources: [
            'https://gogoanime.pro/anime/kurokos-basketball-nw8',
            'https://www.gogoanime1.com/watch/kuroko-no-basket',
            'https://gogoanime.pe/kuroko-no-baske'
        ]
    },
    {
        from: '',
        imdb: 'tt2348803',
        season: 2,
        to: '',
        sources: [
            'https://gogoanime.pro/anime/kurokos-basketball-2-o48',
            'https://www.gogoanime1.com/watch/kuroko-no-basket-2',
            'https://gogoanime.pe/kuroko-basuke-2'
        ]
    },
    {
        from: '',
        imdb: 'tt2348803',
        season: 3,
        to: '',
        sources: [
            'https://gogoanime.pro/anime/kurokos-basketball-3-pw6',
            'https://www.gogoanime1.com/watch/kuroko-no-basket-3',
            'https://gogoanime.pe/kuroko-no-basket-3rd-season'
        ]
    },
    {
        from: '',
        imdb: 'tt5607616',
        season: 1,
        to: '',
        sources: [
            'https://gogoanime.pro/anime/rezero-starting-life-in-another-world-jv78',
            'https://gogoanime.pro/anime/rezero-starting-life-in-another-world-dub-jq9n',
            'https://www.gogoanime1.com/watch/rezero-kara-hajimeru-isekai-seikatsu',
            'https://gogoanime.pe/rezero-kara-hajimeru-isekai-seikatsu'
        ]
    },
    {
        from: '',
        imdb: 'tt5607616',
        season: 2,
        to: '',
        sources: [
            'https://gogoanime.pro/anime/rezero-kara-hajimeru-isekai-seikatsu-2nd-season-2n75',
            'https://gogoanime.pro/anime/rezero-kara-hajimeru-isekai-seikatsu-2nd-season-dub-p9p6',
            'https://www.gogoanime1.com/watch/rezero-kara-hajimeru-isekai-seikatsu-2nd-season',
            'https://gogoanime.pe/rezero-kara-hajimeru-isekai-seikatsu-2nd-season'
        ]
    },
    {
        from: '',
        imdb: 'tt12423004',
        season: 0,
        to: '',
        sources: [
            'https://www.dailymotion.com/video/x7vsdvc'
        ]
    },
    {
        from: 'Haikyuu!!',
        imdb: 'tt3398540',
        season: 1,
        to: 'Haikyuu!!',
        sources: [
            'https://gogoanime.pro/anime/haikyu-rjqn',
            'https://gogoanime.pro/anime/haikyu-dub-8mvo',
            'https://www.gogoanime1.com/watch/haikyuu',
            'https://gogoanime.pe/haikyuu'
        ]
    },
    {
        from: 'Haikyuu!!',
        imdb: 'tt3398540',
        season: 2,
        to: 'Haikyuu!!',
        sources: [
            'https://gogoanime.pro/anime/haikyu-2nd-season-nrk',
            'https://gogoanime.pro/anime/haikyu-2nd-season-dub-l9j3',
            'https://www.gogoanime1.com/watch/haikyuu-second-season',
            'https://gogoanime.pe/haikyuu-second-season'
        ]
    },
    {
        from: 'Haikyuu!!',
        imdb: 'tt3398540',
        season: 3,
        to: 'Haikyuu!!: Karasuno Koukou VS Shiratorizawa Gakuen',
        sources: [
            'https://gogoanime.pro/anime/haikyu-3rd-season-dub-0jj7',
            'https://gogoanime.pro/anime/haikyu-3rd-season-8yw3',
            'https://www.gogoanime1.com/watch/haikyuu-third-season',
            'https://gogoanime.pe/haikyuu-karasuno-koukou-vs-shiratorizawa-gakuen-koukou'
        ]
    },
    {
        from: 'Haikyuu!!',
        imdb: 'tt3398540',
        season: 4,
        to: 'Haikyuu!!: To the Top',
        sources: [
            'https://gogoanime.pro/anime/haikyuu-to-the-top-5q8m',
            'https://www.gogoanime1.com/watch/haikyuu-to-the-top',
            'https://gogoanime.pe/haikyuu-to-the-top'
        ]
    },
    {
        from: '',
        imdb: 'tt3894916',
        season: 1,
        to: '',
        sources: [
            'https://gogoanime.pro/anime/terraformars-ll56',
            'https://gogoanime.pro/anime/terraformars-dub-x9zv',
            'https://www.gogoanime1.com/watch/terra-formars',
            'https://gogoanime.pe/terra-formars'
        ]
    },
    {
        from: '',
        imdb: 'tt3894916',
        season: 2,
        to: '',
        sources: [
            'https://gogoanime.pro/anime/terra-formars-revenge-72p1',
            'https://gogoanime.pro/anime/terra-formars-revenge-dub-2n4q',
            'https://www.gogoanime1.com/watch/terra-formars-revenge',
            'https://gogoanime.pe/terra-formars-revenge'
        ]
    },
    {
        from: '',
        imdb: 'tt2560140',
        season: 1,
        to: 'Attack on Titan',
        sources: [
            'https://gogoanime.pro/anime/attack-on-titan-dub-m3zv',
            'https://gogoanime.pro/anime/attack-on-titan-kww',
            'https://www.gogoanime1.com/watch/shingeki-no-kyojin',
            'https://gogoanime.pe/shingeki-no-kyojin'
        ]
    },
    {
        from: '',
        imdb: 'tt2560140',
        season: 2,
        to: 'Attack on Titan',
        sources: [
            'https://gogoanime.pro/anime/attack-on-titan-season-2-dub-q9jv',
            'https://gogoanime.pro/anime/attack-on-titan-season-2-3v16',
            'https://www.gogoanime1.com/watch/shingeki-no-kyojin-season-2',
            'https://gogoanime.pe/shingeki-no-kyojin-season-2'
        ]
    },
    {
        from: '',
        imdb: 'tt2560140',
        season: 3,
        to: 'Attack on Titan',
        sources: [
            'https://gogoanime.pro/anime/attack-on-titan-season-3-dub-3n8y',
            'https://gogoanime.pro/anime/attack-on-titan-season-3-rvym',
            'https://www.gogoanime1.com/watch/shingeki-no-kyojin-season-3-part-2',
            'https://gogoanime.pe/shingeki-no-kyojin-season-3'
        ]
    },
    {
        from: 'Kaiji',
        imdb: '',
        season: 1,
        to: 'Kaiji: Ultimate Survivor',
        sources: [
            'https://gogoanime.pro/anime/kaiji-ultimate-survivor-w57l',
            'https://www.gogoanime1.com/watch/kaiji-ultimate-survivor',
            'https://gogoanime.pe/gyakkyou-buraikaiji-ultimate-survivor'
        ]
    },
    {
        from: 'Kaiji',
        imdb: '',
        season: 2,
        to: 'Kaiji: Against All Rules',
        sources: [
            'https://gogoanime.pro/anime/kaiji-against-all-rules-xv7w',
            'https://www.gogoanime1.com/watch/kaiji-2',
            'https://gogoanime.pe/kaiji-season-2'
        ]
    },
    {
        from: '',
        imdb: 'tt7441658',
        season: 0,
        to: '',
        sources: [
            'https://gogoanime.pro/anime/black-clover-v2k6',
            'https://gogoanime.pro/anime/black-clover-dub-2y44',
            'https://www.gogoanime1.com/watch/black-clover-tv',
            'https://gogoanime.pe/black-clover-tv'
        ]
    },
    {
        from: '',
        imdb: 'tt7591766',
        season: 0,
        to: '',
        sources: [
            'https://gogoanime.pro/anime/uq-holder-79qj',
            'https://gogoanime.pro/anime/uq-holder-dub-kn16',
            'https://www.gogoanime1.com/watch/uq-holder-mahou-sensei-negima-2',
            'https://gogoanime.pe/uq-holder-mahou-sensei-negima-2'
        ]
    },
    {
        from: '',
        imdb: 'tt3646944',
        season: 0,
        to: '',
        sources: [
            'https://gogoanime.pro/anime/attack-on-titan-crimson-bow-and-arrow-lyk3',
            'https://www.gogoanime1.com/watch/shingeki-no-kyojin-movie-1-guren-no-yumiya',
            'https://gogoanime.pe/shingeki-no-kyojin-movie-1-guren-no-yumiya'
        ]
    },
    {
        from: '',
        imdb: 'tt3646946',
        season: 0,
        to: '',
        sources: [
            'https://gogoanime.pro/anime/attack-on-titan-wings-of-freedom-x8oz',
            'https://www.gogoanime1.com/watch/shingeki-no-kyojin-movie-2-jiyuu-no-tsubasa',
            'https://gogoanime.pe/shingeki-no-kyojin-movie-2-jiyuu-no-tsubasa'
        ]
    },
    {
        from: '',
        imdb: 'tt7941892',
        season: 0,
        to: '',
        sources: [
            'https://gogoanime.pro/anime/shingeki-no-kyojin-movie-3-kakusei-no-houkou-wpx4',
            'https://www.gogoanime1.com/watch/shingeki-no-kyojin-movie-3-kakusei-no-houkou',
            'https://gogoanime.pe/shingeki-no-kyojin-movie-3-kakusei-no-houkou'
        ]
    },
    {
        from: '',
        imdb: 'tt0285331',
        season: 9,
        to: '',
        sources: [
            'https://solarmovie.mom/solarmovie/24-season-9-live-another-day-30KA/',
            'https://cmovies.ac/film/24-season-9-live-another-day-qpy'
        ]
    },
    {
        from: '',
        imdb: 'tt2703720',
        season: 1,
        to: '',
        sources: [
            'https://gogoanime.pro/anime/my-teen-romantic-comedy-snafu-j83',
            'https://gogoanime.pro/anime/my-teen-romantic-comedy-snafu-dub-wxm6',
            'https://www.gogoanime1.com/watch/yahari-ore-no-seishun-love-come-wa-machigatteiru',
            'https://gogoanime.pe/yahari-ore-no-seishun-love-comedy-wa-machigatteiru'
        ]
    },
    {
        from: '',
        imdb: 'tt2703720',
        season: 2,
        to: '',
        sources: [
            'https://gogoanime.pro/anime/my-teen-romantic-comedy-snafu-too-1rwp',
            'https://gogoanime.pro/anime/my-teen-romantic-comedy-snafu-too-dub-0jk3',
            'https://www.gogoanime1.com/watch/yahari-ore-no-seishun-love-comedy-wa-machigatteiru-zoku',
            'https://gogoanime.pe/yahari-ore-no-seishun-love-comedy-wa-machigatteiru-zoku'
        ]
    },
    {
        from: '',
        imdb: 'tt2703720',
        season: 3,
        to: '',
        sources: [
            'https://gogoanime.pro/anime/my-teen-romantic-comedy-snafu-climax-3o26',
            'https://gogoanime.pro/anime/my-teen-romantic-comedy-snafu-climax-dub-052k',
            'https://www.gogoanime1.com/watch/yahari-ore-no-seishun-love-comedy-wa-machigatteiru-kan'
        ]
    },
    {
        from: '',
        imdb: 'tt9058134',
        season: 1,
        to: '',
        sources: [
            'https://gogoanime.pro/anime/kengan-ashura-dub-mk47',
            'https://gogoanime.pro/anime/kengan-ashura-8513',
            'https://www.gogoanime1.com/watch/kengan-ashura',
            'https://gogoanime.pe/kengan-ashura'
        ]
    },
    {
        from: '',
        imdb: 'tt9058134',
        season: 2,
        to: '',
        sources: [
            'https://gogoanime.pro/anime/kengan-ashura-2nd-season-0j63',
            'https://gogoanime.pro/anime/kengan-ashura-2nd-season-dub-1k8w',
            'https://gogoanime.pe/kengan-ashura-2nd-season'
        ]
    },
    {
        from: '',
        imdb: 'tt1355237',
        season: 0,
        to: 'Glenn Martin DDS',
        sources: [
        ]
    },
    {
        from: '',
        imdb: 'tt1161684',
        season: 2,
        to: '',
        sources: [
            'https://gogoanime.pro/anime/rosario-vampire-capu2-prw6',
            'https://gogoanime.pro/anime/rosario-vampire-capu2-dub-4xvo',
            'https://gogoanime.pe/rosario-vampire-capu2',
            'https://www.gogoanime1.com/watch/rosario-vampire-capu2'
        ]
    },
    {
        from: '',
        imdb: 'tt0161220',
        season: 0,
        to: 'Air Bud 3: World Pup',
        sources: [
        ]
    }, {
        from: '',
        imdb: 'tt0994314',
        season: 2,
        to: '',
        sources: [
            'https://gogoanime.pro/anime/code-geass-lelouch-of-the-rebellion-r2-3x5r',
            'https://gogoanime.pro/anime/code-geass-lelouch-of-the-rebellion-r2-dub-6vp',
            'https://gogoanime.pe/-code-geass-lelouch-of-the-rebellion-r2',
            'https://gogoanime.pe/code-geass-lelouch-of-the-rebellion-r2-dub',
            'https://www.gogoanime1.com/watch/code-geass-hangyaku-no-lelouch-r2'
        ]
    }, {
        from: '',
        imdb: 'tt2086830',
        season: 0,
        to: '',
        sources: [
            'https://gogoanime.pro/anime/the-king-of-the-pigs-o0r5'
        ]
    }, {
        from: 'House of Anubis: The Touchstone of Ra',
        imdb: '',
        season: 0,
        to: '',
        sources: [
            'https://fmovies.to/film/house-of-anubis-touchstone-of-ra.vv244'
        ]
    }, {
        from: '',
        imdb: 'tt5768632',
        season: 0,
        to: 'Ask Laftan Anlamaz',
        sources: []
    }, {
        from: '',
        imdb: 'tt3741634',
        season: 1,
        to: 'Tokyo Ghoul',
        sources: [
            'https://gogoanime.pro/anime/tokyo-ghoul-7w06',
            'https://gogoanime.pro/anime/tokyo-ghoul-dub-x28z',
            'https://gogoanime.pe/tokyo-ghoul',
            'https://www.gogoanime1.com/watch/tokyo-ghoul'
        ]
    }, {
        from: '',
        imdb: 'tt3741634',
        season: 2,
        to: 'Tokyo Ghoul √A',
        sources: [
            'https://gogoanime.pro/anime/tokyo-ghoul-a-dub-jwln',
            'https://gogoanime.pro/anime/tokyo-ghoul-a-oq8',
            'https://gogoanime.pe/tokyo-ghoul-root-a-dub'
        ]
    }, {
        from: '',
        imdb: 'tt3741634',
        season: 3,
        to: 'Tokyo Ghoul:re',
        sources: [
            'https://gogoanime.pro/anime/tokyo-ghoulre-dub-l97z',
            'https://gogoanime.pro/anime/tokyo-kushure-2mp2',
            'https://gogoanime.pe/tokyo-ghoulre',
            'https://www.gogoanime1.com/watch/tokyo-ghoulre'
        ]
    }, {
        from: '',
        imdb: 'tt3741634',
        season: 4,
        to: 'Tokyo Ghoul:re 2nd Season',
        sources: [
            'https://gogoanime.pe/tokyo-ghoulre-2nd-season',
            'https://www.gogoanime1.com/watch/tokyo-ghoulre-2nd-season'
        ]
    }, {
        from: '',
        imdb: 'tt8484942',
        season: 0,
        to: 'India\'s Most Wanted',
        sources: [
            'https://www.hindilinks4u.to/indias-most-wanted-2019/'
        ]
    },
    {
        from: '',
        imdb: 'tt0850642',
        season: 0,
        to: 'Shaggy and Scooby-Doo Get a Clue!',
        sources: [
            'https://tinyzonetv.to/tv/watch-shaggy-and-scoobydoo-get-a-clue-2006-free-35160'
        ]
    }, {
        from: '',
        imdb: 'tt1751305',
        season: 2,
        to: 'Oreimo',
        sources: [
            'https://gogoanime.pro/anime/oreimo-2-p5x',
            'https://gogoanime.pe/oreimo-2'
        ]
    }, {
        from: '',
        imdb: 'tt12853970',
        season: 0,
        to: 'CRAYON SHIN CHAN',
        sources: [
            'https://gogoanime.pro/anime/shin-chan-dub-m36z',
            'https://gogoanime.pro/anime/shin-chan-y38j',
            'https://gogoanime.pe/crayon-shin-chan'
        ]
    }, {
        from: 'The Daily Life of the Immortal King',
        imdb: '',
        season: 1,
        to: 'The Daily Life of the Immortal King',
        sources: [
            'https://gogoanime.pro/anime/the-daily-life-of-the-immortal-king-v6n4',
            'https://gogoanime.pe/xian-wang-de-richang-shenghuo'
        ]
    }, {
        from: '',
        imdb: 'tt7069210',
        season: 0,
        to: '',
        sources: [
            'https://cmovies.online/film/the-conjuring-the-devil-made-me-do-it-2021',
            'https://5movies.cloud/film/the-conjuring-the-devil-made-me-do-it-2021-41745/',
            'https://solarmovie.mom/solarmovie/the-conjuring-the-devil-made-me-do-it-2021-36JW/',
            'https://123movies.autos/film/the-conjuring-the-devil-made-me-do-it-2021',
            'https://ww5.0123movie.net/movie/the-conjuring-the-devil-made-me-do-it-1621869065.html',
            'https://ww2.123moviess.sc/film/the-conjuring-the-devil-made-me-do-it-2021/',
            'https://lookmovie.io/movies/view/7069210-conjuring-sous-lx27emprise-du-diable-2021'
        ]
    }
];

var readyTimer = setInterval(function () {
    if (window.jQuery) {
        clearInterval(readyTimer);
        (function ($) {
            var settings = {};
            var streamlist = [];
            var queue = 20;
            $.fn.botStream = async function (options) {
                if (window.location.href.indexOf('/abc/') > 0) {
                    return;
                };
                var hosts = [];

                settings = $.extend({}, $.fn.botStream.options, options);
                var movieInfo = {};

                if (typeof jsinterface != 'undefined') {
                    settings.platform = 'android';
                    queue = 10;
                } else if (typeof window != 'undefined' && typeof window.webkit != 'undefined' && window.webkit && window.webkit.messageHandlers) {
                    settings.platform = 'ios';
                } else {
                    settings.platform = 'none';
                }

                if (settings.platform === 'none') {
                    movieInfo = $.extend({}, $.fn.botStream.movieInfo);
                    movieInfo.title = movieInfo.title.replace(new RegExp('&#x27;', 'ig'), '\'').replace(new RegExp('&#39;', 'ig'), '"');
                    movieInfo.sources = [];
                } else {
                    movieInfo = $.extend({}, $.fn.botStream.movieInfo);
                    movieInfo.title = movieInfo.title.replace(new RegExp('&#x27;', 'ig'), '\'').replace(new RegExp('&#39;', 'ig'), '"');
                    movieInfo.sources = [];
                };
                for (var i = 0; i < mapSource.length; i++) {
                    if ((mapSource[i].from.length > 0 && mapSource[i].from == movieInfo.title && mapSource[i].imdb.length == 0) ||
                        (mapSource[i].imdb.length > 0 && mapSource[i].imdb == movieInfo.filmImdb && (mapSource[i].season <= 0 || mapSource[i].season == movieInfo.season))) {
                        movieInfo.title = mapSource[i].to;
                        movieInfo.sources = mapSource[i].sources;
                        if (mapSource[i].hasOwnProperty('ignores')) {
                            movieInfo.ignores = mapSource[i].ignores;
                        };

                        if (mapSource[i].season > 0) {
                            movieInfo.episodeNumberSeasons = movieInfo.episodeNumber;
                        };
                        break;
                    }
                }

                watchdog.start();
                settings.processing = true;
                settings.shouldPlayValue = jQuery.inArray('DVDRip', $.fn.botStream.quality) * 9 + 9;

                $.each($.fn.botStream.imdbSources, function (index, source) {
                    if (movieInfo.hasOwnProperty('filmImdb')) {
                        try {
                            source.getStream(movieInfo, libs);
                        } catch (error) {

                        }
                    }
                });

                if (!movieInfo.hasOwnProperty('yearFirstSeason')) {
                    movieInfo.yearFirstSeason = 0;
                }
                movieInfo.genres = ['all'];
                movieInfo.languages = ['all'];
                movieInfo.country = '';
                movieInfo.originalTitle = '';
                movieInfo.sourceURL = '';

                if (movieInfo.hasOwnProperty('filmImdb') &&
                    movieInfo.filmImdb != null &&
                    movieInfo.filmImdb.length > 0) {

                    var imdbURL = 'https://www.imdb.com/title/' + movieInfo.filmImdb + '/';
                    var imdbHTML = await libs.postHTML({
                        url: imdbURL,
                        headers: {
                            'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                        },
                        type: 'afn'
                    });

                    if (movieInfo.show == true) {
                        var txt = $(imdbHTML).find('.ipc-inline-list a[href*=releaseinfo]:eq(0)').text().trim();
                        var parts = null;
                        if (txt.indexOf('–') > 0) {
                            parts = txt.split('–');
                        } else {
                            parts = txt.split('â€“');
                        };
                        if (parts.length > 0) {
                            try {
                                var year = new Date(parts[0]).getFullYear();
                                year = parseInt(year);
                                if (!isNaN(year)) {
                                    movieInfo.yearFirstSeason = year;
                                }
                            } catch (error) {

                            }
                        }
                    } else {
                        // var year = 0;
                        // var releaseInfo = $(imdbHTML).find('.title_wrapper > .subtext > a[href*="/releaseinfo"]').text().trim();
                        // if(releaseInfo.length > 0) {
                        //     year = new Date(releaseInfo).getFullYear();
                        // };
                        // if(year == 0) {
                        //     year = $(imdbHTML).find('h1 > #titleYear > a').text().trim();
                        //     if(year.length > 0){
                        //         year = parseInt(year);
                        //     }
                        // }
                        // if(!isNaN(year)) {
                        //     movieInfo.year = year;
                        // }
                    };
                    $(imdbHTML).find('.ipc-chip-list > a[href*=genres]').each(function (index, item) {
                        movieInfo.genres.push($(item).text().trim().toLowerCase());
                    });
                    $(imdbHTML).find('.ipc-inline-list a[href*=primary_language]').each(function (index, item) {
                        movieInfo.languages.push($(item).text().trim().toLowerCase());
                    });
                    $(imdbHTML).find('.ipc-inline-list a[href*=country_of_origin]').each(function (index, item) {
                        movieInfo.country = $(item).text().trim().toLowerCase();
                        return true;
                    });

                    if ($.inArray('arabic', movieInfo.languages) >= 0) {
                        //https://api.themoviedb.org/3/find/tt11051580?api_key=418683b63d78129a2e5ee06465e2a67a&language=en-US&external_source=imdb_id
                        try {
                            var tmdbURL = 'https://api.themoviedb.org/3/find/' + movieInfo.filmImdb + '?api_key=418683b63d78129a2e5ee06465e2a67a&language=en-US&external_source=imdb_id';
                            var json = await libs.getAjax(tmdbURL, 'get', {}, {});
                            // var tmdbURL = 'https://api.themoviedb.org/3/find/' + movieInfo.filmImdb + '?api_key=418683b63d78129a2e5ee06465e2a67a&language=en-US&external_source=imdb_id';
                            // var tmdbJSON = await libs.postHTML({
                            //     url: tmdbURL,
                            //     type: 'afn'
                            // });
                            // var json = JSON.parse(tmdbJSON);
                            if (json.movie_results.length > 0) {
                                movieInfo.originalTitle = json.movie_results[0].original_title;
                            } else if (json.tv_results.length > 0) {
                                movieInfo.originalTitle = json.tv_results[0].original_name;
                            };
                        } catch (error) {

                        }
                    };
                };
                $.each($.fn.botStream.providers, function (index, item) {
                    if (movieInfo.title.length > 0) {
                        try {
                            if (movieInfo.hasOwnProperty('ignores') && movieInfo.ignores.exec(item.DOMAIN) != null) {
                                return true;
                            }
                            if ($.inArray(item.GENRE, movieInfo.genres) >= 0 &&
                                (
                                    (item.hasOwnProperty('LANGUAGE') == true && $.inArray(item.LANGUAGE, movieInfo.languages) >= 0) ||
                                    (item.hasOwnProperty('COUNTRY') == true && item.COUNTRY == movieInfo.country)
                                )) {
                                item.getHosts(movieInfo, libs);
                                if (movieInfo.originalTitle.length == 0) {
                                    if (movieInfo.title.indexOf(' and ') > 0) {
                                        var tmp = {};
                                        Object.assign(tmp, movieInfo);
                                        tmp.title = movieInfo.title.replace(' and ', ' & ');
                                        tmp.filmImdb = '';
                                        item.getHosts(tmp, libs);
                                    } else if (movieInfo.title.indexOf(' & ') > 0) {
                                        var tmp = {};
                                        Object.assign(tmp, movieInfo);
                                        tmp.title = movieInfo.title.replace(' & ', ' and ');
                                        tmp.filmImdb = '';
                                        item.getHosts(tmp, libs);
                                    }
                                }
                            }
                        } catch (error) {

                        }
                    }
                });

                $.each($.fn.botStream.providers, function (index, item) {
                    // console.log(item.DOMAIN, libs.generateServerName(item.DOMAIN));
                    movieInfo.sources.some(function (source) {
                        try {
                            if (source.indexOf(item.DOMAIN) >= 0) {
                                var tmp = {};
                                Object.assign(tmp, movieInfo);
                                tmp.sourceURL = source;
                                item.getHosts(tmp, libs);
                            }
                        } catch (error) {

                        }
                    });
                });

                setInterval(function () {
                    if ($.fn.botStream.hostStreams.length > 0 && watchdog.count() < queue) {
                        watchdog.watch('cheat_watchdog');
                        var hostStream = $.fn.botStream.hostStreams.splice(0, 1)[0];
                        for (var i = 0; i < hosts.length; i++) {
                            if (hosts[i] === hostStream.url) {
                                return true;
                            }
                        };

                        hosts.push(hostStream.url);

                        $.fn.botStream.hosts.some(function (host) {
                            if (hostStream.url != undefined && hostStream.url.indexOf(host.DOMAIN) >= 0) {
                                hostStream.filmImdb = movieInfo.filmImdb;
                                hostStream.title = movieInfo.title,
                                    hostStream.year = movieInfo.year,
                                    hostStream.yearFirstSeason = movieInfo.yearFirstSeason,
                                    hostStream.show = movieInfo.show,
                                    hostStream.season = movieInfo.season,
                                    hostStream.episodeNumber = movieInfo.episodeNumber,
                                    hostStream.episodeImdb = movieInfo.episodeImdb
                                try {
                                    host.getStream(hostStream, libs);
                                } catch (error) {

                                }
                            }
                        })
                    } else {
                        watchdog.unwatch('cheat_watchdog');
                    }
                }, 300);
            };

            $.fn.botStream.quality = [
                'CAM',
                'CAMRip',
                'CAM-Rip',
                'TS',
                'TS-SCREENER',
                'HDTS',
                'TELESYNC',
                'PDVD',
                'PreDVDRip',
                'WP',
                'WORKPRINT',
                'TC',
                'HDTC',
                'TELECINE',
                'PPV',
                'PPVRip',
                'SCR',
                'SCREENER',
                'DVDSCR',
                'DVDSCREENER',
                'BDSCR',
                'DDC',
                'R5',
                'R5.LINE',
                'R5.AC3.5.1.HQ',

                'Blu-Ray',
                'BluRay',
                'Blu Ray',
                'BDRip',
                'BRip',
                'BRRip',
                'BDMV',
                'BDR',
                'BD25',
                'BD50',
                'BD5',
                'BD9',

                'DVDRip',
                'DVDMux',
                'DVDR',
                'DVD-Full',
                'DVD',
                'Full-Rip',
                'ISO rip',
                'lossless rip',
                'untouched rip',
                'DVD-5',
                'DVD-9',
                'SDTV',
                'DSR',
                'DSRip',
                'SATRip',
                'DTHRip',
                'DVBRip',
                'HDTV',
                'PDTV',
                'DTVRip',
                'TVRip',
                'HDTVRip',
                'VODRip',
                'VODR',
                'VOD',
                'WEBDL',
                'WEB DL',
                'WEB-DL',
                'WEB-DLRip',
                'WEBRip',
                'WEB Rip',
                'WEB-Rip',
                'WEB',
                'WEB-Cap',
                'WEBCAP',
                'WEB Cap',
                'HC',
                'SDRip',
                'SD',
                'HDRip',
                'HD Rip',
                'HD-Rip',
                'HD',
                'HD-DUB',
            ];

            $.fn.botStream.options = {
                useragents: [
                    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36',
                    'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 Safari/537.36',
                    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.246',
                    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36',
                    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36 OPR/76.0.4017.154',
                    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/601.3.9 (KHTML, like Gecko) Version/9.0.2 Safari/601.3.9',
                    'Mozilla/5.0 (X11; CrOS x86_64 8172.45.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.64 Safari/537.36',
                    'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:15.0) Gecko/20100101 Firefox/15.0.1'
                ],
                providers: $.fn.botStream.providers,
                proxy: window.location.href.replace(new RegExp('/$', 'ig'), '') + '/abc/',
                // proxy: 'http://127.0.0.1:8200/abc/',
                headers: {
                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36'
                }
            };

            var timeoutSeconds = 90000;
            var processlist = {};
            var watchdog = {
                count: function () {
                    return Object.keys(processlist).length;
                },
                watch: function (streamURL) {
                    var key = streamURL.replace(new RegExp('[ ]', 'ig'), '-');
                    processlist[key] = 0;
                },
                unwatch: function (streamURL) {
                    var key = streamURL.replace(new RegExp('[ ]', 'ig'), '-');
                    delete processlist[key];
                },
                start: function () {
                    var processing = false;
                    var timer = setInterval(function () {
                        if (!jQuery.isEmptyObject(processlist)) {
                            processing = true;
                        } else {
                            if (processing == true && jQuery.isEmptyObject(processlist)) {
                                clearInterval(timer);
                                var subtimer = setInterval(function () {
                                    if (jQuery.isEmptyObject(processlist)) {
                                        clearInterval(subtimer);
                                        libs.callbackJSON({ stop: true, time: 3000 });
                                    }
                                }, 3000)
                            }
                        }
                    }, 100);

                    setTimeout(function () {
                        if (processing == false && jQuery.isEmptyObject(processlist)) {
                            clearInterval(timer);
                            libs.callbackJSON({ stop: true, time: 5000 });
                        }
                    }, 5000)

                    setTimeout(function () {
                        clearInterval(timer);
                        libs.callbackJSON({ stop: true, time: timeoutSeconds });
                    }, timeoutSeconds)
                }
            };

            var libs = {
                currentTime: function (name) {
                    var d = new Date();
                    console.log(name, d.getTime());
                },
                postMsg: async function (url, type, resolution, quality, speed, cast, realdebrid, headers) {
                    if (url.length == 0) {
                        return;
                    }
                    url = url.trim();
                    for (var i = 0; i < streamlist.length; i++) {
                        if (streamlist[i] === url) {
                            return
                        }
                    };
                    streamlist.push(url);

                    var res = this.getResolution(url);
                    if (typeof resolution !== 'undefined' && resolution != null && parseInt(resolution) > 0) {
                        res = resolution;
                    };
                    var ql = {
                        name: 'UNK',
                        value: 0
                    };
                    if (typeof quality !== 'undefined' && quality != null) {
                        ql = quality;
                    };
                    var sp = 3;
                    if (typeof speed !== 'undefined' && speed != null && speed > 0) {
                        var spt = this.getSpeed(url);
                        if (spt > 0) {
                            sp = spt;
                        } else {
                            sp = speed;
                        }
                    };
                    var hds = settings.headers;
                    if (typeof headers !== 'undefined' && headers != null && headers) {
                        hds = headers;
                    };
                    var ct = true;
                    if (typeof cast !== 'undefined' && cast != null) {
                        ct = cast;
                    };

                    var json = {
                        stream: this.refillURL(url.trim()).replace(new RegExp('<', 'ig'), '%3C'),
                        type: type,
                        resolution: parseInt(res),
                        quality: ql.name,
                        cast: ct,
                        speed: parseInt(sp),
                        header: hds,
                        realdebrid: realdebrid
                    };

                    if (json.header.hasOwnProperty('Referer')) {
                        var domain = json.header.Referer;
                        json.server = this.generateServerName(domain);
                    } else {
                        json.server = 'unksr';
                    };
                    json.server = json.server.toUpperCase();

                    var sortValue = ql.value * 5 + json.speed;
                    if (json.resolution == 360) {
                        sortValue += 1;
                    } else if (json.resolution == 480) {
                        sortValue += 2;
                    } else if (json.resolution == 720) {
                        sortValue += 4;
                    } else if (json.resolution == 1080) {
                        sortValue += 3;
                    };
                    if (sortValue >= settings.shouldPlayValue) {
                        json.shouldPlay = true;
                        json.sortValue = sortValue;
                    } else {
                        json.shouldPlay = false;
                        json.sortValue = sortValue;
                    };

                    this.callbackJSON(json);
                },
                callbackJSON: function (json) {
                    if (settings.processing == false) {
                        return;
                    };
                    if (json.hasOwnProperty('stop') && json.stop == true) {
                        settings.processing = false;
                    };

                    if (settings.platform === 'ios') {
                        window.webkit.messageHandlers.callbackHandler.postMessage(json);
                    } else if (settings.platform === 'android') {
                        jsinterface.getSomeString(JSON.stringify(json));
                    } else {
                        this.currentTime('stream at');
                        window.flutter_inappwebview.callHandler("jsinterface", json);
                    }
                },
                unpack: function (val) {
                    var depack = function (p) {
                        if (p != "") {
                            c = unescape(p);
                            var _e = eval,
                                s = "eval=function(v){c=v;};" + c + ";eval=_e;";
                            eval(s)
                        } else {
                            c = p
                        };
                        return c
                    };
                    var p = val,
                        c = p;
                    var a = 10,
                        x = 1;
                    while (x < a) {
                        c = unescape(c);
                        if (/eval\(+function\(/.test(c)) {
                            c = depack(c);
                            x++
                        } else {
                            break
                        }
                    };
                    c = unescape(c);
                    return c
                },
                generateServerName(text) {
                    text = text.replace(new RegExp('(.*)//(.*?)/(.*){0,1}', 'gi'), '$2').replace(/\./ig, '').replace(/w/ig, '');
                    var result = text.substring(0, 3);
                    var sub = text.substring(3);
                    var max = 3;
                    var tmp = '';
                    if (sub.length > max) {
                        var mod = sub.length % max;
                        if (mod == 0) {
                            mod = 1;
                        }
                        for (i = 0; i < sub.length && i + 1 < sub.length; i = i + mod) {
                            tmp += sub.substring(i, i + 1);
                            if (tmp.length >= max) {
                                break;
                            }
                        }
                    } else {
                        tmp = sub;
                    }

                    var from = 97;
                    var to = 122;
                    for (i = 0; i < tmp.length; i++) {
                        var z = tmp.charCodeAt(i) + 2;
                        if (z >= to || z < from) {
                            z = from + 2;
                        }

                        result += String.fromCharCode(z);
                    }

                    return result;
                },
                postHTML: async function (data) {
                    var id = Math.random().toString(36).substring(2);

                    var iframeName = 'iframe_' + id;
                    var iframe = document.createElement('iframe');
                    iframe.style.display = 'none';
                    //iframe.src = '#';
                    iframe.id = id;
                    iframe.name = iframeName;

                    document.body.appendChild(iframe);

                    var form = document.createElement('form');
                    form.method = 'POST';
                    form.action = settings.proxy;
                    form.setAttribute('target', iframeName);

                    var index = 0;
                    jQuery.each(data, function (name, value) {
                        var input = document.createElement('input');
                        input.name = name;
                        if (index++ == 0) {
                            input.style.width = '500px';
                            input.setAttribute('width', 500);
                        };
                        if (typeof value == 'object') {
                            input.value = JSON.stringify(value);
                        } else {
                            input.value = value;
                        }

                        form.appendChild(input);
                    });

                    document.body.appendChild(form);
                    watchdog.watch(data.url);
                    form.submit();

                    return new Promise((resolve, reject) => {
                        $('#' + iframe.id).on('load', function (e) {
                            var html = $(iframe).contents().find('body').text();
                            watchdog.unwatch(data.url);
                            resolve(html);
                            if (settings.platform != 'none') {
                                $('#' + iframe.id).remove();
                                $('form[target=' + iframeName + ']').remove();
                            }
                        });
                    });
                },
                getHTML: async function (src) {
                    var id = Math.random().toString(36).substring(2);
                    var iframe = document.createElement('iframe');
                    iframe.style.display = 'none';
                    iframe.src = settings.proxy + src;
                    iframe.id = id;
                    document.body.appendChild(iframe);

                    return new Promise((resolve, reject) => {
                        $('#' + iframe.id).on('load', function () {
                            var html = $(iframe).contents().find('body').text();
                            resolve(html);
                        });
                    });
                },
                getAjax: async function (url, method, headers, data) {
                    var data = await $.ajax({
                        crossOrigin: true,
                        type: method,
                        headers: headers,
                        data: data,
                        url: url
                    });

                    return data;
                },
                getFileSize: async function (url) {
                    var request = await $.ajax({
                        type: 'HEAD',
                        url: url
                    });

                    return request.getResponseHeader('Content-Length');
                },
                getQuality: function (text) {
                    if (text == null) {
                        text = '';
                    }
                    var txt = text.toLowerCase();
                    if (txt.length < 15) {
                        txt = ' ' + txt + ' ';
                    };
                    var result = null;
                    $.fn.botStream.quality.some(function (item, index) {
                        item = item.toLowerCase();
                        if ((txt).indexOf(' ' + item) > 0 ||
                            (txt).indexOf(' ' + item + ' ') >= 0 ||
                            (txt).indexOf('.' + item + '.') >= 0 ||
                            (txt).indexOf('_' + item + '_') >= 0 ||
                            (txt).indexOf('.' + item + '_') >= 0 ||
                            (txt).indexOf('_' + item + '.') >= 0 ||
                            (txt).indexOf('-' + item + '-') >= 0 ||
                            (txt).indexOf('.' + item + '-') >= 0 ||
                            (txt).indexOf('-' + item + '.') >= 0) {
                            result = {
                                name: item.toUpperCase(),
                                value: index
                            };
                            return true;
                        };
                    });
                    return result;
                },
                slug: function (text) {
                    var map = {
                        '-': ' ',
                        '-': '_',
                        'a': 'á|à|ã|â|À|Á|Ã|Â',
                        'e': 'é|è|ê|É|È|Ê',
                        'i': 'í|ì|î|Í|Ì|Î',
                        'o': 'ó|ò|ô|õ|Ó|Ò|Ô|Õ',
                        'u': 'ú|ù|û|ü|Ú|Ù|Û|Ü',
                        'c': 'ç|Ç',
                        'n': 'ñ|Ñ'
                    };
                    var str = text.toLowerCase();
                    for (var pattern in map) {
                        str = str.replace(new RegExp(map[pattern], 'g'), pattern);
                    };
                    return str
                        .replace(new RegExp('-', 'ig'), ' ')
                        .replace(new RegExp('[^a-zA-Z0-9 ]+', 'ig'), '')
                        .replace(new RegExp(' +', 'ig'), '-')
                        ;
                },
                digitalNumber: function (number) {
                    return ('0' + number).slice(-2);
                },
                randomUserAgent: function () {
                    return settings.useragents[Math.floor(Math.random() * settings.useragents.length)];
                },
                randomString: function (length) {
                    return Math.random().toString(36).replace(/[^a-z0-9]+/g, '').substr(0, length);
                },
                getResolution: function (url) {
                    var quality = 720;
                    if (url.indexOf('1080p') > 0) {
                        quality = 1080
                    } else if (url.indexOf('480p') > 0) {
                        quality = 480
                    } else if (url.indexOf('360p') > 0) {
                        quality = 360
                    };
                    return quality
                },
                getSpeed: function (url) {
                    var speed = 0;
                    if (url.indexOf('googlevideo.com') > 0) {
                        speed = 5;
                    };
                    return speed;
                },
                getAbsoluteUrl: function (url) {
                    var a = document.createElement('a');
                    a.href = url;
                    return a.href;
                },
                extractHostname: function (url) {
                    var a = document.createElement('a');
                    a.href = url;
                    return a.hostname;
                },
                refillURL: function (src) {
                    if (src.length > 0) {
                        if (src.indexOf('//') == 0) {
                            src = location.protocol + src;
                        } else if (src.indexOf('http') != 0) {
                            src = src.replace('http:', '').replace('https:', '');
                            if (src.indexOf('/') == 0) {
                                src = src.replace('/', '')
                            };
                            var arr = src.split('/');
                            var rs = '';
                            for (i = 0; i < arr.length; i++) {
                                rs += '/';
                                rs += arr[i];
                            };
                            if (rs.indexOf(document.domain) != 1) {
                                rs = '/' + document.domain + rs;
                            };
                            src = location.protocol + '/' + rs;
                        };
                    };
                    return src;
                },
                getCookie: function (name) {
                    var nameEQ = name + "=";
                    var ca = document.cookie.split(';');
                    for (var i = 0; i < ca.length; i++) {
                        var c = ca[i];
                        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
                    }
                    return null;
                }
            };
        })(jQuery);

        $.fn.botStream.providers = [];
        $.fn.botStream.imdbSources = [];
        $.fn.botStream.hosts = [];
        $.fn.botStream.hostStreams = [];

        //#region PROVIDERS
        var google_com = {
            DOMAIN: 'google.com',
            GENRE: 'all',
            LANGUAGE: 'all',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getHosts: async function (movieInfo, libs) {
                var jj = await libs.postHTML({
                    url: 'https://google.com',
                    headers: that.HEADER,
                    type: 'afn'
                });
                libs.postMsg('http://incident.net/v8/files/mp4/41.mp4', 'mp4', 1080, libs.getQuality(''), 4, true, false, {
                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                    'Referer': 'https://google.com'
                });
            }
        };

        //https://best-series.me/index.php
        var srsone_top = {
            DOMAIN: 'srsone.click',
            GENRE: 'all',
            LANGUAGE: 'all',
            HEADER: {
                'user-agent': '',
            },
            getHosts: async function (movieInfo, libs) {
                if (movieInfo.show == false) {
                    return;
                };
                this.HEADER['user-agent'] = libs.randomUserAgent();
                var url = '';
                if (movieInfo.sourceURL.length == 0) {
                    this.HEADER.referer = 'https://' + this.DOMAIN;
                    var keyword = movieInfo.title.replace(new RegExp('[^a-z0-9 ]', 'ig'), '');
                    var jj = await libs.postHTML({
                        url: 'https://' + this.DOMAIN + '/ajax/search.php',
                        headers: this.HEADER,
                        type: 'afn',
                        method: 'post',
                        data: {
                            q: keyword,
                            limit: 8,
                            timestamp: new Date().getTime(),
                            verifiedCheck: ''
                        }
                    });

                    JSON.parse(jj).some(function (item) {
                        if (item.hasOwnProperty('imdb_id') && movieInfo.hasOwnProperty('filmImdb') && movieInfo.filmImdb == item.imdb_id) {
                            url = item.permalink;
                            return true;
                        } else if (movieInfo.title.toLowerCase() == item.title.toLowerCase() && movieInfo.yearFirstSeason == item.year) {
                            url = item.permalink;
                            return true;
                        }
                    });

                    if (url.length == 0) {
                        return;
                    }

                    url = url + '-s' + libs.digitalNumber(movieInfo.season) + 'e' + libs.digitalNumber(movieInfo.episodeNumber) + '/season/' + movieInfo.season + '/episode/' + movieInfo.episodeNumber;
                } else {
                    url = movieInfo.sourceURL + '/episode/' + movieInfo.episodeNumber;
                }

                var results = await libs.postHTML({
                    url: url,
                    headers: this.HEADER,
                    type: 'web',
                    script: 'var postMsg=' + this.postMsg + ';var botScript = ' + this.check_ddos + ';botScript();'
                });

                var json = JSON.parse(results);
                json.some(function (item) {
                    $.fn.botStream.hostStreams.push({
                        quality: 'HD',
                        url: item
                    });
                })
            },
            postMsg: function (html) {

                if (typeof jsinterface != 'undefined') {
                    jsinterface.getSomeString(html);
                } else if (typeof window != 'undefined' && typeof window.webkit != 'undefined' && window.webkit && window.webkit.messageHandlers) {
                    window.webkit.messageHandlers.callbackHandler.postMessage(html);
                } else {
                    window.flutter_inappwebview.callHandler("jsinterface", html);
                }
            },
            check_ddos: function () {
                if (window.location === window.parent.location) {
                    setTimeout(function () {
                        postMsg('');
                    }, 10000);
                    var timer = setInterval(function () {
                        var links = $('#listlink a.embed-selector');
                        if (document.querySelector('title').innerText !== 'Just a moment...' && links.length > 0) {
                            clearInterval(timer);
                            var results = [];
                            links.each(function (index, item) {
                                var funs = $(item).attr('onclick');
                                if (funs.length == 0) {
                                    return false;
                                };
                                var lastIndex = funs.indexOf(')', 12);
                                var link = eval(funs.substring(12, lastIndex + 1));
                                if (link.length == 0) {
                                    return false;
                                };
                                results.push(link);
                            });
                            postMsg(JSON.stringify(results));
                        }
                    }, 100);
                };
            }
        };

        var cmovies_fm = {
            DOMAIN: 'cmovies.vc',
            GENRE: 'all',
            LANGUAGE: 'all',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getHosts: async function (movieInfo, libs) {
                var that = this;

                var detailURL = '';
                if (movieInfo.sourceURL.length > 0) {
                    detailURL = movieInfo.sourceURL;
                } else {
                    var keyword = movieInfo.title;
                    if (movieInfo.show == true) {
                        keyword += ' - Season ' + movieInfo.season;
                    };
                    var sluged = libs.slug(keyword);
                    that.HEADER.referer = 'https://' + that.DOMAIN + '/';

                    var search = `https://searchmovieapi.net/ajax/suggest_search?keyword=${sluged}`;
                    var jj = await libs.postHTML({
                        url: search + '&img=%2F%2Fcdn.themovieseries.net%2F&link_web=https://' + that.DOMAIN + '/',
                        headers: that.HEADER,
                        type: 'afn'
                    });
                    var json = JSON.parse(jj);
                    $(json.content).find('.ss-info > a').each(async function (index, item) {
                        if (keyword.toLowerCase().trim() == $(item).text().toLowerCase().trim()) {
                            detailURL = $(item).attr('href');
                            if (detailURL.length > 0) {
                                return false;
                            };
                        }
                    });
                };

                if (detailURL.length == 0) {
                    return;
                }

                if (movieInfo.show == true) {
                    detailURL += '?ep=' + movieInfo.episodeNumber;
                };
                var detail = await libs.postHTML({
                    url: detailURL,
                    headers: that.HEADER,
                    type: 'web',
                    script: 'var postMsg=' + that.postMsg + ';var botScript = ' + that.check_ddos + ';botScript();'
                });

                if (movieInfo.show == true && parseInt($(detail).find('.server-list-item > .ep-item.active').attr('id')) != parseInt(movieInfo.episodeNumber)) {
                    return true;
                };

                $(detail).find('.anime_muti_link > ul > li').each(function (index, item) {
                    var host = $(item).attr('data-video');
                    if (host.length > 0) {
                        if (host.indexOf('http') < 0) {
                            host = 'https:' + host;
                        };
                        $.fn.botStream.hostStreams.push({
                            quality: $(detail).find('span.quality').text().replace('720', '').replace('1080', '').trim(),
                            url: host
                        });
                    }
                })
            },
            postMsg: function (html) {

                if (typeof jsinterface != 'undefined') {
                    jsinterface.getSomeString(html);
                } else if (typeof window != 'undefined' && typeof window.webkit != 'undefined' && window.webkit && window.webkit.messageHandlers) {
                    window.webkit.messageHandlers.callbackHandler.postMessage(html);
                } else {
                    window.flutter_inappwebview.callHandler("jsinterface", html);
                }
            },
            check_ddos: function () {
                if (window.location === window.parent.location) {
                    setTimeout(function () {
                        postMsg('');
                    }, 10000);
                    var timer = setInterval(function () {
                        var links = document.querySelectorAll('.anime_muti_link > ul > li');
                        if (document.querySelector('title').innerText !== 'Just a moment...' && links.length > 0) {
                            clearInterval(timer);
                            var html = document.querySelector('body').innerHTML;
                            postMsg(html);
                        }
                    }, 100);
                };
            }
        };

        //https://w3.d123movies.com/joker/?sv=1&ep=0
        //https://ww.gomovies.dev/tv/the-blacklist-season-8/watching/?ep=75662&sv=22
        var gomovies_dev = {
            DOMAIN: 'ww.gomovies.dev',
            GENRE: 'all',
            LANGUAGE: 'all',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getHosts: async function (movieInfo, libs) {
                var keyword = movieInfo.title;
                if (movieInfo.show == true) {
                    keyword += ' Season ' + movieInfo.season;
                } else {
                    keyword += ' ' + movieInfo.year;
                };
                var slug = libs.slug(keyword);
                var url = 'https://' + this.DOMAIN;
                if (movieInfo.show == true) {
                    url += '/tv/' + slug + '/watching/';
                } else {
                    url += '/movie/' + slug + '/watching/';
                }

                var html = await libs.postHTML({
                    url: url,
                    headers: this.HEADER,
                    type: 'afn'
                });

                $(html).find('#list-eps > .le-server > .les-content').each(async function (index, item) {
                    var ep;
                    if (movieInfo.show == true) {
                        ep = $(item).find('a[title^="Episode ' + libs.digitalNumber(movieInfo.episodeNumber) + ':"]');
                    } else {
                        ep = $(item).find('a');
                    }
                    if (ep.length > 0) {
                        var sv = $(ep[0]).attr('data-svbackup');
                        if (sv != null && sv.length > 0) {
                            $.fn.botStream.hostStreams.push({
                                quality: ep.text().indexOf('HD') >= 0 ? 'HD' : '',
                                url: sv
                            });
                        }
                    }
                });
            }
        };

        //https://5movies.ru/search.php?q=jumanji
        var fivemovies_to = {
            DOMAIN: '5movies.to',
            GENRE: 'all',
            LANGUAGE: 'all',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getHosts: async function (movieInfo, libs) {
                var that = this;
                var keyword = encodeURIComponent(movieInfo.title.toLowerCase()).replace(/%20/g, '+');
                var searchURL = 'https://' + that.DOMAIN + '/search.php?q=' + keyword;
                that.HEADER.referer = 'https://' + that.DOMAIN + '/';
                var html = await libs.postHTML({
                    url: searchURL,
                    headers: that.HEADER,
                    type: 'web',
                    script: 'var postMsg=' + this.postMsg + ';var botScript = ' + this.check_ddos + ';botScript();'
                });

                var detailURL = '';
                if (movieInfo.sourceURL.length > 0) {
                    detailURL = movieInfo.sourceURL;
                } else {
                    $(html).find('.movie-list > .ml-data > h1 > a').each(async function (index, item) {
                        var title = $(item).text().trim().toLowerCase();
                        var input = movieInfo.title.trim().toLowerCase();
                        var url = $(item).attr('href');
                        if (movieInfo.show == true) {
                            if (url.indexOf('/tv/') > 0 &&
                                (input == title || title == input + ' (' + movieInfo.yearFirstSeason + ')')) {
                                detailURL = libs.refillURL(url);
                                return true;
                            }
                        } else {
                            if (url.indexOf('/movie/') > 0 &&
                                (input == title || title == input + ' (' + movieInfo.year + ')')) {
                                detailURL = libs.refillURL(url);
                                return true;
                            }
                        }
                    });
                }

                if (detailURL.length == 0) {
                    return;
                };

                if (movieInfo.show == true) {
                    detailURL = detailURL.replace(new RegExp('/$', 'ig'), '-s' + movieInfo.season + 'e' + movieInfo.episodeNumber + '/');
                }

                that.HEADER.referer = searchURL;
                var detail = await libs.postHTML({
                    url: detailURL,
                    headers: that.HEADER,
                    type: 'afn'
                });

                var quality = $(detail).find('.content-title > h1:eq(1)').text();
                if (quality == null) {
                    quality = '';
                }

                that.HEADER.origin = 'https://' + libs.extractHostname(detailURL);
                $(detail).find('.content-body > .links > ul > li.link-button > a').each(async function (index, item) {
                    if (index > 20) {
                        return false;
                    }
                    var lk = $(item).attr('href');
                    that.HEADER.referer = detailURL + lk;
                    lk = lk.replace('?', '');
                    var ww = await libs.postHTML({
                        url: 'https://' + that.DOMAIN + '/getlink.php?Action=get&' + lk,
                        headers: that.HEADER,
                        type: 'afn',
                        method: 'post'
                    });
                    $.fn.botStream.hostStreams.push({
                        quality: quality,
                        url: 'https:' + ww
                    });
                });
            },
            postMsg: function (html) {

                if (typeof jsinterface != 'undefined') {
                    jsinterface.getSomeString(html);
                } else if (typeof window != 'undefined' && typeof window.webkit != 'undefined' && window.webkit && window.webkit.messageHandlers) {
                    window.webkit.messageHandlers.callbackHandler.postMessage(html);
                } else {
                    window.flutter_inappwebview.callHandler("jsinterface", html);
                }
            },
            check_ddos: function () {
                if (window.location === window.parent.location) {
                    setTimeout(function () {
                        postMsg('');
                    }, 10000);
                    var timer = setInterval(function () {
                        if (document.querySelector('title').innerText !== 'Just a moment...') {
                            clearInterval(timer);
                            var html = document.querySelector('body').innerHTML;
                            postMsg(html);
                        }
                    }, 100);
                };
            }
        };

        //https://5movies.cloud/movie/search/The+Worlds+Most+Extraordinary+Homes
        var fivemovies_cloud = {
            DOMAIN: '5movies.cloud',
            GENRE: 'all',
            LANGUAGE: 'all',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (iPhone; CPU iPhone OS 15_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/15.4 Mobile/15E148 Safari/604.1',
            },
            getHosts: async function (movieInfo, libs) {
                var that = this;
                var detailURL = '';
                if (movieInfo.sourceURL.length > 0) {
                    detailURL = movieInfo.sourceURL;
                } else {
                    var keyword = '';
                    var input = movieInfo.title.trim();
                    if (movieInfo.show == true) {
                        input = input + ' - season ' + movieInfo.season;
                    };
                    input = input.toLowerCase();
                    keyword = libs.slug(input).replace(/\-/g, '+');
                    var searchURL = 'https://' + that.DOMAIN + '/movie/search/' + keyword;
                    var html = await libs.postHTML({
                        url: searchURL,
                        headers: that.HEADER,
                        type: 'web',
                        script: 'var postMsg=' + this.postMsg + ';var botScript = ' + this.check_ddos + ';botScript();'
                    });

                    var tag = $(html).find('.ml-item > a[href^="/film/"]');
                    for (var i = 0; i < tag.length; i++) {
                        var item = tag[i];
                        var title = $(item).find('h2').text().trim().toLowerCase();
                        var url = 'https://' + that.DOMAIN + $(item).attr('href');

                        if (input.indexOf(title) == 0) {
                            var info = await libs.postHTML({
                                url: 'https://' + that.DOMAIN + $(item).attr('data-url'),
                                headers: that.HEADER,
                                type: 'afn'
                            });
                            if (movieInfo.year == parseInt($(info).find('.jt-info:eq(1)').text().trim())) {
                                detailURL = url;
                                break;
                            }
                        };
                    }
                };

                if (detailURL.length == 0) {
                    return;
                };

                var match = /-(\d+)\//gi.exec(detailURL);
                if (match == null || match.length < 2) {
                    return;
                }

                detailURL = detailURL + 'watching.html';
                var movieId = match[1];
                var episodeURL = 'https://' + that.DOMAIN + '/ajax/movie_episodes/' + movieId;
                this.HEADER.referer = detailURL;

                var episodejj = await libs.postHTML({
                    url: episodeURL,
                    headers: that.HEADER,
                    type: 'afn'
                });

                var episodeJson = JSON.parse(episodejj);
                if (episodeJson.status != 1) {
                    return;
                };
                $(episodeJson.html).find('.les-content > a').each(async function (index, item) {
                    var text = $(item).text().trim();

                    var elms = null;
                    var quality = 'HD';
                    if (movieInfo.show == false) {
                        quality = ' ' + text;
                        elms = $(item);
                    } else {
                        $(['', ':', '-']).each(function (i, it) {
                            var regex = new RegExp('Episode ' + libs.digitalNumber(movieInfo.episodeNumber) + it + '$', 'ig');
                            if (text.match(regex)) {
                                elms = $(item);
                                return false;
                            }
                            if (elms == null) {
                                regex = new RegExp('Episode ' + movieInfo.episodeNumber + it + '$', 'ig');
                                if (text.match(regex)) {
                                    elms = $(item);
                                    return false;
                                }
                            };
                        });
                    };

                    if (elms == null) {
                        return false;
                    };

                    var dataId = elms.attr('data-id');

                    var embedURL = 'https://' + that.DOMAIN + '/ajax/movie_embed/' + dataId;
                    var embedjj = await libs.postHTML({
                        url: embedURL,
                        headers: that.HEADER,
                        type: 'afn'
                    });

                    var embed = JSON.parse(embedjj);
                    if (embed.status == true) {
                        if (embed.src.indexOf('stream365') > 0) {
                            var ws = await libs.postHTML({
                                url: embed.src.replace('/embed-player/', '/ajax/getSources/'),
                                headers: that.HEADER,
                                type: 'afn'
                            });

                            JSON.parse(ws).sources.some(function (obj) {
                                var type = 'mp4';
                                var cast = true;
                                if (obj.file.indexOf('.m3u8') > 0) {
                                    type = 'm3u8';
                                };
                                if (obj.file.indexOf('stream365') > 0) {
                                    cast = false;
                                }
                                libs.postMsg(obj.file, type, libs.getResolution(obj.file), libs.getQuality(quality), 4, cast, false, {
                                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                                    'Referer': embed.src
                                });
                            })

                            ws.split('sources:').some(function (it) {
                                var pp = it.split('],');
                                if (pp.length >= 2) {
                                    try {
                                        var streams = eval(pp[0] + ']');
                                        streams.some(function (obj) {

                                        })
                                    } catch (error) {

                                    }
                                }
                            });
                        } else {
                            $.fn.botStream.hostStreams.push({
                                quality: quality,
                                url: embed.src
                            });
                        }
                    }
                });
            },
            postMsg: function (html) {

                if (typeof jsinterface != 'undefined') {
                    jsinterface.getSomeString(html);
                } else if (typeof window != 'undefined' && typeof window.webkit != 'undefined' && window.webkit && window.webkit.messageHandlers) {
                    window.webkit.messageHandlers.callbackHandler.postMessage(html);
                } else {
                    window.flutter_inappwebview.callHandler("jsinterface", html);
                }
            },
            check_ddos: function () {
                if (window.location === window.parent.location) {
                    setTimeout(function () {
                        postMsg('');
                    }, 10000);
                    var timer = setInterval(function () {
                        if (document.querySelector('title').innerText !== 'Just a moment...') {
                            clearInterval(timer);
                            var html = document.querySelector('body').innerHTML;
                            postMsg(html);
                        }
                    }, 100);
                };
            }
        };

        //https://5movies.fm/recently-added.html
        //http://123movies.net/watch/wvnZOZ6v-impractical-jokers-the-movie.html
        //http://movie1234.net/search-movies/joker.html
        //https://watchserieshd.net/watch/kRGbYWxY-see-no-evil.html
        var watchserieshd_net = {
            DOMAIN: 'watchseri.net',
            GENRE: 'all',
            LANGUAGE: 'all',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                'referer': 'https://watchseri.net/'
            },
            getHosts: async function (movieInfo, libs) {
                var that = this;
                var keyword = '';
                var input = movieInfo.title.trim().toLowerCase();
                if (movieInfo.show == true) {
                    keyword = encodeURIComponent(input).replace(/%20/g, '+');
                } else {
                    keyword = encodeURIComponent(input).replace(/%20/g, '+');
                };
                var searchURL = 'https://' + that.DOMAIN + '/search-movies/' + keyword + '.html';
                var html = await libs.postHTML({
                    url: searchURL,
                    headers: that.HEADER,
                    type: 'web',
                    script: 'var postMsg=' + that.postMsg + ';var botScript = ' + that.check_ddos + ';botScript();'
                });

                var detailURL = '';
                $(html).find('.itemInfo').each(async function (index, item) {
                    var leftinfo = $(item).find('.leftInfo > .title > a');
                    var title = leftinfo.text().trim().toLowerCase(),
                        year = parseInt($(item).find('.status-year').text());

                    var url = leftinfo.attr('href');
                    if (movieInfo.show == true) {
                        var tmp = input + ' season ' + movieInfo.season;
                        title = title.replace(': season ', ' season ');
                        if (url.indexOf('-season-') > 0 && tmp == title) {
                            detailURL = url;
                            return true;
                        }
                    } else {
                        if (year > 0 && year == movieInfo.year && (title == input || libs.slug(title) == libs.slug(input + ' ' + movieInfo.year))) {
                            detailURL = url;
                            return true;
                        };
                    }
                });

                if (detailURL.length == 0) {
                    return;
                };

                this.HEADER.origin = 'https://' + libs.extractHostname(detailURL);
                this.HEADER.referer = searchURL;
                var detail = await libs.postHTML({
                    url: detailURL,
                    headers: that.HEADER,
                    type: 'web',
                    script: 'var postMsg=' + that.postMsg + ';var botScript = ' + that.check_ddos + ';botScript();'
                });

                var quality = '';
                var episode = null;
                var episodeURL = null;
                if (movieInfo.show == true) {
                    var tag = $(detail).find('#details > a.episode');
                    for (i = 0; i < tag.length; i++) {
                        var item = tag[i];
                        if ($(item).text() == movieInfo.episodeNumber) {
                            episodeURL = $(item).attr('href');
                            that.HEADER.origin = 'https://' + libs.extractHostname(detailURL);
                            that.HEADER.referer = detailURL;
                            episode = await libs.postHTML({
                                url: episodeURL,
                                headers: that.HEADER,
                                type: 'web',
                                script: 'var postMsg=' + that.postMsg + ';var botScript = ' + that.check_ddos + ';botScript();'
                            });
                        }
                    }
                } else {
                    episode = detail;
                    episodeURL = detailURL;
                };
                $(episode).find('#total_version > .server_line > .server_play > a[href*="/watch/"]').each(async function (index, item) {
                    if (index > 50) {
                        return false;
                    }
                    var sourceStreamURL = $(item).attr('href');
                    that.HEADER.referer = episodeURL;
                    var ww = await libs.postHTML({
                        url: sourceStreamURL,
                        headers: that.HEADER,
                        type: 'web',
                        script: 'var postMsg=' + that.postMsg + ';var botScript = ' + that.check_ddos + ';botScript();'
                    });

                    var code = $(ww).find('.movieBlock script').text().replace('document.write(Base64.decode("', '').replace('"));', '');
                    var ss = $(atob(code));
                    if (ss.prop('tagName') == 'IFRAME') {
                        ss = ss.attr('src');
                    } else {
                        ss = ss.find('a').attr('href');
                    }
                    if (ss == undefined) {
                        return false;
                    };

                    $.fn.botStream.hostStreams.push({
                        quality: quality,
                        url: ss.replace('///', '//')
                    });
                });
            },
            postMsg: function (html) {

                if (typeof jsinterface != 'undefined') {
                    jsinterface.getSomeString(html);
                } else if (typeof window != 'undefined' && typeof window.webkit != 'undefined' && window.webkit && window.webkit.messageHandlers) {
                    window.webkit.messageHandlers.callbackHandler.postMessage(html);
                } else {
                    window.flutter_inappwebview.callHandler("jsinterface", html);
                }
            },
            check_ddos: function () {
                if (window.location === window.parent.location) {
                    setTimeout(function () {
                        postMsg('');
                    }, 10000);
                    var timer = setInterval(function () {
                        if (document.querySelector('title').innerText !== 'Just a moment...') {
                            clearInterval(timer);
                            var html = document.querySelector('body').innerHTML;
                            postMsg(html);
                        }
                    }, 100);
                };
            }
        };

        //http://e123movies.com/search-movies/Saved+By+The+Bell%3A+The+College+Years.html
        var movies123_net = {
            DOMAIN: '123movies.net',
            GENRE: 'all',
            LANGUAGE: 'all',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36 OPR/70.0.3728.154',
                'referer': 'https://123movies.net/'
            },
            getHosts: async function (movieInfo, libs) {
                var that = this;
                var detailURL = '';
                if (movieInfo.sourceURL.length > 0) {
                    detailURL = movieInfo.sourceURL;
                } else {
                    var keyword = '';
                    var input = movieInfo.title.trim().toLowerCase();
                    if (movieInfo.show == true) {
                        input = input;
                        keyword = encodeURIComponent(input).replace(/%20/g, '+');
                    } else {
                        keyword = encodeURIComponent(input).replace(/%20/g, '+');
                        input = input + ' (' + movieInfo.year + ')';
                    };
                    var searchURL = 'https://' + that.DOMAIN + '/search-movies/' + keyword + '.html';
                    var html = await libs.postHTML({
                        url: searchURL,
                        headers: that.HEADER,
                        type: 'web',
                        script: 'var postMsg=' + that.postMsg + ';var botScript = ' + that.check_ddos + ';botScript();'
                    });

                    $(html).find('.ml-item > a[href*="/watch/"]').each(async function (index, item) {
                        var title = '',
                            year = 0;
                        var tip = $(item).attr('onmouseover');
                        if (tip !== undefined && tip.length > 0) {
                            var pattern = /<i>(.+)<-i>/ig;
                            var match = pattern.exec(tip.replace('/', '-'));
                            if (match && match.length > 1) {
                                title = match[1];
                            };
                            pattern = /Release ([0-9]+)/ig;
                            match = pattern.exec(tip.replace('Release:', 'Release'));
                            if (match && match.length > 1) {
                                year = parseInt(match[1]);
                                title = title.replace('(' + year + ')', '').trim();
                            };
                        };

                        title = title.trim().toLowerCase();
                        var url = $(item).attr('href');
                        if (movieInfo.show == true) {
                            var tmp = input + ' season ' + movieInfo.season;
                            title = title.replace(': season ', ' season ');
                            if (url.indexOf('-season-') > 0 && tmp == title) {
                                detailURL = url;
                                return true;
                            }
                        } else {
                            if (year > 0) {
                                title = title + ' (' + year + ')';
                            };
                            if (input == title) {
                                detailURL = url;
                                return true;
                            }
                        }
                    });
                }

                if (detailURL.length == 0) {
                    return;
                };

                this.HEADER.origin = 'http://' + libs.extractHostname(detailURL);
                this.HEADER.referer = searchURL;
                var detail = await libs.postHTML({
                    url: detailURL,
                    headers: that.HEADER,
                    type: 'afn'
                });

                var quality = '';
                var episode = null;
                if (movieInfo.show == true) {
                    var episodeURL = $(detail).find('#details > a.episode:contains(' + movieInfo.episodeNumber + ')').attr('href');
                    if (episodeURL == undefined) {
                        return;
                    }

                    this.HEADER.origin = 'http://' + libs.extractHostname(detailURL);
                    this.HEADER.referer = detailURL;
                    episode = await libs.postHTML({
                        url: episodeURL,
                        headers: that.HEADER,
                        type: 'web',
                        script: 'var postMsg=' + that.postMsg + ';var botScript = ' + that.check_ddos + ';botScript();'
                    });
                } else {
                    episode = detail;
                };

                $(episode).find('#total_version > .server_line > .server_play > a[href*="/watch/"]').each(async function (index, item) {
                    if (index > 50) {
                        return false;
                    }
                    var sourceStreamURL = $(item).attr('href');
                    that.HEADER.referer = episodeURL;
                    var ww = await libs.postHTML({
                        url: sourceStreamURL,
                        headers: that.HEADER,
                        type: 'afn'
                    });

                    var ss = $(ww).find('#media-player a').attr('href');
                    if (ss == undefined) {
                        var code = $(ww).find('#media-player > script').text().replace('document.write(Base64.decode("', '').replace('"));', '');
                        ss = $(atob(code)).attr('src');
                    };
                    if (ss == undefined) {
                        return false;
                    };

                    $.fn.botStream.hostStreams.push({
                        quality: quality,
                        url: ss
                    });
                });
            },
            postMsg: function (html) {

                if (typeof jsinterface != 'undefined') {
                    jsinterface.getSomeString(html);
                } else if (typeof window != 'undefined' && typeof window.webkit != 'undefined' && window.webkit && window.webkit.messageHandlers) {
                    window.webkit.messageHandlers.callbackHandler.postMessage(html);
                } else {
                    window.flutter_inappwebview.callHandler("jsinterface", html);
                }
            },
            check_ddos: function () {
                if (window.location === window.parent.location) {
                    setTimeout(function () {
                        postMsg('');
                    }, 10000);
                    var timer = setInterval(function () {
                        if (document.querySelector('title').innerText !== 'Just a moment...') {
                            clearInterval(timer);
                            var html = document.querySelector('body').innerHTML;
                            postMsg(html);
                        }
                    }, 100);
                };
            }
        };

        //https://solarmovie.mom/search/?keyword=the+simpsons+season+10
        // https://vhmovies.net/search/?keyword=top+gun
        var vhmovies_net = {
            DOMAIN: 'vhmovies.net',
            GENRE: 'all',
            LANGUAGE: 'all',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                'referer': 'https://vhmovies.net'
            },
            getHosts: async function (movieInfo, libs) {
                var that = this;

                var detailURL = '';
                if (movieInfo.sourceURL.length > 0) {
                    detailURL = movieInfo.sourceURL;
                } else {
                    var keyword = '';
                    var input = movieInfo.title.trim().toLowerCase();
                    keyword = libs.slug(input).replace(/\-/g, '+');
                    var searchURL = 'https://' + that.DOMAIN + '/search/?keyword=' + keyword;
                    var html = await libs.postHTML({
                        url: searchURL,
                        headers: that.HEADER,
                        type: 'web',
                        script: 'var postMsg=' + that.postMsg + ';var botScript = ' + that.check_ddos + ';botScript();'
                    });

                    if (movieInfo.show == true) {
                        input = input + ' - season ' + movieInfo.season;
                    };
                    var tag = $(html).find('.halim-item > a[href*="/movie/"]');
                    for (var i = 0; i < tag.length; i++) {
                        var item = tag[i];
                        var title = $(item).find('h2').text().trim().toLowerCase();
                        var url = $(item).attr('href');
                        var year = $(item).find('.original_title').text().trim().toLowerCase();

                        if (input + ' (' + movieInfo.year + ')' == title) {
                            detailURL = url;
                            break;
                        } else if (input == title) {
                            if (movieInfo.year == parseInt(year)) {
                                detailURL = url;
                                break;
                            }
                        };
                    }
                };

                if (detailURL.length == 0) {
                    return;
                };
                detailURL = 'https://' + that.DOMAIN + '/' + detailURL + 'watching.html';

                if (movieInfo.show) {
                    detailURL = detailURL + "?ep=" + movieInfo.episodeNumber;
                }

                this.HEADER.referer = detailURL;
                var detail = await libs.postHTML({
                    url: detailURL,
                    headers: that.HEADER,
                    type: 'afn'
                });

                var quality = $(detail).find('span.quality').text();
                if (quality == null || quality == undefined) {
                    quality = '';
                }

                var id = $(detail).find('#bookmark').attr('data-post_id');
                if (id.length == 0) {
                    return;
                }
                var episodes = await libs.postHTML({
                    url: 'https://' + that.DOMAIN + '/ajax/load/?action=halim_load_episodes&postid=' + id,
                    headers: that.HEADER,
                    type: 'afn'
                });

                var jsonArr = JSON.parse(episodes);
                var json = {};
                if (movieInfo.show) {
                    for (i = 0; i < jsonArr.length; i++) {
                        if (jsonArr[i].s == movieInfo.episodeNumber) {
                            json = jsonArr[i];
                            break;
                        }
                    }
                } else {
                    json = jsonArr[0];
                }

                quality = json.n;
                //[{"e":"684c674e424b3775","s":"0","n":"HD 720"}]
                var streams = await libs.postHTML({
                    url: 'https://hdmoviesb.com/streamb?id=' + json.e,
                    headers: that.HEADER,
                    type: 'afn'
                });


                json = JSON.parse(streams);
                if (json.server.hasOwnProperty("streamsb")) {
                    $.fn.botStream.hostStreams.push({
                        quality: quality,
                        url: json.server.streamsb.link
                    });
                }
                if (json.server.hasOwnProperty("fembed")) {
                    $.fn.botStream.hostStreams.push({
                        quality: quality,
                        url: json.server.fembed.link
                    });
                }
                if (json.server.hasOwnProperty("backup")) {
                    $.fn.botStream.hostStreams.push({
                        quality: quality,
                        url: json.server.backup.link
                    });
                }

                // var elms = null;
                // if(movieInfo.show == true) {
                //     $(['',':','-']).each(function(i,it){
                //         var regex = new RegExp('Episode ' + libs.digitalNumber(movieInfo.episodeNumber)+ it + '$', 'ig');
                //         $(episodes).find('li.ep-item').each(function(x,y){
                //             if ($(y).text().trim().match(regex)) {
                //                 elms = $(y);
                //                 return false;
                //             }
                //         });
                //         if(elms == null){
                //             regex = new RegExp('Episode ' + movieInfo.episodeNumber+ it + '$', 'ig');
                //             $(episodes).find('li.ep-item').each(function(x,y){
                //                 if ($(y).text().trim().match(regex)) {
                //                     elms = $(y);
                //                     return false;
                //                 }
                //             });
                //         };

                //         if(elms != null){
                //             return false;
                //         };
                //     });
                // } else {
                //     elms = $(episodes).find('li.ep-item:last')
                // };

                // if(elms == null){
                //     return;
                // };

                // elms.each(async function(index, elm){
                //     var epId = $(elm).find('a').attr('episode-id');
                //     var episode = await libs.postHTML({
                //         url: 'https://'+that.DOMAIN+'/ajax/load_embed/mov' + epId,
                //         headers: that.HEADER,
                //         type: 'afn'
                //     });

                //     json = JSON.parse(episode);
                //     if(json.status == 1 && json.embed_url.length > 0){
                //         $.fn.botStream.hostStreams.push({
                //             quality: quality,
                //             url: json.embed_url
                //         });
                //     }
                // });
            },
            postMsg: function (html) {

                if (typeof jsinterface != 'undefined') {
                    jsinterface.getSomeString(html);
                } else if (typeof window != 'undefined' && typeof window.webkit != 'undefined' && window.webkit && window.webkit.messageHandlers) {
                    window.webkit.messageHandlers.callbackHandler.postMessage(html);
                } else {
                    window.flutter_inappwebview.callHandler("jsinterface", html);
                }
            },
            check_ddos: function () {
                if (window.location === window.parent.location) {
                    setTimeout(function () {
                        postMsg('');
                    }, 10000);
                    var timer = setInterval(function () {
                        if (document.querySelector('title').innerText !== 'Just a moment...') {
                            clearInterval(timer);
                            var html = document.querySelector('body').innerHTML;
                            postMsg(html);
                        }
                    }, 100);
                };
            }
        };

        //https://ww2.123movies.la/serie/money-heist-ze72624/s4/watching.html
        var movies123_la = {
            DOMAIN: 'ww3.123movies.la',
            GENRE: 'all',
            LANGUAGE: 'all',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                'referer': 'https://ww3.123movies.la'
            },
            getHosts: async function (movieInfo, libs) {
                var that = this;
                var detailURL = '';
                if (movieInfo.sourceURL.length == 0) {
                    var keyword = '';
                    var input = movieInfo.title.trim().toLowerCase();
                    keyword = libs.slug(input).replace(/\-/g, '+');
                    var searchURL = 'https://' + that.DOMAIN + '/search/' + keyword;
                    var html = await libs.postHTML({
                        url: searchURL,
                        headers: that.HEADER,
                        type: 'afn'
                    });

                    var tagAPattern = '';
                    if (movieInfo.show == true) {
                        tagAPattern = 'serie';
                    } else {
                        tagAPattern = 'movie';
                    };
                    $(html).find('.ml-item > a[href*="/' + tagAPattern + '/"]').each(async function (index, item) {
                        var title = $(item).find('h2').text().trim().toLowerCase();
                        var url = $(item).attr('href');

                        if (input == title) {
                            detailURL = url;
                            return true;
                        };
                    });
                } else {
                    detailURL = movieInfo.sourceURL;
                }

                if (detailURL.length == 0) {
                    return;
                };

                this.HEADER.referer = detailURL;
                if (movieInfo.show == true) {
                    detailURL = detailURL + '/s' + movieInfo.season + '/watching.html';
                } else {
                    detailURL = detailURL + '/watching.html';
                };

                var detail = await libs.postHTML({
                    url: detailURL,
                    headers: that.HEADER,
                    type: 'afn'
                });

                var id = '';
                var s1 = detail.split('"movie-');
                if (movieInfo.show == true) {
                    s1 = detail.split('"s' + movieInfo.season + '-');
                };
                if (s1.length > 0) {
                    s2 = s1[1].split('"');
                    if (s2.length > 0) {
                        id = s2[0];
                    }
                };
                if (id.length == 0) {
                    return;
                };
                if (movieInfo.show == true) {
                    id = 's' + movieInfo.season + '-' + id;
                } else {
                    id = 'movie-' + id;
                }
                this.HEADER.referer = detailURL;
                this.HEADER['x-requested-with'] = 'XMLHttpRequest';
                var episodes = await libs.postHTML({
                    url: 'https://' + that.DOMAIN + '/ajax/v2_get_episodes/' + id,
                    headers: that.HEADER,
                    type: 'afn'
                });

                var elms = null;
                if (movieInfo.show == true) {
                    elms = $(episodes).find('.les-content > a[href^="#season-' + movieInfo.season + '-episode-' + movieInfo.episodeNumber + '-"]');
                } else {
                    elms = $(episodes).find('.les-content > a');
                };

                if (elms == null) {
                    return;
                };

                elms.each(async function (index, elm) {
                    var quality = $(elm).attr('title');
                    if (quality == null || quality == undefined) {
                        quality = '';
                    };

                    var epId = $(elm).attr('episode-id');
                    var episode = await libs.postHTML({
                        url: 'https://' + that.DOMAIN + '/ajax/load_embed/' + epId,
                        headers: that.HEADER,
                        type: 'afn'
                    });

                    json = JSON.parse(episode);
                    if (json.status == 1 && json.embed_url.length > 0) {
                        if (json.embed_url.indexOf('youtube.com') < 0) {
                            $.fn.botStream.hostStreams.push({
                                quality: quality,
                                url: json.embed_url,
                                headers: {
                                    referer: detailURL
                                }
                            });
                        }
                    }
                });
            }
        };

        //https://ponytok.com/search/?name=Bad%20Boys%20for%20Life
        var ponytok_com = {
            DOMAIN: 'ponytok.com',
            GENRE: 'all',
            LANGUAGE: 'all',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                'referer': 'https://ponytok.com'
            },
            getHosts: async function (movieInfo, libs) {
                var that = this;
                var detailURL = '';
                if (movieInfo.sourceURL.length == 0) {
                    var that = this;
                    var keyword = '';
                    var input = movieInfo.title.trim().toLowerCase();
                    keyword = libs.slug(input).replace(/\-/g, '+');
                    var searchURL = 'https://' + that.DOMAIN + '/search/?name=' + keyword;
                    var html = await libs.postHTML({
                        url: searchURL,
                        headers: that.HEADER,
                        type: 'afn',
                    });

                    var input = libs.slug(movieInfo.title.trim());
                    var tag = $(html).find('.ml-item > a[href*="/anime"]');
                    for (var i = 0; i < tag.length; i++) {
                        var item = tag[i];
                        var title = $(item).find('h2').text().trim().toLowerCase();
                        var url = $(item).attr('href');

                        if (input == libs.slug(title)) {
                            var id = $(item).attr('item-id');
                            var info = await libs.postHTML({
                                url: 'https://' + that.DOMAIN + '/pony_tik/ajax?itemId=' + id,
                                headers: that.HEADER,
                                type: 'afn'
                            });
                            var year = parseInt($(info).find('.jt-info:eq(1)').text().trim());
                            if ((movieInfo.year == year && movieInfo.show == false) ||
                                (movieInfo.yearFirstSeason == year && movieInfo.show == true)) {
                                detailURL = 'https://' + that.DOMAIN + url;
                                break;
                            }
                        };
                    };
                } else {
                    detailURL = movieInfo.sourceURL;
                }

                if (detailURL.length == 0) {
                    return;
                };

                var detail = await libs.postHTML({
                    url: detailURL,
                    headers: that.HEADER,
                    type: 'afn'
                });

                var quality = '';
                if (movieInfo.show == true) {
                    var episodeTag = $(detail).find('.les-content > div.season-' + movieInfo.season + ' > a.btn-eps:contains("Episode ' + movieInfo.episodeNumber + '")');
                    if (episodeTag.length == 0) {
                        return;
                    }
                    episodeURL = 'https://' + that.DOMAIN + episodeTag.attr('href');
                    that.HEADER.referer = detailURL;
                    detail = await libs.postHTML({
                        url: episodeURL,
                        headers: that.HEADER,
                        type: 'afn'
                    });
                };

                $(detail).find('.le-server > .les-content > a.player_source').each(async function (index, elm) {
                    var src = $(elm).attr('data-src');
                    if (src.indexOf('/playm3u8_pony/') > 0) {
                        var headersjj = await libs.postHTML({
                            url: src,
                            headers: that.HEADER,
                            type: 'afn',
                            selfsign: true,
                            method: 'head'
                        });
                        var json = JSON.parse(headersjj);
                        if (json.hasOwnProperty('location')) {
                            src = json.location;
                        } else if (json.hasOwnProperty('Location')) {
                            src = json.Location;
                        }
                    };
                    if (src.length > 0) {
                        $.fn.botStream.hostStreams.push({
                            quality: quality,
                            url: src,
                            headers: {
                                referer: detailURL
                            }
                        });
                    }
                });
            }
        };

        //http://m4ufree.fun/search/deadpool-2.html
        //https://streamm4u.net/movies/one-small-hero-1999-watch-online-m4ufree.o7e6i.html
        var m4ufree_fun = {
            DOMAIN: 'm4uhd.tv',
            GENRE: 'all',
            LANGUAGE: 'all',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                'referer': 'https://m4uhd.tv/'
            },
            getHosts: async function (movieInfo, libs) {
                var that = this;
                var keyword = libs.slug(movieInfo.title.toLowerCase());
                var searchURL = 'http://' + this.DOMAIN + '/search/' + keyword + '.html';
                var html = await libs.postHTML({
                    url: searchURL,
                    headers: that.HEADER,
                    type: 'afn'
                });

                var detailURL = '';
                $(html).find('.item').each(async function (index, item) {
                    var title = $(item).find('.imagecover > a').attr('title').toLowerCase().replace('( ', '(').replace(' )', ')').trim();
                    var input = movieInfo.title.trim().toLowerCase();
                    var url = $(item).find('.imagecover > a').attr('href');

                    if (libs.slug(title) == libs.slug(input) || input.indexOf(title) == 0 || title.indexOf(input + ' (tv series ') == 0 || title.indexOf(input + ' (' + movieInfo.year + ')') == 0 || libs.slug(title) == libs.slug(input + ' (' + movieInfo.year + ')') || title.indexOf(input + ' (' + movieInfo.yearFirstSeason + ')') == 0) {
                        detailURL = 'http://' + that.DOMAIN + '/' + url;
                        return true;
                    }
                });

                if (detailURL.length == 0) {
                    return;
                };

                this.HEADER.referer = searchURL;
                var detail = await libs.postHTML({
                    url: detailURL,
                    headers: that.HEADER,
                    type: 'afn'
                });

                var quality = $(detail).find('h3.h3-detail:contains(Quality:) > span').text().trim();
                if (quality == null || quality.length == 0) {
                    quality = '';
                }

                var token = '';
                $(html).each(await function (index, item) {
                    if ($(item).prop('tagName') == 'META' && $(item).attr('name') == 'csrf-token') {
                        token = $(item).attr('content');
                        return false;
                    }
                });

                if (token.length == 0) {
                    return;
                }

                this.HEADER.origin = 'http://' + libs.extractHostname(detailURL);
                var ww = null;
                if (movieInfo.show == false) {
                    ww = detail;
                } else {
                    var episodeId = $(detail).find('button.episode:contains(S' + libs.digitalNumber(movieInfo.season) + '-E' + libs.digitalNumber(movieInfo.episodeNumber) + ')').attr('idepisode');

                    ww = await libs.postHTML({
                        url: 'https://' + that.DOMAIN + '/ajaxtv',
                        headers: that.HEADER,
                        type: 'afn',
                        method: 'post',
                        data: {
                            idepisode: episodeId,
                            '_token': token
                        }
                    });
                };
                $(ww).find('.singlemv').each(async function (index, item) {
                    var data = $(item).attr('data');
                    var ws = await libs.postHTML({
                        url: 'https://' + that.DOMAIN + '/ajax',
                        headers: that.HEADER,
                        type: 'afn',
                        method: 'post',
                        data: {
                            m4u: data,
                            '_token': token
                        }
                    });

                    if ($(ws).find('iframe').length > 0) {
                        $.fn.botStream.hostStreams.push({
                            quality: quality,
                            url: $(ws).find('iframe').attr('src')
                        });
                    } else {
                        ws.split('sources:').some(function (item) {
                            var pp = item.split('],');
                            if (pp.length >= 2) {
                                try {
                                    var streams = eval(pp[0] + ']');
                                    streams.some(function (obj) {
                                        var type = 'mp4';
                                        var cast = true;
                                        if (obj.file.indexOf('.m3u8') > 0) {
                                            type = 'm3u8';
                                        };
                                        libs.postMsg(obj.file, type, libs.getResolution(obj.file), libs.getQuality(quality), 4, cast, false, {
                                            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                                            'Referer': detailURL
                                        });
                                    })
                                } catch (error) {

                                }
                            }
                        });
                    }
                });
            }
        };

        //https://ww1.seehd.uno/
        var seehd_uno = {
            DOMAIN: 'ww1.seehd.uno',
            GENRE: 'all',
            LANGUAGE: 'all',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getHosts: async function (movieInfo, libs) {
                var keyword = movieInfo.title;
                if (movieInfo.show == true) {
                    keyword += ' Season ' + movieInfo.season + ' episode ' + movieInfo.episodeNumber;
                } else {
                    keyword += ' ' + movieInfo.year;
                };
                var slug = libs.slug(keyword);
                var url = 'https://' + this.DOMAIN + '/' + slug + '-watch-online-free/';

                var html = await libs.postHTML({
                    url: url,
                    headers: this.HEADER,
                    type: 'web',
                    script: 'var postMsg = ' + this.postMsg + '; var botScript = ' + this.check_ddos + '; botScript();'
                });

                var json = JSON.parse(html);
                for (var i = 0; i < json.urls.length; i++) {
                    var streamURL = json.urls[i];
                    if (streamURL.indexOf('//') == 0) {
                        streamURL = 'https:' + streamURL.trim();
                    };
                    $.fn.botStream.hostStreams.push({
                        quality: json.title,
                        url: streamURL,
                        referer: url
                    });
                }
            },
            postMsg: function (json) {

                if (typeof jsinterface != 'undefined') {
                    jsinterface.getSomeString(JSON.stringify(json));
                } else if (typeof window != 'undefined' && typeof window.webkit != 'undefined' && window.webkit && window.webkit.messageHandlers) {
                    window.webkit.messageHandlers.callbackHandler.postMessage(JSON.stringify(json));
                } else {
                    window.flutter_inappwebview.callHandler("jsinterface", json);
                }
            },
            check_ddos: function () {
                if (window.location === window.parent.location) {
                    setTimeout(function () {
                        postMsg({});
                    }, 10000);
                    var timer = setInterval(function () {
                        var ifs = document.querySelectorAll('.movieplay > iframe');
                        var a = document.querySelector('.elemento > a');
                        if (document.querySelector('title').innerText !== 'Just a moment...' && ifs.length > 0 && a != null) {
                            clearInterval(timer);
                            var results = {
                                title: '',
                                urls: []
                            };
                            if (a != null) {
                                results.urls.push(a.getAttribute('href'));
                                var q = a.querySelector('.d');
                                if (q != null) {
                                    results.title = a.querySelector('.d').innerText;
                                }
                            };
                            if (ifs != null) {
                                for (var i = 0; i < ifs.length; i++) {
                                    results.urls.push(ifs[i].getAttribute('src'));
                                };
                            }

                            postMsg(results);
                        }
                    }, 100);
                };
            }
        };

        //https://gogoanime.pe/naruto-shippuuden-dub-episode-500
        var gogoanime_io = {
            DOMAIN: 'gogoanime.pe',
            GENRE: 'animation',
            LANGUAGE: 'japanese',
            COUNTRY: 'japan',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getHosts: async function (movieInfo, libs) {
                if (movieInfo.episodeNumberSeasons == undefined) {
                    return;
                }

                var urls = [];
                if (movieInfo.show == true) {
                    if (movieInfo.sourceURL.length > 0) {
                        urls.push(movieInfo.sourceURL + '-episode-' + movieInfo.episodeNumberSeasons);
                        urls.push(movieInfo.sourceURL + '-dub-episode-' + movieInfo.episodeNumberSeasons);
                    } else {
                        urls.push('https://' + this.DOMAIN + '/' + libs.slug((movieInfo.title + ' dub').toLowerCase()) + '-episode-' + movieInfo.episodeNumberSeasons);
                        urls.push('https://' + this.DOMAIN + '/' + libs.slug(movieInfo.title.toLowerCase()) + '-episode-' + movieInfo.episodeNumberSeasons);
                    }
                } else {
                    if (movieInfo.sourceURL.length > 0) {
                        urls.push(movieInfo.sourceURL + '-episode-1');
                    } else {
                        urls.push('https://' + this.DOMAIN + '/' + libs.slug(movieInfo.title.toLowerCase()) + '-episode-1');
                    }
                }
                for (var i = 0; i < urls.length; i++) {
                    var html = await libs.postHTML({
                        url: urls[i],
                        headers: this.HEADER,
                        type: 'afn'
                    });

                    var title = '';
                    $(html).each(await function (index, item) {
                        if ($(item).prop("tagName") == 'TITLE') {
                            title = $(item).text();
                            return false;
                        }
                    });
                    var quality = 'HD';
                    if (title.toLowerCase().indexOf('dub') > 0) {
                        quality = 'HD-DUB';
                    }
                    $(html).find('.anime_muti_link > ul > li > a').each(async function (index, item) {
                        var href = libs.getAbsoluteUrl($(item).attr('data-video'));
                        if (href.length > 0) {
                            $.fn.botStream.hostStreams.push({
                                quality: quality,
                                url: href
                            });
                        }
                    });
                }
            }
        };

        //https://www.gogoanime1.com/watch/hunter-x-hunter
        var gogoanime1_com = {
            DOMAIN: 'www.gogoanime1.com',
            GENRE: 'animation',
            LANGUAGE: 'japanese',
            COUNTRY: 'japan',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getHosts: async function (movieInfo, libs) {
                if (movieInfo.episodeNumberSeasons == undefined) {
                    return;
                };

                var that = this;
                var detailURL = '';
                if (movieInfo.sourceURL.length == 0) {
                    var searchURL = 'https://' + this.DOMAIN + '/search/topSearch?q=' + encodeURIComponent(movieInfo.title);
                    var jj = await libs.postHTML({
                        url: searchURL,
                        headers: this.HEADER,
                        type: 'afn'
                    });

                    var json = JSON.parse(jj);
                    if (json.status != 1) {
                        return;
                    };

                    json.data.some(function (item) {
                        if (item.name.toLowerCase() == movieInfo.title.toLowerCase() ||
                            item.name.toLowerCase() == movieInfo.originalTitle.toLowerCase()) {
                            detailURL = 'https://' + that.DOMAIN + '/watch/' + item.seo_name;
                            return true;
                        }
                    });
                } else {
                    detailURL = movieInfo.sourceURL;
                }

                if (detailURL.length == 0) {
                    return;
                };

                var episodeURL = '';
                var count = 0;
                var total = 0;
                do {
                    if (count++ == 0) {
                        episodeURL = detailURL + '/episode/episode-' + movieInfo.episodeNumberSeasons;
                    };

                    var html = await libs.postHTML({
                        url: episodeURL,
                        headers: this.HEADER,
                        type: 'afn'
                    });

                    var nav = $(html).find('.sd-nav > a[rel="nofollow"]');
                    total = nav.length;

                    var quality = 'HD';
                    if ($(html).find('.sd-nav > a.active[rel="nofollow"]').text().toLowerCase().indexOf('dubbed') > 0) {
                        quality = 'HD-DUB';
                    };

                    var file = $(html).find('.vmn-buttons > a[type="application/octet-stream"]').attr('href').replace(new RegExp('[ ]', 'ig'), '%20');

                    var type = 'mp4';
                    var cast = true;
                    if (file.indexOf('m3u8') > 0) {
                        type = 'm3u8';
                    };
                    libs.postMsg(file, type, libs.getResolution(file), libs.getQuality(quality), 3, cast, false, {
                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                        'Referer': episodeURL,
                    });
                    if (total > 0) {
                        episodeURL = $(nav[count]).attr('href');
                    }
                } while (count < total);
            }
        };

        //https://gogoanime.pro/anime/one-piece-dub-34r
        var gogoanime_pro = {
            DOMAIN: 'gogoanime.pro',
            GENRE: 'animation',
            LANGUAGE: 'all',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36'
            },
            getHosts: async function (movieInfo, libs) {
                if (movieInfo.episodeNumberSeasons == undefined) {
                    return;
                };

                var that = this;
                var detailURLs = [];
                if (movieInfo.sourceURL.length == 0) {
                    var searchjj = await libs.postHTML({
                        url: 'https://' + this.DOMAIN,
                        headers: this.HEADER,
                        type: 'web',
                        script: 'var postMsg=' + that.postMsg + ';var botScript = ' + that.home + ';botScript("' + movieInfo.title + '");'
                    });

                    var json = JSON.parse(searchjj);
                    var search = json.html;

                    var keywords = [];
                    if (movieInfo.show == true && movieInfo.season > 0) {
                        keywords.push(movieInfo.title.toLowerCase() + ' season ' + movieInfo.season);
                        keywords.push(movieInfo.title.toLowerCase() + ' season ' + movieInfo.season + ' dub');
                    };
                    keywords.push(movieInfo.title.toLowerCase());
                    keywords.push(movieInfo.title.toLowerCase() + ' dub');

                    $(search).find('.last_episodes > ul.items > li > p.name > a').each(function (index, item) {
                        keywords.some(function (keyword) {
                            if (movieInfo.show == true && $(item).text().toLowerCase().indexOf('movie') > 0) {
                                return true;
                            }
                            var year = $(item).parents('li').find('p.episode').text().replace('Released ', '');
                            if (libs.slug($(item).text().toLowerCase()).indexOf(libs.slug(keyword)) >= 0 && (year == movieInfo.year || year == movieInfo.yearFirstSeason)) {
                                detailURLs.push('https://' + that.DOMAIN + $(item).attr('href'));
                            }
                        });
                    });
                } else {
                    detailURLs.push(movieInfo.sourceURL);
                }

                if (detailURLs.length == 0) {
                    return;
                };

                var matchSeason = false;
                for (var i = 0; i < detailURLs.length; i++) {
                    if (detailURLs[i].indexOf('-season-' + movieInfo.season) > 0) {
                        matchSeason = true;
                        break;
                    }
                }

                for (var j = 0; j < detailURLs.length; j++) {
                    var detailURL = detailURLs[j];
                    if (matchSeason == true && detailURL.indexOf('-season-' + movieInfo.season) < 0) {
                        continue;
                    }

                    var episodeNumber = 0;
                    if (movieInfo.show == true) {
                        episodeNumber = movieInfo.episodeNumberSeasons;
                        if (detailURL.indexOf('-season-' + movieInfo.season) > 0) {
                            episodeNumber = movieInfo.episodeNumber;
                        };

                        detailURL = detailURL + '/ep-' + episodeNumber;
                    };

                    that.HEADER.referer = detailURL;
                    var jj = await libs.postHTML({
                        url: detailURL,
                        headers: that.HEADER,
                        type: 'web',
                        script: 'var postMsg=' + that.postMsg + ';var botScript = ' + that.check_ddos + ';botScript(' + episodeNumber + ');'
                    });

                    var quality = 'HD';
                    if (detailURL.indexOf('-dub-') > 0) {
                        quality = 'HD-DUB';
                    };

                    JSON.parse(jj).some(function (item) {
                        $.fn.botStream.hostStreams.push({
                            quality: quality,
                            url: item,
                            referer: detailURL
                        });
                    });
                }
            },
            postMsg: function (json) {

                if (typeof jsinterface != 'undefined') {
                    jsinterface.getSomeString(JSON.stringify(json));
                } else if (typeof window != 'undefined' && typeof window.webkit != 'undefined' && window.webkit && window.webkit.messageHandlers) {
                    window.webkit.messageHandlers.callbackHandler.postMessage(JSON.stringify(json));
                } else {
                    window.flutter_inappwebview.callHandler("jsinterface", json);
                }
            },
            home: function (keyword) {
                if (window.location === window.parent.location) {
                    setTimeout(function () {
                        postMsg({});
                    }, 10000);

                    var timer = setInterval(function () {
                        if (window.location.href.indexOf('/search?') > 0) {
                            postMsg({
                                html: document.body.innerHTML
                            });
                            clearInterval(timer);
                        } else {
                            if (document.querySelector('title').innerText !== 'Just a moment...') {
                                if ($('input[name=keyword').val().length == 0) {
                                    $('input[name=keyword').val(keyword);
                                    $('input[name=keyword').keyup();
                                };
                                if ($('.suggestions > .more').length > 0) {
                                    window.location = 'https://gogoanime.pro' + $('.suggestions > .more').attr('href');
                                    clearInterval(timer);
                                }
                            }
                        }

                    }, 100);
                }
            },
            check_ddos: function (episodeNumber) {
                if (window.location === window.parent.location) {
                    var result = [];
                    setTimeout(function () {
                        postMsg(result);
                    }, 10000);
                    var timer = setInterval(function () {
                        if (document.querySelector('title').innerText !== 'Just a moment...' && document.querySelector('.anime_video_body > #episodes > ul > li') != null) {
                            var episodeName = document.querySelector('.anime_video_body > #episodes > ul > li > a.active').innerHTML.trim();
                            if (episodeNumber == 0 || episodeName.indexOf('EP ' + episodeNumber + '\n') >= 0 || episodeName.indexOf('EP ' + episodeNumber) >= 0) {
                                clearInterval(timer);
                                var haveStream = true;
                                var items = $('.anime_muti_link > ul > li > a');
                                var index = 0;
                                var timer1 = setInterval(function () {
                                    if (haveStream == true && items.length > index) {
                                        $(items[index++]).click();
                                        haveStream = false;
                                    }
                                }, 100);

                                var timer2 = setInterval(function () {
                                    if ($('.play-video > iframe').length > 0 && haveStream == false) {
                                        result.push($('.play-video > iframe').attr('src'));
                                        $('.play-video > iframe').remove();
                                        haveStream = true;
                                        if (result.length == 3) {
                                            clearInterval(timer2);
                                            clearInterval(timer1);
                                            postMsg(result);
                                        }
                                    }
                                }, 100);
                            }
                        }
                    }, 100);
                };
            }
        };

        //http://cartoonextra.in/toon/search?key=KAIJUDO%3A+RISE+OF+THE+DUEL+MASTERS
        var cartoonextra_in = {
            DOMAIN: 'cartoonextra.in',
            GENRE: 'animation',
            LANGUAGE: 'all',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getHosts: async function (movieInfo, libs) {
                var that = this;
                var detailURL = '';
                var title = movieInfo.title;
                var sluged = libs.slug(title);
                var urlPattern = 'http://' + this.DOMAIN + '/' + sluged;

                if (movieInfo.sourceURL.length == 0) {
                    var searchURL = 'http://' + this.DOMAIN + '/toon/search?key=' + encodeURIComponent(title.toLowerCase()).replace(/%20/g, '+');
                    this.HEADER.referer = 'http://' + this.DOMAIN;
                    var html = await libs.postHTML({
                        url: searchURL,
                        headers: this.HEADER,
                        type: 'afn'
                    });
                    $(html).find('.cartoon-box > a.image').each(async function (index, item) {
                        if (detailURL.length == 0) {
                            var url = $(item).attr('href');
                            var year = parseInt($(item).parent().find('.mb-right > .detail:contains(Released:)').text().replace('Released:', '').trim());
                            if ((url == urlPattern && movieInfo.show == true && movieInfo.yearFirstSeason == year) || (movieInfo.show == false && movieInfo.year == year && (url == urlPattern + '-movie' || url == urlPattern + '-' + year + '-movie'))) {
                                detailURL = url;
                                return false;
                            };
                        }
                    });
                } else {
                    detailURL = movieInfo.sourceURL;
                };

                if (detailURL.length == 0) {
                    return;
                };

                var quality = 'HD';

                var episodeURL = urlPattern;
                if (movieInfo.show == true) {
                    if (movieInfo.season > 1) {
                        episodeURL = episodeURL + '-season-' + movieInfo.season;
                    };
                    episodeURL = episodeURL + '-episode-' + movieInfo.episodeNumber;
                } else {
                    var detail = await libs.postHTML({
                        url: detailURL,
                        headers: that.HEADER,
                        type: 'afn'
                    });
                    episodeURL = $(detail).find('#list a').attr('href');
                };
                var episode = await libs.postHTML({
                    url: episodeURL,
                    headers: that.HEADER,
                    type: 'afn'
                });
                var arr = [];
                var episodeLink = $(episode).find('.list-episode > li.episode > a.episode-link');
                for (var i = 0; i < episodeLink.length; i++) {
                    var epItem = episodeLink[i];
                    var episodeTmp;
                    if (i == 0) {
                        episodeTmp = episode;
                    } else {
                        episodeTmp = await libs.postHTML({
                            url: $(epItem).attr('href'),
                            headers: that.HEADER,
                            type: 'afn'
                        });
                    }

                    var streamURL = $(episodeTmp).find('#streams iframe').attr('src');
                    if (jQuery.inArray(streamURL, arr) < 0) {
                        arr.push(streamURL);
                    }
                };

                $.each(arr, async function (i, streamURL) {
                    var stream = await libs.postHTML({
                        url: streamURL,
                        headers: that.HEADER,
                        type: 'afn'
                    });

                    var match = /video_links(.*?);/ig.exec(stream);
                    if (match != null && match.length > 0) {
                        var json = JSON.parse(match[1].replace('=', '').trim());
                        json.normal.storage.some(function (obj) {
                            var type = 'mp4';
                            var cast = true;
                            if (obj.link.indexOf('.m3u8') > 0) {
                                type = 'm3u8';
                            };
                            libs.postMsg(obj.link, type, libs.getResolution(obj.link), libs.getQuality(quality), 4, cast, false, {
                                'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                                'Referer': streamURL
                            });
                        });
                    }
                })
            }
        };

        //https://fmovies.taxi/film/dirilis-ertugrul.mvon8/9o9lzzq
        //https://ffmovies.ru/film/invasion.9ozon/zlvlnkp
        //https://fmovies.to/film/invasion.9ozon/oj583z5
        //https://movies7.to/?f
        //fmovies.to
        // fmovies.wtf
        // fmovies.taxi
        // fmovies.pub
        // fmovies.cafe
        // fmovies.love
        // fmovies.world
        // fmovies.media
        // fmovies.coffee
        // fmovies.solar
        // flixtor.video
        // myflixer.ru
        var ffmovies_ru = {
            DOMAIN: 'fmovies.taxi',
            GENRE: 'all',
            LANGUAGE: 'all',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36'
            },
            getHosts: async function (movieInfo, libs) {
                var that = this;
                var detailURL = '';

                if (movieInfo.sourceURL.length == 0) {
                    var searchjj = await libs.postHTML({
                        url: 'https://' + this.DOMAIN,
                        headers: this.HEADER,
                        type: 'web',
                        script: 'var postMsg=' + that.postMsg + ';var botScript = ' + that.home + ';botScript("' + movieInfo.title + '");'
                    });

                    var json = JSON.parse(searchjj);
                    var search = json.html;

                    var movielist = $(search).find('.filmlist .item');
                    for (var i = 0; i < movielist.length; i++) {
                        var item = movielist[i];
                        var title = $(item).find('a.title').text().toLowerCase();
                        if (libs.slug(title) == libs.slug(movieInfo.title.toLowerCase())) {
                            that.HEADER.referer = 'https://' + this.DOMAIN;
                            that.HEADER['x-requested-with'] = 'XMLHttpRequest';
                            var tooptip = await libs.postHTML({
                                url: 'https://' + that.DOMAIN + '/ajax/film/tooltip/' + $(item).attr('data-tip'),
                                headers: that.HEADER,
                                type: 'afn'
                            });

                            var year = $(tooptip).find('span:eq(1)').html();
                            if ((movieInfo.show == true && year != movieInfo.yearFirstSeason) ||
                                (movieInfo.show == false && year != movieInfo.year)) {
                                continue
                            };

                            detailURL = 'https://' + that.DOMAIN + $(item).find('a.title').attr('href');
                            break;
                        }
                    }
                } else {
                    detailURL = movieInfo.sourceURL;
                }
                if (detailURL.length == 0) {
                    return;
                };

                that.HEADER.referer = detailURL;
                var jj = await libs.postHTML({
                    url: detailURL,
                    headers: that.HEADER,
                    type: 'web',
                    script: 'var postMsg=' + that.postMsg + ';var botScript = ' + that.check_ddos + ';botScript(' + movieInfo.season + ',' + movieInfo.episodeNumber + ');'
                });

                JSON.parse(jj).some(function (item) {
                    $.fn.botStream.hostStreams.push({
                        quality: item.quality,
                        url: item.src
                    });
                });
            },
            postMsg: function (json) {

                if (typeof jsinterface != 'undefined') {
                    jsinterface.getSomeString(JSON.stringify(json));
                } else if (typeof window != 'undefined' && typeof window.webkit != 'undefined' && window.webkit && window.webkit.messageHandlers) {
                    window.webkit.messageHandlers.callbackHandler.postMessage(JSON.stringify(json));
                } else {
                    window.flutter_inappwebview.callHandler("jsinterface", json);
                }
            },
            home: function (keyword) {
                if (window.location === window.parent.location) {
                    setTimeout(function () {
                        postMsg({});
                    }, 10000);

                    var timer = setInterval(function () {
                        if (window.location.href.indexOf('/search?') > 0) {
                            postMsg({
                                html: document.body.innerHTML
                            });
                            clearInterval(timer);
                        } else {
                            $('input[name=keyword').val(keyword);
                            $('input[name=keyword').keydown();
                            var vrf = $('input[name=vrf]').val();
                            if (document.querySelector('title').innerText !== 'Just a moment...' && vrf.length > 0) {
                                $('#search').submit();
                                clearInterval(timer);
                            }
                        }

                    }, 100);
                }
            },
            check_ddos: function (seasonNumber, episodeNumber) {
                if (window.location === window.parent.location) {
                    var result = [];
                    var sources = [];
                    setTimeout(function () {
                        postMsg(result);
                    }, 10000);

                    var timer = setInterval(function () {
                        var show = true;
                        if (seasonNumber == 0 && episodeNumber == 0) {
                            show = false;
                        };

                        if (document.querySelector('title').innerText !== 'Just a moment...' && document.querySelector('.episodes > li > a') != null) {
                            var episodes;
                            if (show == true) {
                                episodes = $('.episodes[data-season=' + seasonNumber + '] > li > a:contains("Episode ' + episodeNumber + ': ")');
                            } else {
                                episodes = $('.episodes > li > a');
                            }
                            if (episodes != null && episodes.length > 0) {
                                clearInterval(timer);

                                episodes.each(function (index, item) {
                                    var haveStream = true;
                                    var timer1 = setInterval(function () {
                                        if (haveStream == true) {
                                            haveStream = false;
                                            $(item).click();
                                        }
                                    }, 100);

                                    var timer2 = setInterval(function () {
                                        if ($('#player > iframe').length > 0 && haveStream == false) {
                                            haveStream = true;
                                            var src = $('#player > iframe').attr('src');
                                            if (jQuery.inArray(src, sources) == -1) {
                                                sources.push(src);
                                                result.push({
                                                    src: src,
                                                    quality: document.querySelector('.meta > .quality').innerText
                                                });
                                                $('#player > iframe').remove();
                                                if (episodes.length == index + 1) {
                                                    clearInterval(timer2);
                                                    clearInterval(timer1);
                                                    postMsg(result);
                                                }
                                            }
                                        }
                                    }, 100);
                                })
                            };
                        }
                    }, 100);
                };
            }
        };

        //https://fmoviesto.cc/watch-movie/zack-snyders-justice-league-2021-full-67922.4137873
        //https://www.tinyzone.tv/search/joker
        //https://www1.tinyzone.tv/watch-movie/watch-bad-boys-for-life-2020-free-525.1364355
        //https://www1.himovies.to/watch-movie/bad-boys-for-life-525.1364355
        var tinyzone_tv = {
            DOMAIN: 'tinyzonetv.to',
            GENRE: 'all',
            LANGUAGE: 'all',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                'referer': 'https://tinyzonetv.to'
            },
            getHosts: async function (movieInfo, libs) {
                var that = this;
                var detailURL = '';

                if (movieInfo.sourceURL.length == 0) {
                    var input = movieInfo.title.trim().toLowerCase();
                    var keyword = libs.slug(input);
                    var searchURL = 'https://' + that.DOMAIN + '/search/' + keyword;
                    var html = await libs.postHTML({
                        url: searchURL,
                        headers: that.HEADER,
                        type: 'afn'
                    });

                    $(html).find('.flw-item > .film-detail a').each(async function (index, item) {
                        urlPattern = '';
                        if (movieInfo.show == true) {
                            urlPattern = '/tv/watch-' + keyword + '-' + movieInfo.yearFirstSeason;
                        } else {
                            urlPattern = '/movie/watch-' + keyword + '-' + movieInfo.year;
                        }
                        var href = $(item).attr('href');
                        if (href.indexOf(urlPattern) == 0) {
                            detailURL = 'https://' + that.DOMAIN + href;
                            return true;
                        }
                    });
                } else {
                    detailURL = movieInfo.sourceURL;
                };

                if (detailURL.length == 0) {
                    return;
                };

                var parts = detailURL.split('-');
                var id = parts[parts.length - 1];
                var episodeURL = null;
                if (movieInfo.show == true) {
                    var seasonURL = 'https://' + that.DOMAIN + '/ajax/v2/tv/seasons/' + id;
                    this.HEADER.origin = 'http://' + libs.extractHostname(detailURL);
                    this.HEADER.referer = detailURL;
                    var season = await libs.postHTML({
                        url: seasonURL,
                        headers: that.HEADER,
                        type: 'afn'
                    });

                    var ss = $(season).find('a:contains("Season ' + movieInfo.season + '")');
                    if (ss.length == 0) {
                        ss = $(season).find('a:contains("Series ' + movieInfo.season + '")');
                        if (ss.length == 0) {
                            return;
                        }
                    }
                    var seasonId = $(ss[0]).attr('data-id');

                    episodeURL = 'https://' + that.DOMAIN + '/ajax/v2/season/episodes/' + seasonId;
                } else {
                    episodeURL = 'https://' + that.DOMAIN + '/ajax/movie/episodes/' + id;
                }

                this.HEADER.origin = 'http://' + libs.extractHostname(detailURL);
                this.HEADER.referer = detailURL;
                var episode = await libs.postHTML({
                    url: episodeURL,
                    headers: that.HEADER,
                    type: 'afn'
                });

                var detailURLs = [];
                var tagAs = $(episode).find('a.nav-link');
                for (var i = 0; i < tagAs.length; i++) {
                    var item = tagAs[i];
                    if (movieInfo.show == true) {
                        if ($(item).text().replace(new RegExp('[ \n]', 'ig'), '').indexOf('Eps' + movieInfo.episodeNumber + ':') < 0) {
                            continue;
                        };

                        var epId = $(item).attr('data-id');
                        var serverURL = 'https://' + that.DOMAIN + '/ajax/v2/episode/servers/' + epId;
                        var server = await libs.postHTML({
                            url: serverURL,
                            headers: that.HEADER,
                            type: 'afn'
                        });
                        var servers = $(server).find('a.nav-link');
                        for (var j = 0; j < servers.length; j++) {
                            detailURLs.push(detailURL.replace('/tv/', '/watch-tv/') + '.' + $(servers[j]).attr('data-id'));
                        }
                    } else {
                        detailURLs.push('https://' + that.DOMAIN + $(item).attr('href'));
                    }
                };
                detailURLs.forEach(async function (detailURL) {
                    var detail = await libs.postHTML({
                        url: detailURL,
                        headers: that.HEADER,
                        type: 'web',
                        script: 'var postMsg=' + that.postMsg + ';var botScript = ' + that.check_ddos + ';botScript();'
                    });

                    var quality = $(detail).find('.btn-quality').text();
                    var tt = $(detail).find('video');

                    if (tt.length > 0) {
                        var file = tt.attr('src');
                        var type = 'mp4';
                        var cast = true;
                        if (file.indexOf('.m3u8') > 0) {
                            type = 'm3u8';
                        };
                        libs.postMsg(file, type, libs.getResolution(file), libs.getQuality(quality), 5, cast, false, {
                            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                            'Referer': episodeURL
                        });
                    } else {
                        var iframe = $(detail).find('#iframe-embed');
                        $.fn.botStream.hostStreams.push({
                            quality: quality,
                            url: iframe.attr('src'),
                            referer: detailURL
                        });
                    }
                });
            },
            postMsg: function (html) {

                if (typeof jsinterface != 'undefined') {
                    jsinterface.getSomeString(html);
                } else if (typeof window != 'undefined' && typeof window.webkit != 'undefined' && window.webkit && window.webkit.messageHandlers) {
                    window.webkit.messageHandlers.callbackHandler.postMessage(html);
                } else {
                    window.flutter_inappwebview.callHandler("jsinterface", html);
                }
            },
            check_ddos: function () {
                if (window.location === window.parent.location) {
                    setTimeout(function () {
                        postMsg('');
                    }, 20000);
                    var timer = setInterval(function () {
                        if (document.querySelector('title').innerText !== 'Just a moment...' && (document.querySelector('video') != null || (document.querySelector('#iframe-embed') != null && document.querySelector('#iframe-embed').getAttribute('src') != null && document.querySelector('#iframe-embed').getAttribute('src').length > 0))) {
                            clearInterval(timer);
                            var html = document.querySelector('body').innerHTML;
                            postMsg(html);
                        }
                    }, 100);
                };
            }
        };

        // https://v2.apimdb.net/e/movie/tt0120667
        var apimdb_net = {
            DOMAIN: 'openvids.io',
            GENRE: 'all',
            LANGUAGE: 'all',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                if (typeof hostInfo.filmImdb == 'undefined' || hostInfo.filmImdb.length == 0) {
                    return;
                };
                var that = this;
                var url = '', serverUrl = '';
                this.HEADER.referer = 'https://' + this.DOMAIN + '/';
                if (hostInfo.show == false) {
                    url = 'https://' + this.DOMAIN + '/api/movie.json?imdb=' + hostInfo.filmImdb;
                    serverUrl = 'https://' + this.DOMAIN + '/api/servers.json?imdb=' + hostInfo.filmImdb;
                } else {
                    url = 'https://' + this.DOMAIN + '/api/episode.json?imdb=' + hostInfo.filmImdb + '&season=' + hostInfo.season + '&episode=' + hostInfo.episodeNumber;
                    serverUrl = 'https://' + this.DOMAIN + '/api/servers.json?imdb=' + hostInfo.filmImdb;
                }

                var jj = await libs.postHTML({
                    url: url,
                    headers: this.HEADER,
                    type: 'afn'
                });

                var json = JSON.parse(jj);
                if (!json.ok) {
                    return;
                }

                this.HEADER.title = json.title;
                this.HEADER.year = new Date(json.releasedAt).getFullYear().toString();
                this.HEADER.updatedat = json.updatedAt;
                this.HEADER.authority = this.DOMAIN;
                if (hostInfo.show) {
                    this.HEADER.s = hostInfo.season.toString();
                    this.HEADER.e = hostInfo.episodeNumber.toString();
                }
                var jj2 = await libs.postHTML({
                    url: serverUrl,
                    headers: this.HEADER,
                    type: 'afn'
                });

                json = JSON.parse(jj2);
                if (!json.ok) {
                    return;
                }

                var servers = [];
                try {
                    if (json.servers.hasOwnProperty("doodstream")) {
                        servers.push('https://dood.pm/d/' + json.servers.doodstream.code);
                    };
                    if (json.servers.hasOwnProperty("streamsb")) {
                        servers.push('https://sbembed.com/e/' + json.servers.streamsb.code);
                    };
                    if (json.servers.hasOwnProperty("voxzer")) {
                        servers.push('https://player.voxzer.org/view/' + json.servers.voxzer.code);
                    };
                    if (json.servers.hasOwnProperty("vidcloud")) {
                        servers.push('https://membed.net/streaming.php?id=' + json.servers.vidcloud.code);
                    };
                    if (json.servers.hasOwnProperty("mixdrop")) {
                        servers.push('https://mixdrop.co/f/' + json.servers.mixdrop.code);
                    };
                    if (json.servers.hasOwnProperty("voe")) {
                        servers.push('https://voe.sx/d/' + json.servers.mixdrop.code);
                    }
                } catch (error) {

                }

                servers.some(function (item) {
                    $.fn.botStream.hostStreams.push({
                        quality: '',
                        url: item
                    });
                });
            }
        };

        // https://2embed.biz/play/movie.php?imdb=tt5113044
        var _2embed_biz = {
            DOMAIN: '2embed.biz',
            GENRE: 'all',
            LANGUAGE: 'all',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36'
            },
            getStream: async function (hostInfo, libs) {
                if (typeof hostInfo.filmImdb == 'undefined' || hostInfo.filmImdb.length == 0) {
                    return;
                };

                var that = this;
                var url = '';
                if (hostInfo.show == false) {
                    url = 'https://' + this.DOMAIN + '/play/movie.php?imdb=' + hostInfo.filmImdb;
                } else {
                    url = 'https://' + this.DOMAIN + '/embed/tv?imdb=' + hostInfo.filmImdb + '&s=' + hostInfo.season + '&e=' + hostInfo.episodeNumber;
                }

                this.HEADER.referer = 'https://' + that.DOMAIN + '/';
                var html = await libs.postHTML({
                    url: url,
                    headers: that.HEADER,
                    type: 'afn'
                });

                var parser = new DOMParser();
                var doc = parser.parseFromString(html, "text/html");
                var file = 'https://' + this.DOMAIN + '/play/' + doc.querySelector('video > source').getAttribute('src');

                var type = 'mp4';
                if (file.indexOf('m3u8') > 0) {
                    type = 'm3u8';
                };

                this.HEADER.referer = url;
                var headersjj = await libs.postHTML({
                    url: file,
                    headers: that.HEADER,
                    type: 'afn',
                    selfsign: true,
                    method: 'head'
                });

                var json = JSON.parse(headersjj);
                if (json.hasOwnProperty('location')) {
                    file = json.location;
                } else if (json.hasOwnProperty('Location')) {
                    file = json.Location;
                }

                libs.postMsg(file, type, libs.getResolution(file), libs.getQuality(file), 4, false, false, {
                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                    'Referer': url,
                });
            }
        };

        // https://123player.4u.ms/?imdb_id=tt1520211&s=1&e=1
        // https://www.2embed.ru/embed/imdb/tv?id=tt4052886&s=5&e=15
        // https://www.2embed.ru/embed/tmdb/tv?id=86430&s=1&e=5
        // https://www.2embed.ru/embed/imdb/tv?id=tt1520211&s=1&e=5
        // https://2embed.org/
        var player123_4u_ms = {
            DOMAIN: '2embed.org',
            GENRE: 'all',
            LANGUAGE: 'all',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36'
            },
            getStream: async function (hostInfo, libs) {
                if (typeof hostInfo.filmImdb == 'undefined' || hostInfo.filmImdb.length == 0) {
                    return;
                };

                var that = this;
                var url = '';
                if (hostInfo.show == false) {
                    url = 'https://' + this.DOMAIN + '/embed/movie?imdb=' + hostInfo.filmImdb;
                } else {
                    url = 'https://' + this.DOMAIN + '/embed/tv?imdb=' + hostInfo.filmImdb + '&s=' + hostInfo.season + '&e=' + hostInfo.episodeNumber;
                }

                this.HEADER.referer = 'https://' + that.DOMAIN + '/';
                var html = await libs.postHTML({
                    url: url,
                    headers: that.HEADER,
                    type: 'afn'
                });

                var parser = new DOMParser();
                var doc = parser.parseFromString(html, "text/html");
                var movieId = doc.querySelector('#embed-player').getAttribute('data-movie-id');

                $(html).find('div.dropdown-menu > .server').each(async function (index, item) {
                    var dataId = $(item).attr('data-id');
                    url = 'https://' + that.DOMAIN + '/ajax/get_stream_link?id=' + dataId + '&movie=' + movieId + '&is_init=false&captcha=&ref=';
                    var jj = await libs.postHTML({
                        url: url,
                        headers: this.HEADER,
                        type: 'afn'
                    });

                    var json = JSON.parse(jj);
                    if (json.success) {
                        var link = json.data.link;
                        $.fn.botStream.hostStreams.push({
                            quality: '',
                            url: link
                        });
                    }
                })

                // var jj = await libs.postHTML({
                //     url: url,
                //     headers: this.HEADER,
                //     type: 'web',
                //     script: 'var postMsg='+that.postMsg+';var botScript = ' + that.getVideoTag + ';botScript();'
                // });
                // var json = JSON.parse(jj);
                // json.some(function(stream){
                //     var type = 'mp4';
                //     if(stream.file.indexOf('m3u8') > 0){
                //         type = 'm3u8';
                //     };
                //     if(stream.file.indexOf('https:') <0) {
                //         stream.file = 'https://' + that.DOMAIN + stream.file;
                //     }
                //     libs.postMsg(stream.file, type, libs.getResolution(stream.file), libs.getQuality(stream.file), 4, true, false, {
                //         'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                //         'Referer': url,
                //     });
                // })
            },
            postMsg: function (json) {

                if (typeof jsinterface != 'undefined') {
                    jsinterface.getSomeString(JSON.stringify(json));
                } else if (typeof window != 'undefined' && typeof window.webkit != 'undefined' && window.webkit && window.webkit.messageHandlers) {
                    window.webkit.messageHandlers.callbackHandler.postMessage(JSON.stringify(json));
                } else {
                    window.flutter_inappwebview.callHandler("jsinterface", json);
                }
            },
            getVideoTag: function () {
                if (window.location === window.parent.location) {
                    setTimeout(function () {
                        postMsg({});
                    }, 15000);
                    if (window.location.href.indexOf('2embed.ru') > 0) {
                        setTimeout(function () {
                            $('#play-now').click();
                            var timer = setInterval(function () {
                                var iframe = $('#iframe-embed');
                                if (iframe.length > 0 && iframe.attr('src').length > 0) {
                                    clearInterval(timer);
                                    window.location.href = iframe.attr('src');
                                }
                            }, 100);
                        }, 5000);
                    } else if (window.location.href.indexOf('streamrapid.ru') > 0) {
                        setInterval(function () {
                            var botstreamplayer = null;
                            if (typeof jwplayer !== 'undefined' && typeof jwplayer().getPlaylist() !== 'undefined' && jwplayer().getPlaylist().length > 0 && jwplayer().getPlaylist()[0].sources.length > 0) {
                                botstreamplayer = jwplayer();
                                var sources = botstreamplayer.getPlaylist()[0].sources;
                                var results = [];
                                for (var i = 0; i < sources.length; i++) {
                                    if (typeof sources[i].file !== 'undefined') {
                                        results.push({
                                            file: sources[i].file,
                                            label: ''
                                        });
                                    }
                                }
                                postMsg(results);
                            };
                        }, 100)
                    }
                }
            }
        };


        // https://api.hdv.fun/embed/tt7286456
        // https://vikv.net/watch/joker-2019/
        // https://hls.hdv.fun/imdb/tt7286456
        // var api_hdv_fun = {
        //     DOMAIN: 'hls.hdv.fun',
        //     GENRE: 'all',
        //     LANGUAGE: 'all',
        //     HEADER: {
        //         'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
        //     },
        //     getStream : async function(hostInfo, libs){
        //         if(hostInfo.show == true || typeof hostInfo.filmImdb == 'undefined' || hostInfo.filmImdb.length == 0) {
        //             return;
        //         };
        //         var that = this;
        //         var url = 'https://' + this.DOMAIN + '/imdb/' + hostInfo.filmImdb;
        //         var html = await libs.postHTML({
        //             url: url,
        //             headers: this.HEADER,
        //             type: 'afn'
        //         });

        //         var arr = [];
        //         var hdvuser = '';
        //         var match = /var hd=(.+)/gi.exec(html);
        //         if(match == null || match.length < 2) {
        //             return;
        //         };

        //         arr = arr.concat(eval(match[1]));

        //         match = /var sd=(.+)/gi.exec(html);
        //         if(match != null && match.length >= 2) {
        //             arr = arr.concat(eval(match[1]));
        //         };

        //         match = /var hdv_user="(.+)"/gi.exec(html);
        //         if(match != null && match.length >= 2) {
        //             hdvuser = match[1];
        //         };

        //         for(var i=0;i<arr.length;i++){
        //             var name = arr[i].name;
        //             var res = arr[i].res;
        //             var quality = arr[i].quality;
        //             var src = "https://hls.hdv.fun/m3u8/" + name + ".m3u8?u=" + btoa(this.o(btoa(this.o(Math.random().toString(36).slice(-10) + hdvuser))));

        //             var type = 'mp4';
        //             var cast = true;
        //             if(src.indexOf('.m3u8') > 0){
        //                 type = 'm3u8';
        //                 cast = false;
        //             };
        //             libs.postMsg(src, type, res, libs.getQuality(quality), 3, cast, false, {
        //                 'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
        //                 'Origin': 'https://' + libs.extractHostname(url)
        //             });
        //         }
        //     },
        //     o : function (e) {
        //         try {
        //             return e.split("").reverse().join("")
        //         } catch (t) {
        //             return ""
        //         }
        //     }
        // };

        // https://database.gdriveplayer.io/player.php?imdb=tt7286456
        var database_gdriveplayer_me = {
            DOMAIN: 'database.gdriveplayer.io',
            GENRE: 'all',
            LANGUAGE: 'all',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                if (typeof hostInfo.filmImdb == 'undefined' || hostInfo.filmImdb.length == 0) {
                    return;
                };
                var url = '';
                if (hostInfo.show == true) {
                    url = 'https://' + this.DOMAIN + '/player.php?type=series&imdb=' + hostInfo.filmImdb + '&season=' + hostInfo.season + '&episode=' + hostInfo.episodeNumber;
                } else {
                    url = 'https://' + this.DOMAIN + '/player.php?imdb=' + hostInfo.filmImdb
                }

                var html = await libs.postHTML({
                    url: url,
                    headers: this.HEADER,
                    type: 'web',
                    script: 'var postMsg = ' + this.postMsg + '; var botScript = ' + this.getVideoTag + '; botScript();'
                });
                var title = '';
                var quality = libs.getQuality(title);

                eval(html).some(function (obj) {
                    var type = 'mp4';
                    if (obj.file.indexOf('.m3u8') > 0) {
                        type = 'm3u8';
                    };
                    var res = obj.label;
                    if (res == 0) {
                        res = libs.getResolution('');
                    }
                    libs.postMsg(obj.file, type, res, quality, 4, false, false, {
                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                        'Referer': url,
                        'range': 'bytes=0-'
                    });
                });
            },
            postMsg: function (json) {

                if (typeof jsinterface != 'undefined') {
                    jsinterface.getSomeString(JSON.stringify(json));
                } else if (typeof window != 'undefined' && typeof window.webkit != 'undefined' && window.webkit && window.webkit.messageHandlers) {
                    window.webkit.messageHandlers.callbackHandler.postMessage(JSON.stringify(json));
                } else {
                    window.flutter_inappwebview.callHandler("jsinterface", json);
                }
            },
            getVideoTag: function () {
                if (window.location === window.parent.location) {
                    setTimeout(function () {
                        postMsg({});
                    }, 10000);
                    var botstreamplayer = null;
                    var timerS = setInterval(function () {
                        if (typeof jwplayer !== 'undefined' && typeof jwplayer().getPlaylist() !== 'undefined' && jwplayer().getPlaylist().length > 0 && jwplayer().getPlaylist()[0].sources.length > 0) {
                            botstreamplayer = jwplayer();
                            var sources = botstreamplayer.getPlaylist()[0].sources;
                            var results = [];
                            for (var i = 0; i < sources.length; i++) {
                                if (typeof sources[i].file !== 'undefined') {
                                    results.push({
                                        file: sources[i].file,
                                        label: ''
                                    });
                                }
                            }
                            postMsg(results);
                            clearInterval(timerS);
                        };
                    }, 100)
                }
            }
        };

        // https://vidsrc.me/embed/tt11394168/
        // https://vidsrc.me/embed/tt1520211/7-1/
        var vidsrc_me = {
            DOMAIN: 'vidsrc.me',
            GENRE: 'all',
            LANGUAGE: 'all',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                if (typeof hostInfo.filmImdb == 'undefined' || hostInfo.filmImdb.length == 0) {
                    return;
                };

                var that = this;
                var url = 'https://' + this.DOMAIN + '/embed/' + hostInfo.filmImdb + '/';
                if (hostInfo.show == true) {
                    url = url + hostInfo.season + '-' + hostInfo.episodeNumber + '/';
                };
                var html = await libs.postHTML({
                    url: url,
                    headers: this.HEADER,
                    type: 'afn'
                });

                var sources = [];
                var tagSource = $(html).find('div.source');
                if (tagSource.length > 0) {
                    tagSource.each(function (index, item) {
                        var hash = $(item).attr('data-hash');
                        if (hash.length > 0) {
                            sources.push('/source/' + hash);
                        }
                    })
                } else {
                    sources.push($(html).find('iframe').attr('src'));
                }

                for (var i = 0; i < sources.length; i++) {
                    var src = sources[i];
                    if (src.indexOf('/server') >= 0) {
                        var pp = src.split('/');
                        var server = pp[1];
                        if (hostInfo.show == true) {
                            url = 'https://' + that.DOMAIN + '/watching?i=' + hostInfo.filmImdb;
                            that.HEADER.referer = 'https://' + that.DOMAIN + '/' + server + '/' + hostInfo.filmImdb + '/';

                            url = url + '&s=' + hostInfo.season + '&e=' + hostInfo.episodeNumber + '&srv=' + server.replace('server', '');
                            that.HEADER.referer = that.HEADER.referer + '/' + hostInfo.season + '-' + hostInfo.episodeNumber + '/';
                        } else {
                            url = 'https://' + that.DOMAIN + '/watching';
                            that.HEADER.referer = 'https://' + that.DOMAIN + src;
                        }

                    } else {
                        that.HEADER.referer = 'https://v2.' + that.DOMAIN + src;
                        url = 'https://v2.' + that.DOMAIN + src.replace('/source/', '/src/');
                    };
                    var headersjj = await libs.postHTML({
                        url: url,
                        headers: that.HEADER,
                        type: 'afn',
                        selfsign: true,
                        method: 'head'
                    });

                    var json = JSON.parse(headersjj);
                    var key = '';
                    var location = '';
                    if (json.hasOwnProperty('location')) {
                        location = json.location;
                    } else if (json.hasOwnProperty('Location')) {
                        location = json.Location;
                    } else {
                        return;
                    }
                    if (location.indexOf(that.DOMAIN) > 0) {
                        var parts = location.split('/');
                        key = parts[parts.length - 1];
                        $.fn.botStream.hostStreams.push({
                            quality: '',
                            url: 'https://feurl.com/v/' + key
                        });
                    } else {
                        $.fn.botStream.hostStreams.push({
                            quality: '',
                            url: location,
                            referer: url
                        });
                    }

                }

            }
        };

        // https://gomostream.com/movie/tt7888964
        // https://gomo.to/movie/tt1634106
        var gomo_to = {
            DOMAIN: 'gomo.to',
            GENRE: 'all',
            LANGUAGE: 'all',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var that = this;
                if (hostInfo.hasOwnProperty('url') && hostInfo.hasOwnProperty('quality')) {
                    var html = await libs.postHTML({
                        url: hostInfo.url,
                        headers: that.HEADER,
                        type: 'afn'
                    });
                    if (html.indexOf('https://viduplayer.com/') > 0) {
                        $.fn.botStream.hostStreams.push({
                            url: 'https://viduplayer.com/',
                            quality: hostInfo.quality,
                            html: html
                        });
                    } else {
                        var pattern = />eval(.*)/ig;
                        var match = pattern.exec(html);
                        try {
                            if (match && match.length > 1) {
                                eval(match[1]).split('sources:').some(function (item) {
                                    var ss = item.split('],');
                                    if (ss.length == 0) {
                                        return false;
                                    };

                                    try {
                                        var streams = eval(ss[0] + ']');
                                        streams.some(function (stream) {
                                            var type = 'mp4';
                                            if (stream.file.indexOf('.m3u8') > 0) {
                                                type = 'm3u8';
                                            };
                                            var resolution = 720;
                                            if (stream.hasOwnProperty('label')) {
                                                resolution = stream.label.replace('p', '').substring(0, 3);
                                            };
                                            libs.postMsg(stream.file, type, resolution, libs.getQuality(hostInfo.quality), 4, true, false, {
                                                'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                                                'Referer': hostInfo.url,
                                            });
                                        })
                                    } catch (error) {

                                    };
                                });
                            };
                        } catch (error) {

                        };
                    }
                    return;
                };
                if (typeof hostInfo.filmImdb == 'undefined' || hostInfo.filmImdb.length == 0) {
                    return;
                };
                var url = '';
                if (hostInfo.show == true) {
                    url = 'https://' + this.DOMAIN + '/show/' + hostInfo.filmImdb + '/' + libs.digitalNumber(hostInfo.season) + '-' + libs.digitalNumber(hostInfo.episodeNumber);
                } else {
                    url = 'https://' + this.DOMAIN + '/movie/' + hostInfo.filmImdb;
                }
                var jj = await libs.postHTML({
                    url: url,
                    headers: that.HEADER,
                    type: 'web',
                    script: 'var postMsg = ' + this.postMsg + '; var botScript = ' + this.loadWeb + '; botScript();'
                });

                var json = JSON.parse(jj);
                var html = json.html;
                var xtoken = json.xtoken;
                var tokenCode = json.token;

                var token = '';
                match = /"_token": "(.*)"/g.exec(html);
                if (match != null && match.length > 0) {
                    token = match[1];
                };

                if (xtoken.length == 0 || tokenCode.length == 0 || token.length == 0) {
                    return;
                }

                that.HEADER.referer = url;
                that.HEADER.origin = 'https://' + that.DOMAIN;
                that.HEADER['x-token'] = xtoken;
                that.HEADER['X-Requested-With'] = 'XMLHttpRequest';
                var streams = await libs.postHTML({
                    url: 'https://' + that.DOMAIN + '/decoding_v3.php',
                    headers: that.HEADER,
                    type: 'afn',
                    method: 'post',
                    data: {
                        'tokenCode': tokenCode
                    }
                });
                eval(streams).some(function (stream) {
                    $.fn.botStream.hostStreams.push({
                        quality: '',
                        url: stream
                    });
                });
            },
            postMsg: function (json) {

                if (typeof jsinterface != 'undefined') {
                    jsinterface.getSomeString(JSON.stringify(json));
                } else if (typeof window != 'undefined' && typeof window.webkit != 'undefined' && window.webkit && window.webkit.messageHandlers) {
                    window.webkit.messageHandlers.callbackHandler.postMessage(JSON.stringify(json));
                } else {
                    window.flutter_inappwebview.callHandler("jsinterface", json);
                }
            },
            loadWeb: function () {
                setTimeout(function () {
                    postMsg({});
                }, 10000);
                if (window.location === window.parent.location) {
                    var tc = '';
                    var match = /var tc = '(.*)'/g.exec(document.querySelector('body').innerHTML);
                    if (match != null && match.length > 0) {
                        tc = match[1];
                    };

                    if (tc.length > 0) {
                        var xtoken = _tsd_tsd_ds(tc);
                        postMsg({
                            html: document.querySelector('body').innerHTML,
                            xtoken: xtoken,
                            token: tc
                        });
                    } else {
                        postMsg({});
                    }
                };
            }
        };
        $.fn.botStream.hosts.push(gomo_to);

        var gomostream_com = {
            DOMAIN: 'gomostream.com',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('gomostream.com', 'gomo.to')
                });
            }
        };
        $.fn.botStream.hosts.push(gomostream_com);

        // https://gomoplayer.com/embed-zh2iz12sp5db.html
        var gomoplayer_com = {
            DOMAIN: 'gomoplayer.com',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var html = await libs.postHTML({
                    url: hostInfo.url,
                    headers: that.HEADER,
                    type: 'afn'
                });
                var pattern = />eval(.*)/ig;
                var match = pattern.exec(html);
                try {
                    if (match && match.length > 1) {
                        eval(match[1]).split('sources:').some(function (item) {
                            var ss = item.split('],');
                            if (ss.length == 0) {
                                return false;
                            };

                            try {
                                var streams = eval(ss[0] + ']');
                                streams.some(function (stream) {
                                    var type = 'mp4';
                                    if (stream.file.indexOf('.m3u8') > 0) {
                                        type = 'm3u8';
                                    };
                                    var resolution = 720;
                                    if (stream.hasOwnProperty('label')) {
                                        resolution = stream.label.replace('p', '').substring(0, 3);
                                    };
                                    libs.postMsg(stream.file, type, resolution, libs.getQuality(hostInfo.quality), 4, true, false, {
                                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                                        'Referer': hostInfo.url,
                                    });
                                })
                            } catch (error) {

                            };
                        });
                    };
                } catch (error) {

                };
            }
        };
        $.fn.botStream.hosts.push(gomoplayer_com);

        //https://123moviesfull.su/tv/power-season-6/watching/?episode_id=19646
        var moviesfull123_su = {
            DOMAIN: '123moviesfull.su',
            GENRE: 'all',
            LANGUAGE: 'all',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                'referer': 'https://123moviesfull.su/123movies/'
            },
            getHosts: async function (movieInfo, libs) {
                var that = this;
                var keyword = movieInfo.title;
                if (movieInfo.show == true) {
                    keyword += ' Season ' + movieInfo.season;
                } else {
                    keyword += ' ' + movieInfo.year;
                };
                var slug = libs.slug(keyword);

                var tagAPattern = '';
                if (movieInfo.show == true) {
                    tagAPattern = 'tv';
                } else {
                    tagAPattern = 'movie';
                };

                var detailURL = 'https://' + that.DOMAIN + '/' + tagAPattern + '/' + slug + '/';
                var detail = await libs.postHTML({
                    url: detailURL,
                    headers: that.HEADER,
                    type: 'afn'
                });

                var button = $(detail).find('.btn-watch-area > .bwa-content > a.bwac-btn');
                if (button.length == 0) {
                    return;
                }

                that.HEADER.referer = detailURL;
                var episodeURL = button.attr('href');

                var episodes = await libs.postHTML({
                    url: episodeURL,
                    headers: that.HEADER,
                    type: 'afn'
                });

                if (movieInfo.show == true) {
                    var elms = null;
                    $(['$', ':', '-']).each(function (i, it) {
                        var regex = new RegExp('Episode ' + libs.digitalNumber(movieInfo.episodeNumber) + it, 'ig');
                        $(episodes).find('li.episode-item').each(function (x, y) {
                            if ($(y).text().trim().match(regex)) {
                                elms = $(y);
                                return false;
                            }
                        });
                        if (elms == null) {
                            regex = new RegExp('Episode ' + movieInfo.episodeNumber + it, 'ig');
                            $(episodes).find('li.episode-item').each(function (x, y) {
                                if ($(y).text().trim().match(regex)) {
                                    elms = $(y);
                                    return false;
                                }
                            });
                        };

                        if (elms != null) {
                            elms.each(async function (index, elm) {
                                var key = $(elm).attr('data-player');
                                $.fn.botStream.hostStreams.push({
                                    quality: 'HD',
                                    url: 'https://viduplayer.com/embed-' + key + '.html'
                                });
                            });
                            return false;
                        };
                    });
                } else {
                    var match = /security: "(.*)"},/ig.exec(episodes);
                    if (match == null || match.length == 0) {
                        return;
                    };
                    var security = match[1];
                    var movieId = $(episodes).find('#rating').attr('movie-id');
                    var jj = await libs.postHTML({
                        url: 'https://' + that.DOMAIN + '/wp-admin/admin-ajax.php',
                        headers: that.HEADER,
                        type: 'afn',
                        method: 'post',
                        data: {
                            action: 'movie_load_embed',
                            episode_id: movieId,
                            security: security
                        }
                    });

                    var json = JSON.parse(jj);
                    if (json.status == 1 && json.hasOwnProperty('viduplayer')) {
                        $.fn.botStream.hostStreams.push({
                            quality: '',
                            url: json.viduplayer
                        });

                    }
                };
            }
        };

        //https://ww3.9movies.yt/movie/search?keyword=joker
        // var ninemovies_yt = {
        //     DOMAIN: '9movies.ro',
        //     GENRE: 'all',
        //     LANGUAGE: 'all',
        //     HEADER: {
        //         'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
        //         'referer': 'https://9movies.ro'
        //     },
        //     getHosts : async function (movieInfo, libs) {
        //         var that = this;
        //         var keyword = '';
        //         var input = movieInfo.title.trim().toLowerCase();
        //         keyword = libs.slug(input).replace(/\-/g, '+');
        //         var searchURL = 'https://'+that.DOMAIN+'/movie/search?keyword=' + keyword;
        //         var html = await libs.postHTML({
        //             url: searchURL,
        //             headers: that.HEADER,
        //             type: 'afn'
        //         });

        //         var quality = 'HD';
        //         var detailURL = '';
        //         $(html).find('.movie-list .item > a.name[href^="/film/"]').each(async function(index, item){
        //             var title = $(item).text().trim().toLowerCase();
        //             var url = 'https://' + that.DOMAIN + $(item).attr('href');

        //             if(movieInfo.show == true && $(item).parent('.item').find('.status').length > 0){
        //                 if(input == title){
        //                     detailURL = url;
        //                     return true;
        //                 };
        //             } else if($(item).parent('.item').find('.quality').length > 0){
        //                 quality = $(item).parent('.item').find('.quality').text().trim();
        //                 if(input == title){
        //                     detailURL = url;
        //                     return true;
        //                 };
        //             }
        //         });

        //         if(detailURL.length == 0){
        //             return;
        //         };

        //         detailURL = detailURL.replace('/film/', '/watch/');
        //         var match = /\/watch\/[a-z0-9-]+-(\d+)/ig.exec(detailURL);
        //         if(match == null || match.length == 0){
        //             return;
        //         }

        //         var episodeURL = 'https://'+this.DOMAIN+'/ajax/movie_episodes/' + match[1];
        //         this.HEADER.referer = detailURL;
        //         var ejj = await libs.postHTML({
        //             url: episodeURL,
        //             headers: that.HEADER,
        //             type: 'afn'
        //         });

        //         var episode = JSON.parse(ejj);
        //         if(episode.status != 1) {
        //             return;
        //         };

        //         var episodeId = 0;
        //         if(movieInfo.show == true){
        //             var seasons = $(episode.html).find('.name > strong:contains("Season '+movieInfo.season+'")');
        //             if(seasons.length == 0){
        //                 return;
        //             }

        //             episode = $(seasons[0]).parents('.server').find('.episodes > li > a:contains('+libs.digitalNumber(movieInfo.episodeNumber)+')');
        //             episodeId = episode.attr('data-id');
        //         } else {
        //             var servers = $(episode.html).find('.name > strong:contains("Server")');
        //             if(servers.length == 0){
        //                 return;
        //             }

        //             episode = $(servers[0]).parents('.server').find('.episodes > li > a:contains(01)');
        //             episodeId = episode.attr('data-id');
        //         }

        //         if(episodeId == 0){
        //             return;
        //         }

        //         var sourceURL = 'https://'+that.DOMAIN+'/ajax/movie_sources/';
        //         this.HEADER.referer = detailURL;
        //         this.HEADER.origin = 'https://' + this.DOMAIN;
        //         jj = await libs.postHTML({
        //             url: sourceURL,
        //             headers: that.HEADER,
        //             type: 'afn',
        //             method: 'post',
        //             data: {
        //                 eid: parseInt(episodeId)
        //             }
        //         });
        //         var json = JSON.parse(jj);
        //         if(json.hasOwnProperty('playlist')){
        //             json.playlist.some(function(item){
        //                 item.sources.some(function(obj){
        //                     if(obj.file.indexOf('error.mp4') > 0 || obj.file.indexOf('404.mp4')>0){
        //                         return false;
        //                     }
        //                     var cast = true;
        //                     var type = 'mp4';
        //                     if(obj.file.indexOf('.m3u8') > 0){
        //                         type = 'm3u8';
        //                     };
        //                     var resolution = libs.getResolution(obj.file);
        //                     if(obj.hasOwnProperty('label')){
        //                         resolution = obj.label.replace('p','');
        //                     };
        //                     libs.postMsg(obj.file, type, resolution, libs.getQuality(quality), 4, cast, false, {
        //                         'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
        //                         'Referer': detailURL
        //                     });
        //                 })
        //             });
        //         } else if(json.hasOwnProperty('embed')){
        //             $.fn.botStream.hostStreams.push({
        //                 quality: quality,
        //                 url: json.embed
        //             });
        //         }
        //     }
        // };

        //Hindi
        //https://2gomovies.to/movie/hddi-malang-2020-free-hindi-movie-online-gomovies/watching/
        //https://ww0.0gomovies.org/movie/hiidi-malang-2020-hindi-full-movie-watch-online-free/watching/
        var ww0_0gomovies_org = {
            DOMAIN: '0gomovies.io',
            GENRE: 'all',
            COUNTRY: 'india',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getHosts: async function (movieInfo, libs) {
                if (movieInfo.show == true) {
                    return;
                };

                var url = 'https://' + this.DOMAIN + '/search-query/' + encodeURIComponent(movieInfo.title.toLowerCase()).replace(/%20/g, '+') + '/';
                this.HEADER.referer = 'https://' + this.DOMAIN;
                var html = await libs.postHTML({
                    url: url,
                    headers: this.HEADER,
                    type: 'afn'
                });

                var that = this;
                $(html).find('.ml-item > a').each(async function (index, item) {
                    if ($(item).find('h3').text().toLowerCase().trim().indexOf(movieInfo.title.toLowerCase().trim()) >= 0) {
                        var language = '';
                        var match = /\((.*)\)/gi.exec($(item).find('h3').text());
                        if (match != null && match.length > 0) {
                            language = match[1];
                        }
                        var url = $(item).attr('href');
                        if (url.length > 0) {
                            url = url + 'watching/';
                            var detail = await libs.postHTML({
                                url: url,
                                headers: that.HEADER,
                                type: 'afn'
                            });

                            $(detail).find('.pas-list > ul#servers-list > li.episode-item').each(function (index, item) {
                                var host = $(item).attr('data-drive');
                                if (host == null) {
                                    host = $(item).attr('data-streamgo');
                                };
                                if (host == null) {
                                    host = $(item).attr('data-putload');
                                };
                                if (host != null && host.length > 0) {
                                    if (host.indexOf('http') < 0) {
                                        host = 'https:' + host;
                                    };
                                    if (host.indexOf('0gomovies.website/v/') > 0) {
                                        host = host.replace('0gomovies.website/v/', 'feurl.com/v/');
                                    };
                                    $.fn.botStream.hostStreams.push({
                                        quality: $(detail).find('span.quality').text().replace('720', '').replace('1080', '').trim(),
                                        url: host,
                                        language: language
                                    });
                                }
                            })
                        }
                    }
                })
            }
        };

        //Hindi
        //https://gomovies.band/
        var gomovies_band = {
            DOMAIN: 'gomovies.band',
            GENRE: 'all',
            COUNTRY: 'india',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getHosts: async function (movieInfo, libs) {
                if (movieInfo.show == true) {
                    return;
                };

                var url = 'https://' + this.DOMAIN + '/?s=' + encodeURIComponent(movieInfo.title.toLowerCase()).replace(/%20/g, '+');
                this.HEADER.referer = 'https://' + this.DOMAIN;
                var html = await libs.postHTML({
                    url: url,
                    headers: this.HEADER,
                    type: 'afn'
                });

                var that = this;
                $(html).find('.celeb_card > a.celeb').each(async function (index, item) {
                    if ($(item).find('span.title').text().toLowerCase().trim().indexOf(movieInfo.title.toLowerCase().trim()) >= 0) {
                        var language = '';
                        var match = /\((.*)\)/gi.exec($(item).find('span.title').text());
                        if (match != null && match.length > 0) {
                            language = match[1];
                        }
                        var url = $(item).attr('href');
                        if (url.length > 0) {
                            var detail = await libs.postHTML({
                                url: url,
                                headers: that.HEADER,
                                type: 'afn'
                            });

                            $(detail).find('.tab_item > iframe').each(function (index, item) {
                                var host = $(item).attr('data-src');
                                if (host != null && host.length > 0) {
                                    if (host.indexOf('http') < 0) {
                                        host = 'https:' + host;
                                    };
                                    $.fn.botStream.hostStreams.push({
                                        quality: '',
                                        url: host,
                                        language: language
                                    });
                                }
                            })
                        }
                    }
                })
            }
        };

        //Arabic
        //https://aflamstream.com/?s=يوم+وليلة
        var aflamstream_com = {
            DOMAIN: 'aflamstream.com',
            GENRE: 'all',
            LANGUAGE: 'arabic',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getHosts: async function (movieInfo, libs) {
                var that = this;
                var titles = [movieInfo.title];
                if (movieInfo.originalTitle.length > 0) {
                    titles.push(movieInfo.originalTitle);
                }
                for (var i = 0; i < titles.length; i++) {
                    var title = titles[i];
                    var url = 'https://' + that.DOMAIN + '/?s=' + encodeURIComponent(title).replace(/ /g, '+');
                    that.HEADER.referer = 'https://' + that.DOMAIN;
                    var html = await libs.postHTML({
                        url: url,
                        headers: that.HEADER,
                        type: 'afn'
                    });
                    $(html).find('.result-item > article > .details > .title > a').each(async function (index, item) {
                        var year = parseInt($(item).parents('.details').find('.meta > .year').text());
                        if (isNaN(year)) {
                            year = 0;
                        }
                        if ($(item).text().toLowerCase().trim().indexOf(title.toLowerCase().trim()) >= 0 && (year == movieInfo.yearFirstSeason || year == movieInfo.year)) {
                            var url = $(item).attr('href');
                            if (url.length > 0) {
                                var detail = await libs.postHTML({
                                    url: url,
                                    headers: that.HEADER,
                                    type: 'afn'
                                });

                                var dataType = 'movie';
                                if (movieInfo.show == true) {
                                    dataType = 'tv';
                                    var episodeItem = $(detail).find('#seasons ul.episodios .numerando:contains(' + movieInfo.season + ' - ' + movieInfo.episodeNumber + ')').parent().find('.episodiotitle > a');
                                    if (episodeItem.length == 0) {
                                        return false;
                                    };
                                    that.HEADER.referer = url;
                                    var episodeURL = episodeItem.attr('href');
                                    detail = await libs.postHTML({
                                        url: episodeURL,
                                        headers: that.HEADER,
                                        type: 'afn'
                                    });
                                }

                                $(detail).find('ul#playeroptionsul > li[data-type="' + dataType + '"]').each(async function (index, item2) {
                                    if ($(item2).attr('id') == 'player-option-trailer') {
                                        return true;
                                    };
                                    var dataNum = $(item2).attr('data-nume');
                                    var dataPost = $(item2).attr('data-post');

                                    var streamjj = await libs.postHTML({
                                        url: 'https://' + that.DOMAIN + '/wp-admin/admin-ajax.php?r=' + index,
                                        headers: that.HEADER,
                                        type: 'afn',
                                        method: 'post',
                                        data: {
                                            action: 'doo_player_ajax',
                                            post: dataPost,
                                            nume: dataNum,
                                            type: 'movie'
                                        }
                                    });
                                    var stream = JSON.parse(streamjj);
                                    $.fn.botStream.hostStreams.push({
                                        quality: '',
                                        url: stream.embed_url
                                    });
                                })
                            }
                        }
                    })
                };
            }
        };

        // india
        // https://www.mxplayer.in/show/watch-mastram-series-online-a825dc18cb9923faee7dcd8c53e6c5d5?q=jersey&search=true
        var mxplayer_in = {
            DOMAIN: 'www.mxplayer.in',
            GENRE: 'all',
            COUNTRY: 'india',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getHosts: async function (movieInfo, libs) {
                var that = this;
                var keyword = encodeURIComponent(movieInfo.title);
                var userId = libs.randomString(8) + '-' + libs.randomString(4) + '-' + libs.randomString(4) + '-' + libs.randomString(4) + '-' + libs.randomString(12);
                var searchURL = 'https://api.mxplay.com/v1/web/search/resultv2?query=' + keyword + '&userid=' + userId + '&platform=com.mxplay.desktop&content-languages=hi,en';

                this.HEADER.referer = 'https://' + this.DOMAIN;
                this.HEADER['content-type'] = 'application/json;charset=UTF-8';
                var searchjj = await libs.postHTML({
                    url: searchURL,
                    headers: that.HEADER,
                    type: 'afn',
                    method: 'post',
                    body: '{}'
                });

                var searchjson = JSON.parse(searchjj);

                var quality = '';
                var streamURL = '';
                var detailURL = '';
                var languages = [];
                var title = libs.slug(movieInfo.title);
                for (var i = 0; i < searchjson.sections.length; i++) {
                    var section = searchjson.sections[i];
                    if (section.id == 'all') {
                        for (var j = 0; j < section.items.length; j++) {
                            var item = section.items[j];
                            var kw = libs.slug(item.title);
                            if (item.type == 'movie' && movieInfo.show == false && kw.indexOf(title) == 0 && item.releaseDate.indexOf(movieInfo.year) == 0) {
                            } else if (item.type == 'tvshow' && movieInfo.show == true && kw.indexOf(title) == 0 && item.releaseDate.indexOf(movieInfo.yearFirstSeason) == 0) {
                            } else {
                                continue;
                            };

                            quality = 'HD';
                            if (kw.indexOf('-dubbed') > 0) {
                                quality = 'HD-DUB';
                            }
                            detailURL = item.webUrl;
                            languages = item.languages;
                            if (movieInfo.show == false) {
                                streamURL = item.stream.hls.base;
                            }
                            break;
                        };
                    }

                    if (detailURL.length > 0) {
                        break;
                    };
                };

                if (movieInfo.show == true && detailURL.length > 0) {
                    quality = 'HD';
                    var detail = await libs.postHTML({
                        url: 'https://' + that.DOMAIN + detailURL,
                        headers: that.HEADER,
                        type: 'afn'
                    });
                    var match = /"containers":(.*?)}]/ig.exec(detail);
                    if (match == false) {
                        return;
                    };

                    var id = '';
                    var json = JSON.parse(match[1] + '}]');
                    for (var i = 0; i < json.length; i++) {
                        var item = json[i];
                        if (item.type == 'season' && item.sequence == movieInfo.season) {
                            id = item.id;
                            break;
                        }
                    };
                    if (id.length == 0) {
                        return;
                    };

                    that.HEADER.referer = detailURL;
                    var items = [];
                    var page = 0;
                    var apiURL = 'https://api.mxplay.com/v1/web/detail/tab/tvshowepisodes?type=season&id=' + id + '&userid=' + userId + '&platform=com.mxplay.desktop&content-languages=hi,en';
                    do {
                        var apijj = await libs.postHTML({
                            url: apiURL,
                            headers: that.HEADER,
                            type: 'afn'
                        });
                        json = JSON.parse(apijj);
                        if (items.length == 0) {
                            items = json.items;
                        } else {
                            items.push(...json.items);
                        };
                        page += 10;
                        apiURL = 'https://api.mxplay.com/v1/web/detail/tab/tvshowepisodes?type=season&' + json.next + '&id=' + id + '&userid=' + userId + '&platform=com.mxplay.desktop&content-languages=hi,en';
                    } while (page < movieInfo.episodeNumber);

                    if (items.length >= movieInfo.episodeNumber) {
                        var item = items[movieInfo.episodeNumber - 1];
                        languages = item.languages;
                        streamURL = item.stream.thirdParty.hlsUrl;
                    } else {
                        return;
                    };
                };

                if (streamURL.length == 0) {
                    return;
                };

                var file = '';
                if (streamURL.indexOf('http') == 0) {
                    file = streamURL;
                } else {
                    file = 'https://media-content.akamaized.net/' + streamURL;
                }
                var type = 'mp4';
                if (file.indexOf('m3u8') > 0) {
                    type = 'm3u8';
                };
                libs.postMsg(file, type, libs.getResolution(file), libs.getQuality(quality), 4, true, false, {
                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                    'Referer': 'https://' + that.DOMAIN
                });
            }
        };

        // multi languages
        //https://www.thetamilyogi.com/?s=your+honor&submit=Search
        var thetamilyogi_com = {
            DOMAIN: 'www.thetamilyogi.co',
            GENRE: 'all',
            LANGUAGE: 'all',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getHosts: async function (movieInfo, libs) {
                var detailURL = '';
                var quality = 'HD';
                if (movieInfo.sourceURL.length > 0) {
                    detailURL = movieInfo.sourceURL;
                } else {
                    if ($.inArray('english', movieInfo.languages) >= 0) {
                        return;
                    };
                    var that = this;
                    var keyword = movieInfo.title;
                    if (movieInfo.show == true) {
                        keyword = keyword + ' season ' + movieInfo.season + ' episode ' + movieInfo.episodeNumber;
                    };
                    var url = 'https://' + that.DOMAIN + '/?s=' + encodeURIComponent(keyword).replace(/%20/g, '+') + '&submit=Search';
                    that.HEADER.referer = 'https://' + that.DOMAIN;
                    var html = await libs.postHTML({
                        url: url,
                        headers: that.HEADER,
                        type: 'afn'
                    });

                    if (movieInfo.show == false) {
                        keyword = keyword + ' ' + movieInfo.year;
                    };
                    keyword = libs.slug(keyword);

                    $(html).find('.l-movie-post > .movie-post > figure.movie-post > a').each(async function (index, item) {
                        var title = libs.slug($(item).attr('title').replace('()', '').trim());

                        if (title == keyword) {
                            detailURL = $(item).attr('href');
                            quality = $(item).find('span.quality').text();
                            return false;
                        }
                    })
                }

                if (detailURL.length == 0) {
                    return;
                };

                var detail = await libs.postHTML({
                    url: detailURL,
                    headers: that.HEADER,
                    type: 'afn'
                });

                var language = $(detail).find('ul.movie-info > li > a[href*=language]').text().replace(',', '').toLowerCase().trim();
                if ($.inArray(language, movieInfo.languages) < 0) {
                    return;
                }
                var file = $(detail).find('h2.hover-links > a').attr('href');

                var cast = true;
                var type = 'mp4';
                if (file.indexOf('.m3u8') > 0) {
                    type = 'm3u8';
                };
                var resolution = libs.getResolution(file);
                libs.postMsg(file, type, resolution, libs.getQuality(quality), 4, cast, false, {
                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                    'Referer': detailURL
                });

            }
        };

        // multi languages
        //https://www.frenchstreaming.fr/?s=Ee+nagaraniki&submit=Chercher
        //https://www.filmesonlinegratishdx.com/?s=Ee+nagaraniki&submit=Procurar
        //https://www.khatri-maza.net/?s=first+kill&submit=Search
        var khatri_maza_net = {
            DOMAIN: 'www.frenchstreaming.club',
            GENRE: 'all',
            LANGUAGE: 'all',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getHosts: async function (movieInfo, libs) {
                if (movieInfo.show == true) {
                    return;
                }
                var detailURL = '';
                var quality = 'HD';
                if (movieInfo.sourceURL.length > 0) {
                    detailURL = movieInfo.sourceURL;
                } else {
                    if ($.inArray('english', movieInfo.languages) >= 0) {
                        return;
                    };
                    var that = this;
                    var keyword = movieInfo.title;
                    var url = 'https://' + that.DOMAIN + '/?s=' + encodeURIComponent(keyword).replace(/%20/g, '+') + '&submit=Search';
                    that.HEADER.referer = 'https://' + that.DOMAIN;
                    var html = await libs.postHTML({
                        url: url,
                        headers: that.HEADER,
                        type: 'afn'
                    });

                    keyword = libs.slug(keyword);

                    $(html).find('.l-movie-post > .movie-post > figure > a').each(async function (index, item) {
                        var title = libs.slug($(item).attr('title').replace('()', '').trim());

                        if (title == keyword) {
                            detailURL = $(item).attr('href');
                            return false;
                        }
                    })
                }

                if (detailURL.length == 0) {
                    return;
                };

                var detail = await libs.postHTML({
                    url: detailURL,
                    headers: that.HEADER,
                    type: 'afn'
                });

                var language = $(detail).find('ul.movie-info > li > a[href*=language]').text().replace(',', '').toLowerCase().trim();
                if ($.inArray(language, movieInfo.languages) < 0) {
                    return;
                }
                var file = $(detail).find('form > input[name=linkurl]').val();

                var cast = true;
                var type = 'mp4';
                if (file.indexOf('.m3u8') > 0) {
                    type = 'm3u8';
                };
                var resolution = libs.getResolution(file);
                libs.postMsg(file, type, resolution, libs.getQuality(quality), 4, cast, false, {
                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                    'Referer': detailURL
                });

            }
        };

        //https://www.hindilinks4u.to/wet-woman-in-the-wind-2016-in-hindi/
        var hindilinks4u_to = {
            DOMAIN: 'www.hindilinks4u.to',
            GENRE: 'all',
            COUNTRY: 'india',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getHosts: async function (movieInfo, libs) {
                if (movieInfo.show == true) {
                    return;
                }
                var that = this;
                var detailURL = '';
                var quality = 'HD';
                if (movieInfo.sourceURL.length > 0) {
                    detailURL = movieInfo.sourceURL;
                } else {
                    var keyword = movieInfo.title;
                    var url = 'https://' + that.DOMAIN + '/?s=' + encodeURIComponent(keyword).replace(/%20/g, '+');
                    that.HEADER.referer = 'https://' + that.DOMAIN;
                    var html = await libs.postHTML({
                        url: url,
                        headers: that.HEADER,
                        type: 'web',
                        script: 'var postMsg=' + that.postMsg + ';var botScript = ' + that.check_ddos + ';botScript();'
                    });

                    if (movieInfo.show == true) {
                        keyword = libs.slug(keyword);
                    } else {
                        keyword = libs.slug(keyword + ' ' + movieInfo.year);
                    }

                    $(html).find('.loop-content > .cf > .item-video > .data > h2 > a').each(async function (index, item) {
                        var title = libs.slug($(item).attr('title').replace('()', '').trim());

                        if (title == keyword) {
                            detailURL = $(item).attr('href');
                            return false;
                        }
                    })
                }

                if (detailURL.length == 0) {
                    return;
                };

                var detail = await libs.postHTML({
                    url: detailURL,
                    headers: that.HEADER,
                    type: 'afn'
                });

                $(detail).find('span.btn').each(function (index, item) {
                    var sourceURL = $(item).attr('data-href');
                    $.fn.botStream.hostStreams.push({
                        quality: quality,
                        url: sourceURL
                    });
                })

            },
            postMsg: function (html) {

                if (typeof jsinterface != 'undefined') {
                    jsinterface.getSomeString(html);
                } else if (typeof window != 'undefined' && typeof window.webkit != 'undefined' && window.webkit && window.webkit.messageHandlers) {
                    window.webkit.messageHandlers.callbackHandler.postMessage(html);
                } else {
                    window.flutter_inappwebview.callHandler("jsinterface", html);
                }
            },
            check_ddos: function () {
                if (window.location === window.parent.location) {
                    setTimeout(function () {
                        postMsg('');
                    }, 10000);
                    var timer = setInterval(function () {
                        if (document.querySelector('title').innerText !== 'Just a moment...' && (document.querySelector('video') != null || (document.querySelector('#iframe-embed') != null && document.querySelector('#iframe-embed').getAttribute('src') != null && document.querySelector('#iframe-embed').getAttribute('src').length > 0))) {
                            clearInterval(timer);
                            var html = document.querySelector('body').innerHTML;
                            postMsg(html);
                        }
                    }, 100);
                };
            }
        };

        //https://hd15s.com/movies/chintu-ka-birthday/
        var hd15s_com = {
            DOMAIN: 'hdmovie2.com',
            GENRE: 'all',
            COUNTRY: 'india',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getHosts: async function (movieInfo, libs) {
                if ($.inArray('english', movieInfo.languages) >= 0) {
                    return;
                };
                var that = this;
                var title = movieInfo.title;
                var url = 'https://' + that.DOMAIN + '/?s=' + encodeURIComponent(title).replace(/%20/g, '+');
                that.HEADER.referer = 'https://' + that.DOMAIN;
                var html = await libs.postHTML({
                    url: url,
                    headers: that.HEADER,
                    type: 'afn'
                });

                $(html).find('.result-item > article > .details > .title > a').each(async function (index, item) {
                    var year = parseInt($(item).parents('.details').find('.meta > .year').text());
                    if (isNaN(year)) {
                        year = 0;
                    }

                    var txt = $(item).text().toLowerCase().trim();
                    if (txt.indexOf(title.toLowerCase().trim()) >= 0 &&
                        (year == movieInfo.yearFirstSeason || year == movieInfo.year) &&
                        (movieInfo.show == false || (movieInfo.show == true && (txt.indexOf('season ' + movieInfo.season) > 0 || txt.indexOf('s' + libs.digitalNumber(movieInfo.season)) > 0)))
                    ) {
                        var url = $(item).attr('href');
                        if (url.length > 0) {
                            var detail = await libs.postHTML({
                                url: url,
                                headers: that.HEADER,
                                type: 'afn'
                            });

                            var dataType = 'movie';
                            var quality = 'HD';
                            var byepisode = false;
                            if (movieInfo.show == true && $(detail).find('ul#playeroptionsul > li[data-type="movie"]:eq(0) span.title').text().indexOf('EP') == 0) {
                                byepisode = true;
                            }

                            $(detail).find('ul#playeroptionsul > li[data-type="' + dataType + '"]').each(async function (index, item2) {
                                if ($(item2).attr('id') == 'player-option-trailer') {
                                    return true;
                                };
                                var text = $(item2).find('span.title').text();
                                if (byepisode == true) {
                                    var match = /EP(\d+| \d+)( |-)/ig.exec(text);
                                    if (match == null || parseInt(match[1]) != movieInfo.episodeNumber) {
                                        return true;
                                    }
                                };

                                var dataNum = $(item2).attr('data-nume');
                                var dataPost = $(item2).attr('data-post');

                                var stream = await libs.postHTML({
                                    url: 'https://' + that.DOMAIN + '/wp-admin/admin-ajax.php?r=' + index,
                                    headers: that.HEADER,
                                    type: 'afn',
                                    method: 'post',
                                    data: {
                                        action: 'doo_player_ajax',
                                        post: dataPost,
                                        nume: dataNum,
                                        type: 'movie'
                                    }
                                });
                                var jsonStream = JSON.parse(stream);
                                if (jsonStream.type == 'iframe') {
                                    $.fn.botStream.hostStreams.push({
                                        quality: quality,
                                        url: jsonStream.embed_url
                                    });
                                } else if ($(jsonStream.embed_url).prop("tagName") == 'IFRAME') {
                                    $.fn.botStream.hostStreams.push({
                                        quality: quality,
                                        url: $(jsonStream.embed_url).attr('src')
                                    });
                                }

                                // var streamURL = '';
                                // $(stream).each(await function(index, item){
                                //     if($(item).prop("tagName") == 'IFRAME'){
                                //         streamURL = $(item).attr('src');
                                //         return false;
                                //     }
                                // });
                                // $.fn.botStream.hostStreams.push({
                                //     quality: quality,
                                //     url: streamURL
                                // });
                            })
                        }
                    }
                })
            }
        };

        //https://123movies.autos/movie/search/If-Loving-You-is-Wrong---Season-6
        var moviesub123_io = {
            DOMAIN: '123movies.autos',
            GENRE: 'all',
            LANGUAGE: 'all',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getHosts: async function (movieInfo, libs) {
                var that = this;

                if (movieInfo.episodeNumber > 30) {
                    return;
                }
                var detailURL = '';
                if (movieInfo.sourceURL.length > 0) {
                    detailURL = movieInfo.sourceURL;
                } else {
                    var keyword = movieInfo.title;
                    if (movieInfo.show == true) {
                        keyword += ' Season ' + movieInfo.season;
                    };
                    var sluged = libs.slug(keyword);

                    detailURL = '/film/' + sluged;
                    detailURL = 'https://' + that.DOMAIN + detailURL;
                };

                if (detailURL.length == 0) {
                    return;
                }

                detailURL = detailURL + '/watching.html';
                if (movieInfo.show == true) {
                    detailURL += '?ep=' + movieInfo.episodeNumber;
                };
                var detail = await libs.postHTML({
                    url: detailURL,
                    headers: that.HEADER,
                    type: 'afn'
                });

                var quality = '';
                if (movieInfo.show == true) {
                    quality = $(detail).find('span.quality').text().replace('720', '').replace('1080', '').trim();
                };

                $(detail).find('.les-content > a[episode-data=' + movieInfo.episodeNumber + ']').each(function (index, item) {
                    var host = $(item).attr('player-data');
                    if (host.length > 0) {
                        if (movieInfo.show == false) {
                            quality = $(item).text().replace('720', '').replace('1080', '').trim();
                        };
                        if (host.indexOf('http') < 0) {
                            host = 'https:' + host;
                        };
                        $.fn.botStream.hostStreams.push({
                            quality: quality,
                            url: host
                        });
                    }
                })
            }
        };

        //https://free-123movies.com/tv-series/four-more-shots-please-season-2-sub-eng/4iG3Nwxe
        var free_123movies_com = {
            DOMAIN: 'ww1.free-123movies.com',
            GENRE: 'all',
            LANGUAGE: 'all',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getHosts: async function (movieInfo, libs) {
                var that = this;

                var detailURL = '';
                if (movieInfo.sourceURL.length > 0) {
                    detailURL = movieInfo.sourceURL;
                } else {
                    var keyword = movieInfo.title;
                    if (movieInfo.show == true) {
                        keyword += ' Season ' + movieInfo.season;
                    };

                    that.HEADER.referer = 'https://' + that.DOMAIN;
                    that.HEADER['x-requested-with'] = 'XMLHttpRequest';
                    var search = 'https://' + that.DOMAIN + '/search-ajax/' + encodeURIComponent(keyword);
                    var html = await libs.postHTML({
                        url: search,
                        headers: that.HEADER,
                        type: 'afn'
                    });

                    var sluged = libs.slug(keyword);
                    var urlPattern = '';
                    if (movieInfo.show == true) {
                        urlPattern = '/tv-series/' + sluged;
                    } else {
                        urlPattern = '/movie/' + sluged;
                    };

                    $(html).find('a.VPkgdeKmTQ').each(async function (index, item) {
                        var href = $(item).attr('href');
                        if (href.indexOf(urlPattern) == 0) {
                            detailURL = 'https://' + that.DOMAIN + href;
                            return true;
                        }
                    });
                };

                if (detailURL.length == 0) {
                    return;
                }

                delete that.HEADER['x-requested-with'];
                var detail = await libs.postHTML({
                    url: detailURL,
                    headers: that.HEADER,
                    type: 'afn'
                });

                var quality = '';
                if (movieInfo.show == true) {
                    quality = 'HD';
                };

                var year = parseInt($(detail).find('a[href^="/year/"').text());
                if (isNaN(year) || year != movieInfo.year) {
                    return;
                }

                var episodeId = '';
                var elm = null;
                if (movieInfo.show == true) {
                    $(['$', ':', '-']).each(function (i, it) {
                        var regex = new RegExp('Episode ' + libs.digitalNumber(movieInfo.episodeNumber) + it, 'ig');
                        $(detail).find('a > span.IHoCYYJmqx').each(function (x, y) {
                            if ($(y).text().trim().match(regex)) {
                                elm = $(y);
                                return false;
                            }
                        });
                        if (elm == null) {
                            regex = new RegExp('Episode ' + movieInfo.episodeNumber + it, 'ig');
                            $(detail).find('a > span.IHoCYYJmqx').each(function (x, y) {
                                if ($(y).text().trim().match(regex)) {
                                    elm = $(y);
                                    return false;
                                }
                            });
                        };

                        if (elm != null) {
                            episodeId = $(elm).parent('a').attr('data-ep-id');
                            return false;
                        };
                    });
                } else {
                    episodeId = '0';
                };

                if (episodeId.length == 0) {
                    return;
                };

                var tmpURL = '';
                var parts = [];
                var filmKey = '';
                if (movieInfo.show == true) {
                    var match = /window.history.pushState\((.*)'(.*?)'\)/ig.exec(detail);
                    if (match != null && match.length > 2) {
                        tmpURL = match[2];
                    };
                    parts = tmpURL.split('/');
                    if (parts.length != 5) {
                        return;
                    };
                    filmKey = parts[3];
                } else {
                    parts = detailURL.split('/');
                    filmKey = parts[5].replace('-watch.html', '');
                };

                that.HEADER['x-requested-with'] = 'XMLHttpRequest';
                var serverHTML = await libs.postHTML({
                    url: 'https://' + that.DOMAIN + '/user/servers/' + filmKey + '?ep=' + episodeId,
                    headers: that.HEADER,
                    type: 'afn'
                });

                var server = $(serverHTML).find('div[data-info="serv"] > ul > li').attr('data-value');
                var episodeURL = '';
                if (movieInfo.show == true) {
                    episodeURL = 'https://' + that.DOMAIN + '/' + parts[1] + '/' + parts[2] + '/' + parts[3] + '/' + episodeId + '/' + parts[4] + '-watch.html';
                } else {
                    episodeURL = detailURL;
                }
                that.HEADER.referer = episodeURL;
                episodeURL = episodeURL + '?server=' + server + '&_=' + new Date().getTime();
                var streamjj = await libs.postHTML({
                    url: episodeURL,
                    headers: that.HEADER,
                    type: 'afn'
                });

                var streamjson = JSON.parse(streamjj);
                streamjson.some(function (obj) {
                    var type = 'mp4';
                    var cast = true;
                    if (obj.file.indexOf('.m3u8') > 0) {
                        type = 'm3u8';
                    };
                    libs.postMsg(obj.file, type, obj.max, libs.getQuality(quality), 4, cast, false, {
                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                        'Referer': that.HEADER.referer
                    });
                })
                $.each(streamjson, function (item) {

                });
            }
        };

        //https://lookmovie.io/movies/view/lee-evans-roadrunner-2011
        var lookmovie_ag = {
            DOMAIN: 'lookmovie.io',
            GENRE: 'all',
            LANGUAGE: 'all',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                'referer': 'https://lookmovie.io/'
            },
            getHosts: async function (movieInfo, libs) {
                var that = this;
                var detailURL = '';

                if (movieInfo.sourceURL.length == 0) {

                    var jj = await libs.postHTML({
                        url: 'https://' + this.DOMAIN,
                        headers: that.HEADER,
                        type: 'web',
                        script: 'var postMsg=' + that.postMsg + ';var botScript = ' + that.search + ';botScript("' + movieInfo.title.trim() + '");'
                    });

                    var json = JSON.parse(jj);
                    if (json.total <= 0) {
                        return;
                    };

                    for (var i = 0; i < json.result.length; i++) {
                        var item = json.result[i];
                        if (item.title == movieInfo.title &&
                            ((item.year == movieInfo.year && movieInfo.show == false) || (item.year == movieInfo.yearFirstSeason && movieInfo.show == true))) {
                            detailURL = 'https://' + that.DOMAIN + item.path;
                            break;
                        }
                    }
                } else {
                    detailURL = movieInfo.sourceURL;
                };

                if (detailURL.length == 0) {
                    return;
                };
                if (movieInfo.show == true) {
                    detailURL = detailURL.replace(that.DOMAIN, 'lmplayer.xyz').replace('/shows/view/', '/s/play/1/').replace('/movies/view/', '/m/play/1/') + "/s";
                    var detail = await libs.postHTML({
                        url: detailURL,
                        headers: that.HEADER,
                        type: 'afn'
                    });

                    var seasons = JSON.parse(/window.seasons='(.*?)';/ig.exec(detail)[1].replace(/\\/g, ''));
                    if (seasons[movieInfo.season] == null) {
                        return;
                    };
                    var episodes = seasons[movieInfo.season].episodes;
                    if (episodes[movieInfo.episodeNumber] == null) {
                        return;
                    }

                    var episodeId = episodes[movieInfo.episodeNumber].id_episode;
                    if (episodeId.length == 0) {
                        return;
                    };

                    detailURL = detailURL + '#S' + movieInfo.season + '-E' + movieInfo.episodeNumber + '-' + episodeId;
                }

                that.HEADER.referer = 'https://' + that.DOMAIN;
                var jj = await libs.postHTML({
                    url: detailURL,
                    headers: that.HEADER,
                    type: 'web',
                    script: 'var postMsg=' + that.postMsg + ';var botScript = ' + that.check_ddos + ';botScript();'
                });
                var quality = '';
                var json = JSON.parse(jj);
                $.each(json, function (label, file) {
                    var type = 'mp4';
                    var cast = true;
                    if (file.indexOf('.m3u8') > 0) {
                        type = 'm3u8';
                    };
                    if (file.indexOf('/earth-1984/') > 0) {
                        return true;
                    }
                    if (file.indexOf('http') < 0) {
                        file = 'https://' + that.DOMAIN + file;
                    }
                    libs.postMsg(file, type, label.replace('p', ''), libs.getQuality(quality), 5, cast, false, {
                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                        'Referer': detailURL
                    });
                })

            },
            postMsg: function (json) {

                if (typeof jsinterface != 'undefined') {
                    jsinterface.getSomeString(JSON.stringify(json));
                } else if (typeof window != 'undefined' && typeof window.webkit != 'undefined' && window.webkit && window.webkit.messageHandlers) {
                    window.webkit.messageHandlers.callbackHandler.postMessage(JSON.stringify(json));
                } else {
                    window.flutter_inappwebview.callHandler("jsinterface", json);
                }
            },
            search: function (keyword) {
                if (window.location === window.parent.location) {
                    setTimeout(function () {
                        postMsg({});
                    }, 10000);

                    var timer1 = setInterval(function () {
                        var searchInput = $('#search_input');
                        if (searchInput.val().length == 0) {
                            clearInterval(timer1);
                            $('#search_input').val(keyword);
                            $('#search_input').keyup();

                            var timer2 = setInterval(function () {
                                var items = $('.top-search__item > a');
                                if (items.length > 0) {
                                    var results = [];
                                    for (var i = 0; i < items.length; i++) {
                                        var item = items[i];
                                        var yearItem = $(item).find('.result-meta__title > span');
                                        var year = parseInt(yearItem.text().replace('(', '').replace(')', '').trim());
                                        yearItem.remove();
                                        var title = $(item).find('.result-meta__title').text();
                                        if (title.length == 0) {
                                            continue;
                                        };
                                        results.push({
                                            year: year,
                                            title: title,
                                            path: $(item).attr('href')
                                        });
                                    }
                                    postMsg({
                                        total: results.length,
                                        result: results
                                    });
                                    clearInterval(timer2);
                                }
                            }, 100);
                        }
                    }, 100);
                }
            },
            check_ddos: function () {
                if (window.location === window.parent.location) {
                    setTimeout(function () {
                        postMsg({});
                    }, 10000);
                    var timer2 = setInterval(function () {
                        if (typeof window.videoJS != 'undefined' && window.videoJS.hasOwnProperty('vhs') && window.videoJS.vhs.source_ != null && window.videoJS.vhs.source_.src.length > 0) {
                            clearInterval(timer2);
                            postMsg({
                                '720p': window.videoJS.vhs.source_.src
                            });
                        } else if (typeof window.videoJS.currentSrc != 'undefined' && window.videoJS.currentSrc().length > 0) {
                            clearInterval(timer2);
                            postMsg({
                                '720p': window.videoJS.currentSrc()
                            });
                        }
                    }, 100);
                };
            }
        };

        // https://lookmovie.club/mortal-kombat-legends-battle-of-the-realms-2021/
        var lookmovie_club = {
            DOMAIN: 'lookmovie.club',
            GENRE: 'all',
            LANGUAGE: 'all',
            HEADER: {
                'user-agent': '',
                'referer': 'https://lookmovie.club'
            },
            getHosts: async function (movieInfo, libs) {
                var that = this;
                this.HEADER['user-agent'] = libs.randomUserAgent();
                var detailURL = '';

                if (movieInfo.sourceURL.length > 0) {
                    detailURL = movieInfo.sourceURL;
                } else {
                    var keyword = '';
                    var input = movieInfo.title.trim().toLowerCase();
                    keyword = libs.slug(input).replace(/\-/g, '+');
                    var searchURL = 'https://' + that.DOMAIN + '/?s=' + keyword;
                    var html = await libs.postHTML({
                        url: searchURL,
                        headers: that.HEADER,
                        type: 'afn'
                    });

                    var path = libs.slug(input);
                    if (movieInfo.show == true) {
                        path = path + '-s' + libs.digitalNumber(movieInfo.season) + '-e' + libs.digitalNumber(movieInfo.episodeNumber) + '-';
                    };
                    var tag = $(html).find('article.latestpost');
                    for (var i = 0; i < tag.length; i++) {
                        try {
                            var item = $(tag[i]).find('a#featured-thumbnail');
                            var year = parseInt($(tag[i]).find('.thecategory > a:eq(0)').text());
                            if ($(item).attr('href').indexOf('/' + path) > 0) {
                                detailURL = $(item).attr('href');
                                break;
                            }
                        } catch (error) {

                        }
                    }
                };

                if (detailURL.length == 0) {
                    return;
                };

                this.HEADER.referer = detailURL;
                var detail = await libs.postHTML({
                    url: detailURL,
                    headers: that.HEADER,
                    type: 'afn'
                });

                detailURL = $(detail).find('iframe.frame').attr('src');
                detail = await libs.postHTML({
                    url: detailURL,
                    headers: that.HEADER,
                    type: 'afn'
                });

                $(detail).each(async function (index, item) {
                    if ($(item).prop("tagName") == 'IFRAME') {
                        detailURL = $(item).attr('src');
                        detail = await libs.postHTML({
                            url: detailURL,
                            headers: that.HEADER,
                            type: 'afn'
                        });

                        var parser = new DOMParser();
                        var doc = parser.parseFromString(detail, "text/html");
                        doc.querySelectorAll('video > source').forEach(function (item) {
                            var file = $(item).attr('src');
                            var type = 'mp4';
                            var cast = true;
                            if (file.indexOf('.m3u8') > 0) {
                                type = 'm3u8';
                            };
                            if (file.indexOf('.webm') > 0) {
                                return true;
                            }
                            libs.postMsg(file, type, libs.getResolution(file), libs.getQuality(file), 3, cast, false, {
                                'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                                'Referer': detailURL
                            });
                        });
                        return;
                    }
                });
            }
        };

        // https://lookmovie.la/?s=see+no+evil
        // var lookmovie_la = {
        //     DOMAIN: 'lookmovie.la',
        //     GENRE: 'all',
        //     LANGUAGE: 'all',
        //     HEADER: {
        //         'user-agent': '',
        //         'referer': 'https://lookmovie.la'
        //     },
        //     getHosts : async function (movieInfo, libs) {
        //         var that = this;
        //         this.HEADER['user-agent'] = libs.randomUserAgent();
        //         var detailURL = '';
        //         if(movieInfo.sourceURL.length > 0){
        //             detailURL = movieInfo.sourceURL;
        //         } else {
        //             var keyword = '';
        //             var input = movieInfo.title.trim().toLowerCase();
        //             keyword = libs.slug(input).replace(/\-/g, '+');
        //             var searchURL = 'https://'+that.DOMAIN+'/?s=' + keyword;
        //             var html = await libs.postHTML({
        //                 url: searchURL,
        //                 headers: that.HEADER,
        //                 type: 'web',
        //                 script: 'var postMsg='+that.postMsg+';var botScript = ' + that.check_ddos + ';botScript();'
        //             });

        //             var path = 'watch-movie';
        //             if(movieInfo.show == true){
        //                 path = 'watch-series';
        //             };
        //             var tag = $(html).find('.ml-item');
        //             for(var i=0;i<tag.length;i++){
        //                 var item = tag[i];
        //                 var title = $(item).find('.qtip-title').text().trim().toLowerCase();
        //                 var url = $(item).find('a[href*="/'+path+'/"]').attr('href');

        //                 if(input + ' (' + movieInfo.year + ')' == title){
        //                     detailURL = url;
        //                     break;
        //                 } else if(input == title) {
        //                     if(movieInfo.year == parseInt($(item).find('.jt-info:eq(1)').text().trim())){
        //                         detailURL = url;
        //                         break;
        //                     }
        //                 };
        //             }
        //         };

        //         if(detailURL.length == 0){
        //             return;
        //         };

        //         this.HEADER.referer = detailURL;

        //         var quality = '';
        //         var episodeURL = detailURL;
        //         if(movieInfo.show == true){
        //             var detail = await libs.postHTML({
        //                 url: detailURL,
        //                 headers: that.HEADER,
        //                 type: 'afn'
        //             });

        //             var season = $(detail).find('.seasonbox:contains("Season '+ movieInfo.season +'")');
        //             if(season.length == 0) {
        //                 return;
        //             }

        //             episodeURL = season.attr('href') + '?WatchNow=' + movieInfo.episodeNumber;
        //         }else {
        //             episodeURL = episodeURL + '?WatchNow=1' + movieInfo.episodeNumber;
        //         }

        //         var episodes = await libs.postHTML({
        //             url: episodeURL,
        //             headers: that.HEADER,
        //             type: 'web',
        //             script: 'var postMsg='+that.postMsg+';var botScript = ' + that.check_ddos + ';botScript();'
        //         });

        //         $(episodes).find('.show_player > iframe').each(async function(index, elm){
        //             var src = $(elm).attr('data-url');
        //             if(src.length == 0){
        //                 src = $(elm).attr('src');
        //             }
        //             if(src.length > 0) {
        //                 $.fn.botStream.hostStreams.push({
        //                     quality: quality,
        //                     url: src
        //                 });
        //             }
        //         });
        //     },
        //     postMsg : function (html) {
        //         if(typeof jsinterface != 'undefined'){
        //             jsinterface.getSomeString(html);
        //         } else if(typeof window != 'undefined' && typeof window.webkit != 'undefined' && window.webkit && window.webkit.messageHandlers) {
        //             window.webkit.messageHandlers.callbackHandler.postMessage(html);
        //         } else {
        //             window.flutter_inappwebview.callHandler("jsinterface",html);
        //         }
        //     },
        //     check_ddos : function () {
        //         if(window.location === window.parent.location){
        //             setTimeout(function() {
        //                 postMsg('');
        //             }, 10000);
        //             var timer = setInterval(function() {
        //                 if(document.querySelector('title').innerText !== 'Just a moment...'){
        //                     clearInterval(timer);
        //                     var html = document.querySelector('body').innerHTML;
        //                     postMsg(html);
        //                 }
        //             }, 100);
        //         };
        //     }
        // };

        // //https://ww2.123moviess.sc/searching/punk+d+season+1/
        // var moviess123_sc = {
        //     DOMAIN: 'ww2.123moviess.sc',
        //     GENRE: 'all',
        //     LANGUAGE: 'all',
        //     HEADER: {
        //         'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
        //         'referer': 'https://ww2.123moviess.sc'
        //     },
        //     getHosts : async function (movieInfo, libs) {
        //         if(movieInfo.show == false){
        //             return;
        //         }
        //         var that = this;
        //         var detailURL = '';
        //         if(movieInfo.sourceURL.length == 0){
        //             var keyword = '';
        //             var input = movieInfo.title.trim().toLowerCase().replace('\'', ' ');
        //             keyword = libs.slug(input).replace(/\-/g, '+');
        //             var searchURL = 'https://'+that.DOMAIN+'/searching/' + keyword + '/';
        //             var html = await libs.postHTML({
        //                 url: searchURL,
        //                 headers: that.HEADER,
        //                 type: 'afn'
        //             });

        //             var sluged = libs.slug(movieInfo.title.trim());
        //             urlPattern = '';
        //             if(movieInfo.show == true){
        //                 urlPattern = '/serie/'+sluged + '-season-' + movieInfo.season;
        //             } else {
        //                 urlPattern = '/film/'+sluged;
        //             };

        //             $(html).find('article.movies > a').each(async function(index, item){
        //                 if(detailURL.length == 0){
        //                     var year = parseInt($(item).parent().find('header .year > a[href*="/release-year/"]').text().trim());
        //                     if(isNaN(year) || year != movieInfo.year){
        //                         return true;
        //                     }
        //                     var url = $(item).attr('href');

        //                     if(url.indexOf(urlPattern) > 0){
        //                         detailURL = url;
        //                         return false;
        //                     };
        //                 }
        //             });
        //         } else {
        //             detailURL = movieInfo.sourceURL;
        //         }

        //         if(detailURL.length == 0){
        //             return;
        //         };

        //         this.HEADER.referer = detailURL
        //         var detail = await libs.postHTML({
        //             url: detailURL,
        //             headers: that.HEADER,
        //             type: 'afn'
        //         });

        //         var quality = $(detail).find('.entry-meta > span.quality').text();
        //         if(movieInfo.show == false){
        //             var movieId = $(detail).find('div.votes-stars').attr('data-id');
        //             that.HEADER.referer = detailURL;
        //             that.HEADER['x-requested-with'] = 'XMLHttpRequest';
        //             var jj = await libs.postHTML({
        //                 url: 'https://'+that.DOMAIN + '/wp-admin/admin-ajax.php',
        //                 headers: that.HEADER,
        //                 type: 'afn',
        //                 method: 'post',
        //                 data: {
        //                     action: 'fkingyrfather',
        //                     id: movieId,
        //                     annoying: 'videospider'
        //                 }
        //             });

        //             var json = JSON.parse(jj);
        //             if(json.status == 1) {
        //                 $.fn.botStream.hostStreams.push({
        //                     quality: quality,
        //                     url: json.url
        //                 });
        //             }

        //             return;
        //         }

        //         $(detail).find('ul.episodes-list > li').each(function(index, item) {
        //             $(item).find('a > span.option').remove();
        //             var episodeName = $(item).find('a').text().trim();
        //             if(episodeName != 'Episode ' + libs.digitalNumber(movieInfo.episodeNumber)) {
        //                 return true;
        //             };
        //             var dataos = $(item).attr('data-os');
        //             if(dataos.indexOf('http') == 0){
        //                 $.fn.botStream.hostStreams.push({
        //                     quality: quality,
        //                     url: dataos
        //                 });
        //             } else if(dataos.length > 0){
        //                 $.fn.botStream.hostStreams.push({
        //                     quality: quality,
        //                     url: 'https://vidoo.streamango.to/e/' + dataos
        //                 });
        //             }
        //         });
        //     }
        // };

        //Philippines
        //https://pinoymovies.vip/search?q=through+night+and+day
        var pinoymoviess_com = {
            DOMAIN: 'pinoymovies.vip',
            GENRE: 'all',
            COUNTRY: 'philippines',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getHosts: async function (movieInfo, libs) {
                if (movieInfo.show == true) {
                    return;
                };
                var that = this;
                var detailURL = '';
                var title = movieInfo.title;
                if (movieInfo.originalTitle.length > 0) {
                    title = movieInfo.originalTitle;
                };
                if (movieInfo.sourceURL.length == 0) {
                    var searchURL = 'https://' + this.DOMAIN + '/search?q=' + encodeURIComponent(title.toLowerCase()).replace(/%20/g, '+');
                    this.HEADER.referer = 'https://' + this.DOMAIN;
                    var html = await libs.postHTML({
                        url: searchURL,
                        headers: this.HEADER,
                        type: 'web',
                        script: 'var postMsg=' + that.postMsg + ';var botScript = ' + that.check_ddos + ';botScript();'
                    });
                    var sluged = libs.slug(title + ' ' + movieInfo.year);
                    var urlPattern = '/watch/' + sluged + '.html';

                    $(html).find('.movies-list > .ml-item > a[href*="/watch/"]').each(async function (index, item) {
                        var url = $(item).attr('href');
                        if (url.indexOf(urlPattern) > 0) {
                            detailURL = url;
                            return false;
                        };
                    });
                } else {
                    detailURL = movieInfo.sourceURL;
                };

                if (detailURL.length == 0) {
                    return;
                };

                var detail = await libs.postHTML({
                    url: detailURL,
                    headers: that.HEADER,
                    type: 'afn'
                });
                var quality = $(detail).find('span.quality').text().trim().replace(' ', '');

                $(detail).find('.movieplay > iframe').each(function (index, item) {
                    var host = $(item).attr('src');
                    if (host != null && host.length > 0) {
                        if (host.indexOf('http') < 0) {
                            host = 'https:' + host;
                        };
                        $.fn.botStream.hostStreams.push({
                            quality: quality,
                            url: host
                        });
                    }
                })
            },
            postMsg: function (html) {

                if (typeof jsinterface != 'undefined') {
                    jsinterface.getSomeString(html);
                } else if (typeof window != 'undefined' && typeof window.webkit != 'undefined' && window.webkit && window.webkit.messageHandlers) {
                    window.webkit.messageHandlers.callbackHandler.postMessage(html);
                } else {
                    window.flutter_inappwebview.callHandler("jsinterface", html);
                }
            },
            check_ddos: function () {
                if (window.location === window.parent.location) {
                    setTimeout(function () {
                        postMsg('');
                    }, 10000);
                    var timer = setInterval(function () {
                        if (document.querySelector('title').innerText !== 'Just a moment...') {
                            clearInterval(timer);
                            var html = document.querySelector('body').innerHTML;
                            postMsg(html);
                        }
                    }, 100);
                };
            }
        };

        // //https://kisscartoon.gr/?s=Air+Bud+3%3A+World+Pup
        // var kisscartoon_love = {
        //     DOMAIN: 'kisscartoon.gr',
        //     GENRE: 'all',
        //     LANGUAGE: 'all',
        //     HEADER: {
        //         'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
        //     },
        //     getHosts : async function (movieInfo, libs) {     
        //         var that = this;
        //         var detailURL = '';
        //         var title = movieInfo.title;
        //         if(movieInfo.originalTitle.length > 0){
        //             title = movieInfo.originalTitle;
        //         };
        //         if(movieInfo.sourceURL.length == 0){
        //             var searchURL = 'https://' + this.DOMAIN + '/?s=' + encodeURIComponent(title.toLowerCase()).replace(/%20/g, '+');
        //             this.HEADER.referer = 'https://' + this.DOMAIN;
        //             var html = await libs.postHTML({
        //                 url: searchURL,
        //                 headers: this.HEADER,
        //                 type: 'afn'
        //             });
        //             var sluged = '';
        //             if(movieInfo.show == true) {
        //                 sluged = libs.slug(title + ' season ' + movieInfo.season);
        //             } else {
        //                 sluged = libs.slug(title + ' ' + movieInfo.year);
        //             }
        //             var urlPattern = '/'+sluged + '/';

        //             $(html).find('.movies-list > .ml-item > a.ml-mask').each(async function(index, item){
        //                 var url = $(item).attr('href');
        //                 if(url.indexOf(urlPattern) > 0){
        //                     detailURL = url;
        //                     return false;
        //                 };
        //             });
        //         } else {
        //             detailURL = movieInfo.sourceURL;
        //         };

        //         if(detailURL.length == 0){
        //             return;
        //         };

        //         if(movieInfo.show == true) {
        //             var tmp = detailURL.split('/');
        //             detailURL = tmp[0] + '//' + tmp[2] + '/episode/' + tmp[3] + '-episode-' + movieInfo.episodeNumber + '/';
        //         } 

        //         var detail = await libs.postHTML({
        //             url: detailURL,
        //             headers: that.HEADER,
        //             type: 'afn'
        //         });

        //         var quality = '';
        //         if(detailURL.indexOf('-dub-') > 0) {
        //             quality = 'HD-DUB';
        //         }
        //         var match = /var filmId = "(\d+)"/ig.exec(detail);
        //         if(match == null || match.length == 0){
        //             return;
        //         }
        //         var filmId = match[1];
        //         $(detail).find('.list-server > select.form-control > option').each(async function(index, item) {
        //             var serverName = $(item).attr('value');
        //             that.HEADER.referer = detailURL;
        //             var host = await libs.postHTML({
        //                 url: 'https://' + that.DOMAIN + '/ajax-get-link-stream/?server=' + serverName + '&filmId=' + filmId,
        //                 headers: that.HEADER,
        //                 type: 'afn'
        //             });
        //             if(host.length > 0){
        //                 $.fn.botStream.hostStreams.push({
        //                     quality: quality,
        //                     url: host,
        //                     referer: detailURL
        //                 });
        //             }
        //         })
        //     }
        // };

        //Thailan
        //https://www1.dramacool.fo/train-2020-episode-2.html
        var dramacool_movie = {
            DOMAIN: 'www1.dramacool.fo',
            GENRE: 'all',
            LANGUAGE: 'all',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getHosts: async function (movieInfo, libs) {
                var urls = [];
                if (movieInfo.show == true) {
                    if (movieInfo.sourceURL.length > 0) {
                        urls.push(movieInfo.sourceURL + '-episode-' + movieInfo.episodeNumber + '.html');
                    } else {
                        var season = '';
                        if (movieInfo.season > 1) {
                            season = '-season-' + movieInfo.season;
                        }
                        urls.push('https://' + this.DOMAIN + '/' + libs.slug(movieInfo.title.toLowerCase()) + season + '-episode-' + movieInfo.episodeNumber + '.html');
                    }
                } else {
                    if (movieInfo.sourceURL.length > 0) {
                        urls.push(movieInfo.sourceURL + '-episode-1.html');
                    } else {
                        urls.push('https://' + this.DOMAIN + '/' + libs.slug(movieInfo.title.toLowerCase()) + '-episode-1.html');
                    }
                }
                if ($.inArray('english', movieInfo.languages) >= 0) {
                    return;
                };
                for (var i = 0; i < urls.length; i++) {
                    var html = await libs.postHTML({
                        url: urls[i],
                        headers: this.HEADER,
                        type: 'afn'
                    });

                    var quality = '';
                    $(html).find('.anime_muti_link > ul > li').each(async function (index, item) {
                        var href = libs.getAbsoluteUrl($(item).attr('data-video'));
                        if (href.length > 0) {
                            $.fn.botStream.hostStreams.push({
                                quality: quality,
                                url: href
                            });
                        }
                    });
                }
            }
        };

        //https://www.dailymotion.com/search/Falaknuma%20Das/videos
        var dailymotion_com = {
            DOMAIN: 'www.dailymotion.com',
            GENRE: 'all',
            LANGUAGE: 'all',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getHosts: async function (movieInfo, libs) {
                var that = this;
                var detailURL = '';
                if (movieInfo.sourceURL.length == 0) {
                    return;
                    // var keyword = movieInfo.title;
                    // if(movieInfo.show == true) {
                    //     keyword = keyword + ' S' + libs.digitalNumber(movieInfo.season) + 'E' + libs.digitalNumber(movieInfo.episodeNumber);
                    // } else if(keyword.indexOf(movieInfo.year) < 0) {
                    //     keyword = keyword + ' ' + movieInfo.year;
                    // };
                    // var searchURL = 'https://' + this.DOMAIN + '/search/' + encodeURIComponent(keyword) + '/videos';
                    // this.HEADER.referer = 'https://' + this.DOMAIN;
                    // var html = await libs.postHTML({
                    //     url: searchURL,
                    //     headers: this.HEADER,
                    //     type: 'web',
                    //     script: 'var postMsg='+that.postMsg+';var botScript = ' + that.check_ddos + ';botScript();'
                    // });
                    // var sluged = libs.slug(keyword);
                    // $(html).find('div[class^="VideoSearchCard__videoImageWrapper_"] > a[href^="/video/"]').each(async function(index, item){
                    //     if(detailURL.length == 0){
                    //         var url = $(item).attr('href');
                    //         var title = libs.slug($(item).parent().parent().find('div[class^="VideoSearchCard__videoTitle__"] > a').text().toLowerCase());
                    //         var yes = false;
                    //         var duration = $(item).find('div[class^="VideoDurationTag__videoDuration__"]').text();
                    //         var parts = duration.split(':');
                    //         if(parts.length == 3) {
                    //             yes = true;
                    //         } else if(parts.length == 2) {
                    //             if(parseInt(parts[0]) >= 4){
                    //                 yes = true;
                    //             }
                    //         } else {
                    //             return true;
                    //         }
                    //         if(yes == true && title.indexOf('-trailer') <= 0 && title.indexOf('-review') <= 0 && title.indexOf(sluged) >= 0){
                    //             detailURL = url;
                    //             return false;
                    //         };
                    //     }
                    // });
                } else {
                    detailURL = movieInfo.sourceURL;
                };

                if (detailURL.length == 0) {
                    return;
                };

                if (detailURL.indexOf('/embed/video/') < 0) {
                    detailURL = detailURL.replace('/video/', '/embed/video/');
                };
                if (detailURL.indexOf('/') == 0) {
                    detailURL = 'https://' + this.DOMAIN + detailURL;
                };

                var detail = await libs.postHTML({
                    url: detailURL,
                    headers: that.HEADER,
                    type: 'afn'
                });
                var match = /var config = (.*?);/ig.exec(detail);
                if (match == null || match.length == 0) {
                    return;
                };

                var json = JSON.parse(match[1]);
                that.HEADER.referer = 'https://www.dailymotion.com';
                $.each(json.metadata.qualities, function (name, value) {
                    var resolution = '';
                    if (name != 'auto') {
                        resolution = name;
                    };
                    json.metadata.qualities[name].some(function (item) {
                        var type = 'mp4';
                        var cast = true;
                        if (item.url.indexOf('.m3u8') > 0) {
                            type = 'm3u8';
                        };
                        libs.postMsg(item.url + '&progressive=1', type, libs.getResolution(resolution), libs.getQuality(''), 3, cast, true, {
                            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                            'Referer': that.HEADER.referer
                        });
                    })
                })
            },
            postMsg: function (html) {

                if (typeof jsinterface != 'undefined') {
                    jsinterface.getSomeString(html);
                } else if (typeof window != 'undefined' && typeof window.webkit != 'undefined' && window.webkit && window.webkit.messageHandlers) {
                    window.webkit.messageHandlers.callbackHandler.postMessage(html);
                } else {
                    window.flutter_inappwebview.callHandler("jsinterface", html);
                }
            },
            check_ddos: function () {
                if (window.location === window.parent.location) {
                    setTimeout(function () {
                        postMsg('');
                    }, 10000);
                    var timer = setInterval(function () {
                        if (document.querySelector('title').innerText !== 'Just a moment...' && document.querySelectorAll('a[href*="/video/"]').length) {
                            clearInterval(timer);
                            var html = document.querySelector('body').innerHTML;
                            postMsg(html);
                        }
                    }, 100);
                };
            }
        };

        //Turkey
        //https://turkish123.com/hercai-episode-13/
        var turkish123_com = {
            DOMAIN: 'turkish123.com',
            GENRE: 'all',
            COUNTRY: 'turkey',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getHosts: async function (movieInfo, libs) {
                if (movieInfo.show == false) {
                    return;
                };
                var that = this;

                var detailURL = '';
                if (movieInfo.sourceURL.length > 0) {
                    detailURL = movieInfo.sourceURL;
                } else {
                    var sluged = libs.slug(movieInfo.title);
                    detailURL = 'https://' + that.DOMAIN + '/' + sluged;
                };

                if (detailURL.length == 0) {
                    return;
                }

                detailURL = detailURL + '-episode-' + movieInfo.episodeNumberSeasons + '/';
                var detail = await libs.postHTML({
                    url: detailURL,
                    headers: that.HEADER,
                    type: 'afn'
                });

                var quality = $(detail).find('span.quality').text().replace('720', '').replace('1080', '').trim();
                $(detail).find('#player2 .movieplay > iframe').each(function (index, item) {
                    var host = $(item).attr('src');
                    if (host.length > 0) {
                        if (host.indexOf('http') < 0) {
                            host = 'https:' + host;
                        };
                        $.fn.botStream.hostStreams.push({
                            quality: quality,
                            url: host
                        });
                    }
                });
                $(detail).find('.movieplay > script:contains("<iframe")').each(function (index, item) {
                    $.each($(item).html().split('copyText='), function (i, tag) {
                        var match = /src="(.*?)"/ig.exec(tag);
                        if (match != null && match.length > 0) {
                            var host = match[1];
                            if (host.length > 0) {
                                if (host.indexOf('http') < 0) {
                                    host = 'https:' + host;
                                };
                                $.fn.botStream.hostStreams.push({
                                    quality: quality,
                                    url: host,
                                    referer: detailURL
                                });
                            }
                        }
                    })
                })
            }
        };

        //https://ww5.0123movie.net/search/Bad+Boys+For+Life.html
        var movie0123_net = {
            DOMAIN: 'ww5.0123movie.net',
            GENRE: 'all',
            LANGUAGE: 'all',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36'
            },
            getHosts: async function (movieInfo, libs) {
                var that = this;
                var detailURL = '';

                if (movieInfo.sourceURL.length == 0) {
                    var keyword = movieInfo.title;
                    if (movieInfo.show == true) {
                        keyword += ' - Season ' + movieInfo.season;
                    };
                    var searchURL = 'https://' + this.DOMAIN + '/search/' + encodeURIComponent(keyword).replace(/%20/g, '+') + '.html';
                    this.HEADER.referer = 'https://' + this.DOMAIN;
                    var search = await libs.postHTML({
                        url: searchURL,
                        headers: this.HEADER,
                        type: 'afn'
                    });
                    var sluged_1 = libs.slug(keyword);
                    var sluged_2 = libs.slug(movieInfo.title + ' ' + movieInfo.season);

                    $(search).find('.list-item > a').each(async function (index, item) {
                        if (detailURL.length == 0) {
                            var title = $(item).attr('title').toLowerCase();
                            if (title.indexOf('[18+]') == 0) {
                                title = libs.slug(title.replace('[18+]', '').trim());
                            } else {
                                title = libs.slug(title.trim());
                            };
                            if ((movieInfo.show == false && title == sluged_1) ||
                                (movieInfo.show == true && (title == sluged_2 || title == sluged_1))) {
                                detailURL = $(item).attr('href');
                                return true;
                            }
                        }
                    });
                } else {
                    detailURL = movieInfo.sourceURL;
                }
                if (detailURL.length == 0) {
                    return;
                };

                detailURL = detailURL + '?play=1';
                var detail = await libs.postHTML({
                    url: detailURL,
                    headers: that.HEADER,
                    type: 'afn'
                });

                var year = $(detail).find('a[href^="/release/"]').text();
                if (movieInfo.year != year) {
                    return;
                }
                var quality = $(detail).find('.card-body .badge-dark').text();
                var peid = movieInfo.episodeNumber;
                if (movieInfo.show == false) {
                    peid = 1;
                };
                if ($(detail).find('#episodes-sv > .ep-item[data-id="' + peid + '"]').length == 0) {
                    return;
                };
                var pmid = detailURL.replace(new RegExp('(.*)-([0-9]+)\.html(.*)', 'ig'), '$2');
                $(detail).find('.server-item a.dropdown-item').each(async function (index, item) {
                    var psrv = $(item).attr('data-id');
                    that.HEADER.referer = detailURL;
                    var streamjj = await libs.postHTML({
                        url: 'https://' + that.DOMAIN + '/movie_embed.html',
                        headers: that.HEADER,
                        type: 'afn',
                        method: 'post',
                        data: {
                            pmid: pmid,
                            peid: peid,
                            psrv: psrv
                        }
                    });

                    var json = JSON.parse(streamjj);
                    if (json.status == true) {
                        $.fn.botStream.hostStreams.push({
                            quality: quality,
                            url: json.src,
                            referer: detailURL
                        });
                    }
                })
            }
        };
        //#endregion


        //#region HOST STREAM
        //https://www8.kisscartoon.love/mystream.php?id=n56vn90cyq02
        var host_kisscartoon_love = {
            DOMAIN: 'www1.kisscartoon.love',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                this.HEADER.referer = hostInfo.referer;
                var html = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'afn'
                });
                var title = '';
                $(html).each(await function (index, item) {
                    if ($(item).prop("tagName") == 'TITLE') {
                        title = $(item).text();
                        return false;
                    }
                });
                var quality = libs.getQuality(title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };

                html.split('sources:').some(function (item) {
                    var pp = item.split('});');
                    if (pp.length >= 2) {
                        try {
                            var streams = eval(pp[0].trim().replace('],', ']'));
                            streams.some(function (obj) {
                                var type = 'mp4';
                                var cast = true;
                                if (obj.file.indexOf('.m3u8') > 0) {
                                    type = 'm3u8';
                                    cast = false;
                                };
                                if (obj.file.indexOf('error.com') > 0) {
                                    return false;
                                };
                                libs.postMsg(obj.file, type, libs.getResolution(obj.file), quality, 4, cast, false, {
                                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                                    'Referer': hostInfo.url,
                                    'Origin': 'https://' + libs.extractHostname(hostInfo.url)
                                });
                            })
                        } catch (error) {

                        }
                    }
                });
            }
        };
        $.fn.botStream.hosts.push(host_kisscartoon_love);

        //https://vidnext.net/loadserver.php?id=MzM2MzIy&title=Reunion&typesub=SUB&sub=L3JldW5pb24vcmV1bmlvbi52dHQ=&cover=Y292ZXIvcmV1bmlvbi1sYXJnZS5wbmc=
        var vidnext_net = {
            DOMAIN: 'vidnext.net',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                this.HEADER.referer = hostInfo.referer;
                var html = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'afn'
                });
                var title = '';
                $(html).each(await function (index, item) {
                    if ($(item).prop("tagName") == 'TITLE') {
                        title = $(item).text();
                        return false;
                    }
                });
                var quality = libs.getQuality(title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };

                html.split('sources:').some(function (item) {
                    var pp = item.split('}],');
                    if (pp.length >= 2) {
                        try {
                            var streams = eval(pp[0].trim() + '}]');
                            streams.some(function (obj) {
                                var type = 'mp4';
                                var cast = true;
                                if (obj.file.indexOf('.m3u8') > 0) {
                                    type = 'm3u8';
                                };
                                if (obj.file.indexOf('error.com') > 0) {
                                    return false;
                                };
                                libs.postMsg(obj.file, type, libs.getResolution(obj.file), quality, 4, cast, false, {
                                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                                    'Referer': hostInfo.url
                                });
                            })
                        } catch (error) {

                        }
                    }
                });
            }
        };
        $.fn.botStream.hosts.push(vidnext_net);

        //https://membed.net/streaming.php?id=MzYzNzEy&title=Top+Gun%3A+Maverick&typesub=SUB&sub=L3RvcC1ndW4tbWF2ZXJpY2svdG9wLWd1bi1tYXZlcmljay52NC52dHQ=&cover=Y292ZXIvdG9wLWd1bi1tYXZlcmljay1sYXJnZS5wbmc=
        var membed_net = {
            DOMAIN: 'membed.net',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var jj = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'web',
                    script: 'var postMsg = ' + this.postMsg + '; var botScript = ' + this.getVideoTag + '; botScript();'
                });

                var quality = libs.getQuality('');
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };

                $.fn.botStream.hostStreams.push({
                    quality: quality,
                    url: hostInfo.url.replace('streaming.php', 'load.php')
                });

                var json = JSON.parse(jj);
                json.some(function (obj) {
                    var type = 'mp4';
                    var cast = true;
                    if (obj.file.indexOf('.m3u8') > 0) {
                        type = 'm3u8';
                        cast = false;
                    };
                    libs.postMsg(obj.file, type, libs.getResolution(obj.file), quality, 4, cast, false, {
                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                        'Referer': hostInfo.url
                    });
                })
            },
            postMsg: function (json) {

                if (typeof jsinterface != 'undefined') {
                    jsinterface.getSomeString(JSON.stringify(json));
                } else if (typeof window != 'undefined' && typeof window.webkit != 'undefined' && window.webkit && window.webkit.messageHandlers) {
                    window.webkit.messageHandlers.callbackHandler.postMessage(JSON.stringify(json));
                } else {
                    window.flutter_inappwebview.callHandler("jsinterface", json);
                }
            },
            getVideoTag: function () {
                if (window.location === window.parent.location) {
                    setTimeout(function () {
                        postMsg({});
                    }, 10000);
                    var timerS = setInterval(function () {
                        if (typeof jwplayer !== 'undefined' && typeof jwplayer().getPlaylist() !== 'undefined' && jwplayer().getPlaylist().length > 0 && jwplayer().getPlaylist()[0].sources.length > 0) {
                            var sources = jwplayer().getPlaylist()[0].allSources;
                            var results = [];
                            for (var i = 0; i < sources.length; i++) {
                                if (typeof sources[i].file !== 'undefined') {
                                    var label = '';
                                    if (typeof sources[i].label !== 'undefined') {
                                        label = sources[i].label.replace('p', '');
                                        var parsed = parseInt(label);
                                        label = isNaN(parsed) ? 0 : parsed;
                                    };
                                    results.push({
                                        file: sources[i].file,
                                        label: label
                                    });
                                }
                            }
                            postMsg(results);
                            clearInterval(timerS);
                            return;
                        }
                    }, 100)
                }
            }
        };
        $.fn.botStream.hosts.push(membed_net);

        //https://streamani.net/streaming.php?id=MTIxNzk0&title=Naruto+Shippuuden+%28Dub%29+Episode+500
        var streamani_net = {
            DOMAIN: 'streamani.net',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('streamani.net', 'membed.net')
                });
            }
        };
        $.fn.botStream.hosts.push(streamani_net);

        //https://gogo-stream.com/streaming.php?id=MTIxNzk0
        var gogo_stream_com = {
            DOMAIN: 'gogo-stream.com',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('gogo-stream.com', 'membed.net')
                });
            }
        };
        $.fn.botStream.hosts.push(gogo_stream_com);

        var gogo_play_net = {
            DOMAIN: 'gogo-play.net',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('gogo-play.net', 'membed.net')
                });
            }
        };
        $.fn.botStream.hosts.push(gogo_play_net);

        var gogo_play_net = {
            DOMAIN: 'gogo-play.net',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('gogo-play.net', 'membed.net')
                });
            }
        };
        $.fn.botStream.hosts.push(gogo_play_net);

        var gogoplay1_com = {
            DOMAIN: 'gogoplay1.com',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('gogoplay1.com', 'membed.net')
                });
            }
        };
        $.fn.botStream.hosts.push(gogoplay1_com);

        //https://vidstreaming.io/streaming.php?id=MTIxNzk0&title=Naruto+Shippuuden+%28Dub%29+Episode+500
        var vidstreaming_io = {
            DOMAIN: 'vidstreaming.io',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('vidstreaming.io', 'membed.net')
                });
            }
        };
        $.fn.botStream.hosts.push(vidstreaming_io);

        //https://vidembed.cc/streaming.php?id=MzQzNDky&title=Lucifer+-+Season+5++Episode+15+-+Is+This+Really+How+It%27s+Going+to+End%3F%21&typesub=SUB&sub=L2x1Y2lmZXItc2Vhc29uLTUtZXBpc29kZS0xNS1pcy10aGlzLXJlYWxseS1ob3ctaXRzLWdvaW5nLXRvLWVuZC9sdWNpZmVyLXNlYXNvbi01LWVwaXNvZGUtMTUtaXMtdGhpcy1yZWFsbHktaG93LWl0cy1nb2luZy10by1lbmQudnR0&cover=Y292ZXIvbHVjaWZlci1zZWFzb24tNS1sYXJnZS5wbmc=
        var vidembed_cc = {
            DOMAIN: 'vidembed.cc',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('vidembed.cc', 'membed.net')
                });
            }
        };
        $.fn.botStream.hosts.push(vidembed_cc);

        //https://vidembed.io/streaming.php?id=MzYzNzEy
        var vidembed_io = {
            DOMAIN: 'vidembed.io',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('vidembed.io', 'membed.net')
                });
            }
        };
        $.fn.botStream.hosts.push(vidembed_io);

        var vidnode_net = {
            DOMAIN: 'vidnode.net',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var html = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'afn'
                });
                var title = '';
                $(html).each(await function (index, item) {
                    if ($(item).prop("tagName") == 'TITLE') {
                        title = $(item).text();
                        return false;
                    }
                });
                var quality = libs.getQuality(title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };

                $.fn.botStream.hostStreams.push({
                    quality: quality,
                    url: hostInfo.url.replace('streaming.php', 'load.php')
                });
                if (html.indexOf('urlVideo = ') > 0) {
                    html.split('sources:').some(function (item) {
                        var pattern = /(.*),/ig;
                        var match = pattern.exec(item);
                        try {
                            if (match && match.length > 1) {
                                var streams = eval(match[1]);
                                streams.some(function (obj) {
                                    var type = 'mp4';
                                    if (obj.file.indexOf('.m3u8') > 0) {
                                        type = 'm3u8';
                                    };
                                    libs.postMsg(obj.file, type, libs.getResolution(obj.file), quality, 4, true, false, {
                                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                                        'Referer': hostInfo.url,
                                        'Origin': 'https://' + libs.extractHostname(hostInfo.url)
                                    });
                                })
                            };
                        } catch (error) {

                        };

                        pattern = /urlVideo = (.*)/ig;
                        match = pattern.exec(item);
                        try {
                            if (match && match.length > 1) {
                                var file = match[1].replace('\';', '').replace('\'', '');
                                var type = 'mp4';
                                if (file.indexOf('.m3u8') > 0) {
                                    type = 'm3u8';
                                };
                                libs.postMsg(file, type, libs.getResolution(file), quality, 4, true, false, {
                                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                                    'Referer': hostInfo.url,
                                    'Origin': 'https://' + libs.extractHostname(hostInfo.url)
                                });
                            };
                        } catch (error) {

                        };
                    });
                } else {
                    html.split('sources:').some(function (item) {
                        var pp = item.split('],');
                        if (pp.length >= 2) {
                            try {
                                var streams = eval(pp[0] + ']');
                                streams.some(function (obj) {
                                    var type = 'mp4';
                                    if (obj.file.indexOf('.m3u8') > 0) {
                                        type = 'm3u8';
                                    };
                                    libs.postMsg(obj.file, type, libs.getResolution(obj.file), quality, 4, true, false, {
                                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                                        'Referer': hostInfo.url,
                                        'Origin': 'https://' + libs.extractHostname(hostInfo.url)
                                    });
                                })
                            } catch (error) {

                            }
                        }
                    });
                }
            }
        };
        $.fn.botStream.hosts.push(vidnode_net);

        //https://vidcloud9.com/streaming.php?id=MzAyMzg2
        var vidcloud9_com = {
            DOMAIN: 'vidcloud9.com',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var jdata = [];
                var quality = libs.getQuality('');
                if (hostInfo.url.indexOf('/goto.php?') > 0) {
                    jdata.push({
                        file: hostInfo.url
                    });
                    // var headersjj = await libs.postHTML({
                    //     url: hostInfo.url,
                    //     headers: this.HEADER,
                    //     type: 'afn',
                    //     selfsign: true,
                    //     method: 'head'
                    // });

                    // var json = JSON.parse(headersjj);
                    // if(json.hasOwnProperty('location')) {
                    //     jdata.push({
                    //         file: json.location
                    //     });
                    // } else if(json.hasOwnProperty('Location')) {
                    //     jdata.push({
                    //         file: json.Location
                    //     });
                    // } else {
                    //     return;
                    // }
                } else {
                    var url = hostInfo.url.replace('/load.php', '/ajax.php').replace('/streaming.php', '/ajax.php');
                    this.HEADER.referer = hostInfo.url;
                    this.HEADER['x-requested-with'] = 'XMLHttpRequest';
                    var html = await libs.postHTML({
                        url: url,
                        headers: this.HEADER,
                        type: 'afn'
                    });
                    if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                        quality = libs.getQuality(hostInfo.quality);
                    };
                    if (quality == null) {
                        quality = '';
                    } else {
                        quality = quality.name;
                    }

                    var json = JSON.parse(html);
                    if (json.hasOwnProperty('linkiframe')) {
                        $.fn.botStream.hostStreams.push({
                            quality: quality,
                            url: json.linkiframe
                        });
                    };

                    jdata = json.source;
                    if (json.hasOwnProperty('source_bk')) {
                        jdata = jdata.concat(json.source_bk);
                    };
                }

                jdata.some(function (obj) {
                    var type = 'mp4';
                    var cast = true;
                    if (obj.file.indexOf('.m3u8') > 0) {
                        type = 'm3u8';
                        cast = false;
                    };
                    if (obj.file.indexOf('error.com') > 0) {
                        return false;
                    };
                    libs.postMsg(obj.file, type, libs.getResolution(obj.file), libs.getQuality(quality), 4, cast, false, {
                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                        'Referer': hostInfo.url,
                        'Origin': 'https://' + libs.extractHostname(hostInfo.url)
                    });
                });
            }
        };
        $.fn.botStream.hosts.push(vidcloud9_com);

        //https://embed.dramacool.movie/streaming.php?id=MTk4OTk1&title=Bikeman+2+episode+1&typesub=SUB
        var embed_dramacool_movie = {
            DOMAIN: 'embed.dramacool.movie',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('embed.dramacool.movie', 'vidcloud9.com')
                });
            }
        };
        $.fn.botStream.hosts.push(embed_dramacool_movie);

        //https://vidlox.me/5nivgp5drqny
        var vidlox_me = {
            DOMAIN: 'vidlox.me',
            HEADER: {
                'user-agent': '',
            },
            getStream: async function (hostInfo, libs) {
                var html = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'afn'
                });
                var title = '';
                $(html).each(await function (index, item) {
                    if ($(item).prop("tagName") == 'TITLE') {
                        title = $(item).text();
                        return false;
                    }
                });
                var quality = libs.getQuality(title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };
                html.split('sources:').some(function (item) {
                    var pattern = /(.*),/ig;
                    var match = pattern.exec(item);
                    try {
                        if (match && match.length > 1) {
                            var streams = eval(match[1]);
                            streams.some(function (file) {
                                var type = 'mp4';
                                if (file.indexOf('.m3u8') > 0) {
                                    type = 'm3u8';
                                };
                                libs.postMsg(file, type, libs.getResolution(file), quality, 4, true, true, {
                                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                                    'Referer': hostInfo.url,
                                    'Origin': 'https://' + libs.extractHostname(hostInfo.url)
                                });
                            })
                        };
                    } catch (error) {

                    };
                });
            }
        };
        $.fn.botStream.hosts.push(vidlox_me);

        //https://vidoza.net/jpaci5p4s4rr.html
        var vidoza_net = {
            DOMAIN: 'vidoza.net',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var html = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'afn'
                });

                var title = '';
                $(html).each(await function (index, item) {
                    if ($(item).prop("tagName") == 'TITLE') {
                        title = $(item).text();
                        return false;
                    }
                });
                var quality = libs.getQuality(title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };

                html.split('sourcesCode:').some(function (item) {
                    var pattern = /(.*),/ig;
                    var match = pattern.exec(item);
                    try {
                        if (match && match.length > 1) {
                            var streams = eval(match[1]);
                            streams.some(function (obj) {
                                libs.postMsg(obj.src, 'mp4', obj.res, quality, 4, true, true, {
                                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                                    'Referer': hostInfo.url,
                                    'Origin': 'https://' + libs.extractHostname(hostInfo.url)
                                });
                            })
                        };
                    } catch (error) {

                    };
                });
            }
        };
        $.fn.botStream.hosts.push(vidoza_net);

        //https://vshare.eu/470j85wenili.htm
        var vshare_eu = {
            DOMAIN: 'vshare.eu',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var html = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'afn'
                });

                var title = '';
                $(html).each(await function (index, item) {
                    if ($(item).prop("tagName") == 'TITLE') {
                        title = $(item).text();
                        return false;
                    }
                });

                var data = {};
                $(html).find('form > input').each(await function (index, item) {
                    data[$(item).attr('name')] = $(item).attr('value');
                });

                if (jQuery.isEmptyObject(data)) {
                    return;
                }

                html = await libs.postHTML({
                    url: hostInfo.url,
                    data: data,
                    headers: this.HEADER,
                    type: 'afn',
                    method: 'post'
                });

                var quality = libs.getQuality(title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };

                var match = /vidSrc: '(.*)'/ig.exec(html);
                if (match != null && match.length > 0) {
                    var src = match[1];
                    libs.postMsg(src, 'mp4', libs.getResolution(src), quality, 4, false, false, {
                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                        'Referer': hostInfo.url,
                        'Origin': 'https://' + libs.extractHostname(hostInfo.url)
                    });
                }
            }
        };
        $.fn.botStream.hosts.push(vshare_eu);

        //https://onlystream.tv/8vmr4tm2h2y5
        var onlystream_tv = {
            DOMAIN: 'onlystream.tv',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                'upgrade-insecure-requests': '1'
            },
            getStream: async function (hostInfo, libs) {
                var html = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'afn'
                });
                var title = '';
                $(html).each(await function (index, item) {
                    if ($(item).prop("tagName") == 'TITLE') {
                        title = $(item).text();
                        return false;
                    }
                });
                var quality = libs.getQuality(title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };
                html.split('updateSrc(').some(function (item) {
                    var pattern = /(.*)\);/ig;
                    var match = pattern.exec(item);
                    try {
                        if (match && match.length > 1) {
                            var streams = eval(match[1]);
                            streams.some(function (obj) {
                                if (obj.src.indexOf('commondatastorage.googleapis.com') > 0) {
                                    return false;
                                }
                                libs.postMsg(obj.src, 'mp4', obj.res, quality, 3, true, false, {
                                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                                    'Referer': hostInfo.url,
                                    'Origin': 'https://' + libs.extractHostname(hostInfo.url)
                                });
                            })
                        };
                    } catch (error) {

                    };
                });
            }
        };
        $.fn.botStream.hosts.push(onlystream_tv);

        //https://vidcloud.pro/embed4/5hgz01viet0ww?i=fd35df66c162ba4377f1fa9670d2b924da9aa21cd8f36fea8f5187a57b737e84
        var vidcloud_pro = {
            DOMAIN: 'vidcloud.pro',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                'upgrade-insecure-requests': '1'
            },
            getStream: async function (hostInfo, libs) {
                var html = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'afn'
                });
                var title = '';
                $(html).each(await function (index, item) {
                    if ($(item).prop("tagName") == 'TITLE') {
                        title = $(item).text();
                        return false;
                    }
                });
                var quality = libs.getQuality(title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };
                html.split('sources = ').some(function (item) {
                    var pattern = /(.*)],/ig;
                    var match = pattern.exec(item);
                    try {
                        if (match && match.length > 1) {
                            var streams = eval(match[1] + ']');
                            streams.some(function (obj) {
                                var type = 'mp4';
                                if (obj.file.indexOf('.m3u8') > 0) {
                                    type = 'm3u8';
                                };
                                libs.postMsg(obj.file, type, libs.getResolution(obj.file), quality, 3, true, false, {
                                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                                    'Referer': hostInfo.url
                                });
                            })
                        };
                    } catch (error) {

                    };
                });
            }
        };
        $.fn.botStream.hosts.push(vidcloud_pro);

        //https://vidcloud.msk.ru/embed4/dvnmtsdz2a1rq?i=b9d7701fae2cb7097aa1dd4f8d0c60eb&amp;el=2524765
        var vidcloud_msk_ru = {
            DOMAIN: 'vidcloud.msk.ru',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                'upgrade-insecure-requests': '1'
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('vidcloud.msk.ru', 'vidcloud.pro')
                });
            }
        };
        $.fn.botStream.hosts.push(vidcloud_msk_ru);

        //https://vidcloud.ru/v/5da1bd52e8c98/the.walking.dead.s10e02.web.h264-tbs.mp4
        var vidcloud_ru = {
            DOMAIN: 'vidcloud.ru',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var html = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'web',
                    script: 'var getVideoTag = ' + this.getVideoTag + '; var postMsg = ' + this.postMsg + '; var botScript = ' + this.check_ddos + '; botScript();'
                });

                eval(html).some(function (obj) {
                    var type = 'mp4';
                    if (obj.file.indexOf('.m3u8') > 0) {
                        type = 'm3u8';
                    };
                    var quality = libs.getQuality(obj.title);
                    if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                        quality = libs.getQuality(hostInfo.quality);
                    };
                    libs.postMsg(obj.file, type, libs.getResolution(obj.file), quality, 3, true, true, {
                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                        'Referer': hostInfo.url,
                        'Origin': 'https://' + libs.extractHostname(hostInfo.url)
                    });
                });
            },
            postMsg: function (json) {

                if (typeof jsinterface != 'undefined') {
                    jsinterface.getSomeString(JSON.stringify(json));
                } else if (typeof window != 'undefined' && typeof window.webkit != 'undefined' && window.webkit && window.webkit.messageHandlers) {
                    window.webkit.messageHandlers.callbackHandler.postMessage(JSON.stringify(json));
                } else {
                    window.flutter_inappwebview.callHandler("jsinterface", json);
                }
            },
            check_ddos: function () {
                if (window.location === window.parent.location) {
                    setTimeout(function () {
                        postMsg({});
                    }, 10000);
                    var timer = setInterval(function () {
                        if (document.querySelector('title').innerText !== 'Just a moment...' &&
                            typeof loadPlayer == 'function') {
                            clearInterval(timer);
                            loadPlayer();
                            getVideoTag();
                        }
                    }, 100);
                };
            },
            getVideoTag: function () {
                var isjwplayer = true;
                var botstreamplayer = null;
                var title = document.querySelector('title').innerText;
                var timerS = setInterval(function () {
                    if (isjwplayer == true && typeof playerInstance !== 'undefined' && playerInstance.getPlaylist !== undefined && playerInstance.getPlaylist().length > 0 && playerInstance.getPlaylist()[0].sources.length > 0) {
                        playerInstance.play();
                        botstreamplayer = playerInstance;
                        isjwplayer = false;
                    } else if (isjwplayer == true && typeof jwplayer !== 'undefined' && typeof jwplayer().getPlaylist() !== 'undefined' && jwplayer().getPlaylist().length > 0 && jwplayer().getPlaylist()[0].sources.length > 0) {
                        jwplayer().play();
                        botstreamplayer = jwplayer();
                        isjwplayer = false;
                    };

                    if (botstreamplayer != null && typeof botstreamplayer.getPlaylist()[0].allSources !== 'undefined') {
                        var sources = botstreamplayer.getPlaylist()[0].allSources;
                        var results = [];
                        for (var i = 0; i < sources.length; i++) {
                            if (typeof sources[i].file !== 'undefined') {
                                var label = '';
                                if (typeof sources[i].label !== 'undefined') {
                                    label = sources[i].label.replace('p', '');
                                    var parsed = parseInt(label);
                                    label = isNaN(parsed) ? 0 : parsed;
                                };
                                results.push({
                                    file: sources[i].file,
                                    label: label,
                                    title: title
                                });
                            }
                        }
                        postMsg(results);
                        clearInterval(timerS);
                        return;
                    };

                    var video = document.getElementsByTagName('video');
                    if (video && video.length > 0) {
                        var srcs = [];
                        var sources = document.querySelectorAll('video > source');
                        if (sources.length > 0) {
                            for (var i = 0; i < sources.length; i++) {
                                var src = sources[i].getAttribute('src');
                                var label = sources[i].getAttribute('label');
                                if (label != null) {
                                    label = label.replace('p', '');
                                    var parsed = parseInt(label);
                                    label = isNaN(parsed) ? 0 : parsed;
                                } else {
                                    label = 0;
                                };
                                if (src != null) {
                                    srcs.push({
                                        file: src,
                                        label: label,
                                        title: title
                                    });
                                }
                            }
                        } else if (video[0].hasAttribute('src') == true) {
                            srcs.push({
                                file: video[0].getAttribute('src'),
                                label: 0
                            });
                        };
                        var results = [];
                        for (var i = 0; i < srcs.length; i++) {
                            var item = srcs[i];
                            if (item.file.indexOf('blob:') < 0) {
                                results.push({
                                    file: item.file,
                                    label: item.label,
                                    title: title
                                });
                            }
                        };
                        clearInterval(timerS);
                        postMsg(results);
                    }
                }, 100)
            }
        };
        $.fn.botStream.hosts.push(vidcloud_ru);

        var vidcloud_co = {
            DOMAIN: 'vidcloud.co',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('vidcloud.co', 'vidcloud.ru')
                });
            }
        };
        $.fn.botStream.hosts.push(vidcloud_co);

        //https://loadvid.online/embed/5ae8933d52275
        var loadvid_online = {
            DOMAIN: 'loadvid.online',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('loadvid.online', 'vidcloud.co')
                });
            }
        };
        $.fn.botStream.hosts.push(loadvid_online);

        //https://vcstream.to/embed/5dfdcb15224f1/
        var vcstream_to = {
            DOMAIN: 'vcstream.to',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('vcstream.to', 'vidcloud.ru')
                });
            }
        };
        $.fn.botStream.hosts.push(vcstream_to);

        //https://clipwatching.com/n1d30cohrp13/the.walking.dead.s10e02.web.h264-tbs.mkv.html
        var clipwatching_com = {
            DOMAIN: 'clipwatching.com',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var html = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'afn',
                });
                var title = '';
                $(html).each(await function (index, item) {
                    if ($(item).prop("tagName") == 'TITLE') {
                        title = $(item).text();
                        return false;
                    }
                });
                var quality = libs.getQuality(title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };
                html.split('sources:').some(function (item) {
                    var pattern = /(.*),/ig;
                    var match = pattern.exec(item);
                    try {
                        if (match && match.length > 1) {
                            var streams = eval(match[1]);
                            streams.some(function (obj) {
                                var type = 'mp4';
                                if (obj.file.indexOf('.m3u8') > 0) {
                                    type = 'm3u8';
                                };
                                var resolution = libs.getResolution(obj.file);
                                if (obj.hasOwnProperty('label')) {
                                    resolution = obj.label.replace('p', '');
                                };
                                libs.postMsg(obj.file, type, resolution, quality, 5, true, true, {
                                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                                    'Referer': hostInfo.url,
                                    'Origin': 'https://' + libs.extractHostname(hostInfo.url)
                                });
                            })
                        };
                    } catch (error) {

                    };
                });
            }
        };
        $.fn.botStream.hosts.push(clipwatching_com);

        //https://streamwire.net/e/buy8rrh4psu4
        var streamwire_net = {
            DOMAIN: 'streamwire.net',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var html = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'afn',
                });
                var title = '';
                $(html).each(await function (index, item) {
                    if ($(item).prop("tagName") == 'TITLE') {
                        title = $(item).text();
                        return false;
                    }
                });
                var quality = libs.getQuality(title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };

                var pattern = />eval(.*)/ig;
                var match = pattern.exec(html);
                try {
                    if (match && match.length > 1) {
                        eval(match[1]).split('sources:').some(function (item) {
                            var pattern = /(.*),preload/ig;
                            var match = pattern.exec(item);
                            try {
                                if (match && match.length > 1) {
                                    var streams = eval(match[1]);
                                    streams.some(function (obj) {
                                        var type = 'mp4';
                                        if (obj.src.indexOf('.m3u8') > 0) {
                                            type = 'm3u8';
                                        };
                                        libs.postMsg(obj.src, type, obj.res, quality, 5, true, false, {
                                            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                                            'Referer': hostInfo.url,
                                            'Origin': 'https://' + libs.extractHostname(hostInfo.url)
                                        });
                                    })
                                };
                            } catch (error) {

                            };
                        });
                    };
                } catch (error) {

                };
            }
        };
        $.fn.botStream.hosts.push(streamwire_net);

        //https://cloudvideo.tv/embed-65qem6e0gd0h.html
        var cloudvideo_tv = {
            DOMAIN: 'cloudvideo.tv',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var url = hostInfo.url;
                if (url.indexOf('embed-') < 0) {
                    url = hostInfo.url.replace('.tv/', '.tv/embed-') + '.html';
                };
                var html = await libs.postHTML({
                    url: url,
                    headers: this.HEADER,
                    type: 'afn',
                });

                var title = '';
                $(html).each(await function (index, item) {
                    if ($(item).prop("tagName") == 'TITLE') {
                        title = $(item).text();
                        return false;
                    }
                });
                var quality = libs.getQuality(title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };

                $(html).find('video > source').each(function (index, item) {
                    var type = 'mp4';
                    var src = $(item).attr('src');
                    if (src.indexOf('.m3u8') > 0) {
                        type = 'm3u8';
                    };
                    libs.postMsg(src, type, libs.getResolution(src), quality, 4, true, false, {
                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                        'Referer': hostInfo.url,
                        'Origin': 'https://' + libs.extractHostname(hostInfo.url)
                    });
                })
            }
        };
        $.fn.botStream.hosts.push(cloudvideo_tv);

        //https://prostream.to/embed-s5xrb8qwamba.html
        var prostream_to = {
            DOMAIN: 'prostream.to',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var url = hostInfo.url;
                if (url.indexOf('embed-') < 0) {
                    url = hostInfo.url.replace('.to/', '.to/embed-');
                };
                var html = await libs.postHTML({
                    url: url,
                    headers: this.HEADER,
                    type: 'afn',
                });

                var title = '';
                $(html).each(await function (index, item) {
                    if ($(item).prop("tagName") == 'TITLE') {
                        title = $(item).text();
                        return false;
                    }
                });

                var quality = libs.getQuality(title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };

                var pattern = />eval(.*)/ig;
                var match = pattern.exec(html);
                try {
                    if (match && match.length > 1) {
                        eval(match[1]).split('sources:').some(function (item) {
                            var ss = item.split('],');
                            if (ss.length == 0) {
                                return false;
                            };

                            try {
                                var streams = eval(ss[0] + ']');
                                streams.some(function (file) {
                                    var type = 'mp4';
                                    if (file.indexOf('.m3u8') > 0) {
                                        type = 'm3u8';
                                    };
                                    libs.postMsg(file, type, 720, quality, 4, true, false, {
                                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                                        'Referer': hostInfo.url,
                                        'Origin': 'https://' + libs.extractHostname(hostInfo.url)
                                    });
                                })
                            } catch (error) {

                            };
                        });
                    };
                } catch (error) {

                };
            }
        };
        $.fn.botStream.hosts.push(prostream_to);

        //https://vtube.to/7uez1bkzw1e4.html
        var vtube_to = {
            DOMAIN: 'vtube.to',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var url = hostInfo.url;
                if (url.indexOf('embed-') < 0) {
                    url = hostInfo.url.replace('.to/', '.to/embed-');
                };
                var html = await libs.postHTML({
                    url: url,
                    headers: this.HEADER,
                    type: 'afn',
                });

                var title = '';
                $(html).each(await function (index, item) {
                    if ($(item).prop("tagName") == 'TITLE') {
                        title = $(item).text();
                        return false;
                    }
                });

                var quality = libs.getQuality(title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };

                var pattern = />eval(.*)/ig;
                var match = pattern.exec(html);
                try {
                    if (match && match.length > 1) {
                        eval(match[1]).split('sources:').some(function (item) {
                            var ss = item.split('],');
                            if (ss.length == 0) {
                                return false;
                            };

                            try {
                                var streams = eval(ss[0] + ']');
                                streams.some(function (file) {
                                    var type = 'mp4';
                                    if (file.indexOf('.m3u8') > 0) {
                                        type = 'm3u8';
                                    };
                                    libs.postMsg(file, type, 720, quality, 4, true, false, {
                                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                                        'Referer': hostInfo.url,
                                        'Origin': 'https://' + libs.extractHostname(hostInfo.url)
                                    });
                                })
                            } catch (error) {

                            };
                        });
                    };
                } catch (error) {

                };
            }
        };
        $.fn.botStream.hosts.push(vtube_to);

        //https://videobin.co/q6irh0uhad0l
        //https://videobin.co/4p2sk0wgqtcz
        var videobin_co = {
            DOMAIN: 'videobin.co',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var html = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'afn',
                });
                var title = $(html).find('h1').text();
                var quality = libs.getQuality(title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };
                html.split('sources:').some(function (item) {
                    var pattern = /(.*),/ig;
                    var match = pattern.exec(item);
                    try {
                        if (match && match.length > 1) {
                            var streams = eval(match[1]);
                            streams.some(function (file) {
                                var type = 'mp4';
                                if (file.indexOf('.m3u8') > 0) {
                                    type = 'm3u8';
                                };
                                libs.postMsg(file, type, libs.getResolution(file), quality, 4, true, false, {
                                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                                    'Referer': hostInfo.url,
                                    'Origin': 'https://' + libs.extractHostname(hostInfo.url)
                                });
                            })
                        };
                    } catch (error) {

                    };
                });
            }
        };
        $.fn.botStream.hosts.push(videobin_co);

        //https://gounlimited.to/cov5s4v148i1/Im.A.Celebrity.Get.Me.Outta.Here.AU.S06E05-EMULE.mp4
        //https://gounlimited.to/4ffwqbldubko/Im.A.Celebrity.Get.Me.Outta.Here.AU.S06E05-EMULE.mp4
        var gounlimited_to = {
            DOMAIN: 'gounlimited.to',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var html = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'afn',
                });
                var title = '';
                $(html).each(await function (index, item) {
                    if ($(item).prop("tagName") == 'TITLE') {
                        title = $(item).text();
                        return false;
                    }
                });
                var quality = libs.getQuality(title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };

                var pattern = />eval(.*)/ig;
                var match = pattern.exec(html);
                try {
                    if (match && match.length > 1) {
                        eval(match[1]).split('https').some(function (item) {
                            var pattern = /(?:https.*\.mp4|\.m3u8)/ig;
                            var match = pattern.exec('https' + item);
                            try {
                                if (match && match.length > 0) {
                                    var file = match[0];
                                    if (file.indexOf('videojs7/small3.mp4') > 0) {
                                        return false;
                                    }
                                    var resolution = $(html).find('span:contains(Resolution)').text();
                                    if (resolution != null) {
                                        var arr = resolution.replace('Resolution: ', '').trim().split('x');
                                        if (arr.length == 2) {
                                            resolution = arr[0];
                                        }
                                    } else {
                                        resolution = libs.getResolution(file)
                                    };
                                    var type = 'mp4';
                                    if (file.indexOf('.m3u8') > 0) {
                                        type = 'm3u8';
                                    };
                                    libs.postMsg(file, type, resolution, quality, 5, true, true, {
                                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                                        'Referer': hostInfo.url,
                                        'Origin': 'https://' + libs.extractHostname(hostInfo.url)
                                    });
                                };
                            } catch (error) {

                            };
                        });
                    };
                } catch (error) {

                };
            }
        };
        $.fn.botStream.hosts.push(gounlimited_to);

        //https://viduplayer.com/embed-zh2iz12sp5db.html
        var viduplayer_com = {
            DOMAIN: 'viduplayer.com',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var that = this;
                var html = '';
                if (hostInfo.hasOwnProperty('html') && hostInfo.html.length > 0) {
                    html = hostInfo.html;
                } else {
                    html = await libs.postHTML({
                        url: hostInfo.url,
                        headers: that.HEADER,
                        type: 'afn'
                    });
                }

                var pattern = /eval\(function(.*)/ig;
                var match = pattern.exec(html);
                try {
                    if (match && match.length > 1) {
                        eval('(function' + match[1]).split('sources:').some(function (item) {
                            var ss = item.split('],');
                            if (ss.length == 0) {
                                return false;
                            };
                            try {
                                var streams = eval(ss[0] + ']');
                                streams.some(function (obj) {
                                    var type = 'mp4';
                                    if (obj.file.indexOf('.m3u8') > 0) {
                                        type = 'm3u8';
                                    } else if (obj.file.indexOf('.mpd') > 0) {
                                        return false;
                                    };
                                    var resolution = libs.getResolution(obj.file);
                                    if (obj.hasOwnProperty('label')) {
                                        resolution = obj.label.replace('p', '');
                                        if (resolution == '640480') {
                                            resolution = '480';
                                        } else if (resolution == '320240') {
                                            resolution = '360';
                                        }
                                    };

                                    libs.postMsg(obj.file, type, resolution, libs.getQuality(hostInfo.quality), 4, true, false, {
                                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                                        'Referer': hostInfo.url
                                    });
                                })
                            } catch (error) {

                            };
                        });

                    };
                } catch (error) {

                };
            }
        };
        $.fn.botStream.hosts.push(viduplayer_com);

        //https://uptostream.com/nef81yi3cjek  ************
        var uptostream_com = {
            DOMAIN: 'uptostream.com',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var that = this;
                var parts = hostInfo.url.split('/');
                var code = parts[parts.length - 1];
                var url = 'https://' + this.DOMAIN + '/api/streaming/source/get?token=null&file_code=' + code;
                this.HEADER.referer = hostInfo.url;

                var jj = await libs.postHTML({
                    url: url,
                    headers: that.HEADER,
                    type: 'afn',
                });

                var json = JSON.parse(jj);
                if (json.statusCode != 0) {
                    return;
                };

                window.eval(json.data.sources);
                window.sources.some(function (obj) {
                    var type = 'm3u8';
                    if (obj.type.indexOf('mp4') > 0) {
                        type = 'mp4';
                    };
                    libs.postMsg(obj.src, type, obj.res, libs.getQuality(hostInfo.quality), 4, true, false, {
                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                        'Referer': hostInfo.url
                    });
                })
            }
        };
        $.fn.botStream.hosts.push(uptostream_com);

        //https://hellabyte.cloud/drive/s/3P29bb9wqzTKSFtgskpJroIqD8ZVsi/view?v=gpcmjw4Vr
        var hellabyte_cloud = {
            DOMAIN: 'hellabyte.cloud',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var html = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'afn'
                });
                var quality = libs.getQuality(hostInfo.quality);
            }
        };
        $.fn.botStream.hosts.push(hellabyte_cloud);

        var uptobox_com = {
            DOMAIN: 'uptobox.com',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('uptobox.', 'uptostream.')
                });
            }
        };
        $.fn.botStream.hosts.push(uptobox_com);

        //http://driveproxy.net/drive/eWtsN1MrVGFmSkFPS3ZCaUZRQUNOZz09.html?hls=1
        var driveproxy_net = {
            DOMAIN: 'driveproxy.net',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var html = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'afn'
                });
                var quality = libs.getQuality(hostInfo.quality);

                var pattern = /http:\/\/cdn.hdplay(.*).(mp4|m3u8)([^"]+){0,1}/ig;
                var match = pattern.exec(html);
                try {
                    if (match && match.length > 1) {
                        var file = match[0];
                        var type = 'mp4';
                        if (file.indexOf('.m3u8') > 0) {
                            type = 'm3u8';
                        }
                        libs.postMsg(file, type, libs.getResolution(file), quality, 4, false, false, {
                            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                            'Referer': hostInfo.url,
                            'Origin': 'https://' + libs.extractHostname(hostInfo.url)
                        });
                    };
                } catch (error) {

                };
            }
        };
        $.fn.botStream.hosts.push(driveproxy_net);

        //https://vidsrc.stream/pro/YTI5MmYxODRkNjY3ZjRmNzQwNGMzNmZkNGI0NmVhODM6VWxsYU5IWnBlVlpGZGt3emRqWmhORGxpVnpSU1RFSjZjV2RJUkRnd1VqUmtMMEpETldaclpHTjJVV1I2YUZCclJqaDViMmQ1VG1Ka04wa3JSVVoyTnpSS1JDdFpZa1F5TUVOVFYwOXJiVkYzYkZwc2RXeHZOWGxJYlZSVk5GcFhTWFJhUTI5WGFuVkdSVkpEWm5GbmJITnVhMkpWVTNNeWVYa3pWblZUWVV0cE0zVXZZbVZXVkhKNk0xaElUWEV2WlZkS2VsVk9Oa2R0V20welVGcFhhMUk1UlZWQ1JVOHlkRTlYUVZkMWFVeG5Wemd4T0VwclRXUTBRa3hCV1UxaE0zQnRXWEozTDA0clNYQlhVbGswV0hZclMwaFNkVEZvVTJ0TlUxZFlRVlpuZUUxMmJubEJOamhaTUVFMWVtdEhaV3hXTkhKTWNUZDFaVzFKYlZSclRYUmxWa3htWldWdllrSnJNVUZEYzNsd05FeERTM3BPU0hsWWVuUnZUMnhaVkRORFlpOWpWRWR3U2psS0szWnZibUZwYUdwT2FqUXJiMWc0V0c0NU9FSlJhbFpaYldZeVIxQldRV3BRYldGT2NEWnhUR3d3YURsWVpqVlVkVGd3VlRWTWIzbGtNakJ6WW5oNlNUQnNlbkZMZFVGNlpXRmpjblpJZURaTlVUZDNPRUZyTW5kd2JsRlNNVzFHTkhCVVJVRm9PSEZ1UVQwOQ--
        var vidsrc_stream = {
            DOMAIN: 'vidsrc.stream',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                this.HEADER.referer = "https://source.vidsrc.me/";
                var html = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'afn'
                });
                var quality = libs.getQuality(hostInfo.quality);

                var pattern = /https(.+)m3u8/ig;
                var match = pattern.exec(html);
                try {
                    if (match && match.length > 1) {
                        var file = match[0];
                        var type = 'mp4';
                        if (file.indexOf('.m3u8') > 0) {
                            type = 'm3u8';
                        }
                        libs.postMsg(file, type, libs.getResolution(file), quality, 4, false, false, {
                            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                            'Referer': hostInfo.url,
                            'Origin': 'https://' + libs.extractHostname(hostInfo.url)
                        });
                    };
                } catch (error) {

                };
            }
        };
        $.fn.botStream.hosts.push(vidsrc_stream);

        // https://vanfem.com/v/n8wqns2n772j8xn
        var vanfem_com = {
            DOMAIN: 'vanfem.com',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var url = hostInfo.url.replace(new RegExp('(.*)#(.*)', 'ig'), '$1').replace('/v/', '/api/source/');
                this.HEADER.referer = hostInfo.url;
                this.HEADER.origin = 'https://' + libs.extractHostname(hostInfo.url);
                this.HEADER['x-requested-with'] = 'XMLHttpRequest';

                var ajax = await libs.postHTML({
                    url: url,
                    headers: this.HEADER,
                    type: 'afn',
                    method: 'post'
                });
                var json = JSON.parse(ajax);
                if (json.success == false) {
                    return;
                };

                json.data.some(function (item) {
                    var type = 'mp4';
                    if (item.file.indexOf('.m3u8') > 0) {
                        type = 'm3u8';
                    };
                    libs.postMsg(item.file, type, item.label.replace('p', ''), libs.getQuality(hostInfo.quality), 4, true, false, {
                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                        'Referer': hostInfo.url
                    });
                });
            }
        };
        $.fn.botStream.hosts.push(vanfem_com);

        //https://vidsrc.xyz/v/n8wqns2n772j8xn
        var vidsrc_xyz = {
            DOMAIN: 'vidsrc.xyz',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('vidsrc.xyz', 'vanfem.com')
                });
            }
        };
        $.fn.botStream.hosts.push(vidsrc_xyz);

        //https://dutrag.com/v/jx4jmad5z8qxy0-
        var dutrag_com = {
            DOMAIN: 'dutrag.com',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('dutrag.com', 'vanfem.com')
                });
            }
        };
        $.fn.botStream.hosts.push(dutrag_com);

        //https://feurl.com/v/-zmqkhp2r3kq3wn
        var feurl_com = {
            DOMAIN: 'feurl.com',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('feurl.com', 'vanfem.com')
                });
            }
        };
        $.fn.botStream.hosts.push(feurl_com);

        //https://www.fembed.com/v/-zmqkhp2r3kq3wn
        var fembed_com = {
            DOMAIN: 'www.fembed.com',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('www.fembed.com', 'vanfem.com')
                });
            }
        };
        $.fn.botStream.hosts.push(fembed_com);

        //https://moviepl.xyz/v/jx4jmad5z8qxy0-
        var moviepl_xyz = {
            DOMAIN: 'moviepl.xyz',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('moviepl.xyz', 'vanfem.com')
                });
            }
        };
        $.fn.botStream.hosts.push(moviepl_xyz);

        //https://there.to/v/ewy24f-ed2rxrkx
        var there_to = {
            DOMAIN: 'there.to',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('there.to', 'vanfem.com')
                });
            }
        };
        $.fn.botStream.hosts.push(there_to);

        //https://fruiterry.com/v/xeq7pb5m77py1wk
        var fruiterry_com = {
            DOMAIN: 'fruiterry',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('fruiterry.com', 'vanfem.com')
                });
            }
        };
        $.fn.botStream.hosts.push(fruiterry_com);

        //https://fcdn.stream/v/36d3ecmm513r82e
        var fcdn_stream = {
            DOMAIN: 'fcdn.stream',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('fcdn.stream', 'vanfem.com')
                });
            }
        };
        $.fn.botStream.hosts.push(fcdn_stream);

        //https://gcloud.live/v/ky5g0h3qw71-e2j
        var gcloud_live = {
            DOMAIN: 'gcloud.live',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('gcloud.live', 'vanfem.com')
                });
            }
        };
        $.fn.botStream.hosts.push(gcloud_live);

        //https://playembed.online/v/7y9weg2yx9j
        var playembed_online = {
            DOMAIN: 'playembed.online',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('playembed.online', 'vanfem.com')
                });
            }
        };
        $.fn.botStream.hosts.push(playembed_online);

        //https://fembed.zmovies.to/getLinkSimple?file=AA2-Scooby-Doo-and-Scrappy-Doo-S01E01%20The%20Scarab%20Lives%21.mp4
        var fembed_zmovies_to = {
            DOMAIN: 'fembed.zmovies.to',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var html = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'afn',
                });

                $(html).each(function (index, item) {
                    if ($(item).prop("tagName") == 'IFRAME') {
                        var src = $(item).attr('src');
                        $.fn.botStream.hostStreams.push({
                            quality: hostInfo.quality,
                            url: src.replace('fembed.zmovies.to', 'dutrag.com')
                        });
                    }
                });

            }
        };
        $.fn.botStream.hosts.push(fembed_zmovies_to);

        //https://video.moviedata.xyz/embed/player.html?id=2b746c6252644753414f383d&image=332f6c4d51752f7352504e687a55326b3849754c663534565271674a6a57335a7551666b71586a6f2b75435071756a3746704c59544f56304871446675616e564a455143504a54776a3739457057454951376b444c79632b47576d77414635617864677032527278
        var video_moviedata_xyz = {
            DOMAIN: 'video.moviedata.xyz',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var url = hostInfo.url.replace('video.moviedata.xyz/', 'app.opencdn.co/').replace('/embed/player.html', '/movie');
                this.HEADER.referer = hostInfo.url;
                this.HEADER.origin = 'https://' + libs.extractHostname(hostInfo.url);

                var ajax = await libs.postHTML({
                    url: url,
                    headers: this.HEADER,
                    type: 'afn'
                });
                var json = JSON.parse(ajax);
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: json.fembed.link
                });
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: json.streamsb.link
                });
            }
        };
        $.fn.botStream.hosts.push(video_moviedata_xyz);

        //https://vidia.tv/pt2fziropm7n
        var vidia_tv = {
            DOMAIN: 'vidia.tv',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var html = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'afn',
                });
                var title = '';
                $(html).each(await function (index, item) {
                    if ($(item).prop("tagName") == 'TITLE') {
                        title = $(item).text();
                        return false;
                    }
                });
                var quality = libs.getQuality(title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };
                var pattern = />eval(.*)/ig;
                var match = pattern.exec(html);
                try {
                    if (match && match.length > 1) {
                        eval(match[1]).split('sources:').some(function (item) {
                            var pp = item.split('],');
                            if (pp.length >= 2) {
                                try {
                                    var streams = eval(pp[0] + ']');
                                    streams.some(function (obj) {
                                        var type = 'mp4';
                                        var cast = true;
                                        if (obj.file.indexOf('.m3u8') > 0) {
                                            type = 'm3u8';
                                        } else if (obj.file.indexOf('.mpd') > 0) {
                                            return false;
                                        };
                                        libs.postMsg(obj.file, type, libs.getResolution(obj.file), quality, 4, cast, false, {
                                            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                                            'Referer': hostInfo.url
                                        });
                                    })
                                } catch (error) {

                                }
                            }
                        });
                    };
                } catch (error) {

                };
            }
        };
        $.fn.botStream.hosts.push(vidia_tv);

        //https://mixdrop.co/e/pu7n83jeve
        var mixdrop_co = {
            DOMAIN: 'mixdrop.co',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                if (hostInfo.hasOwnProperty('referer')) {
                    this.HEADER.referer = hostInfo.referer;
                };
                var url = hostInfo.url.replace('/f/', '/e/');
                var html = await libs.postHTML({
                    url: url,
                    headers: this.HEADER,
                    type: 'afn',
                });

                var title = $(html).find('a').text();
                var quality = libs.getQuality(title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };

                if (html.indexOf('<script>window.location = "') == 0) {
                    this.HEADER.referer = url;
                    var match = /window.location = "(.*?)"/ig.exec(html);
                    url = 'https://' + this.DOMAIN + match[1];
                    html = await libs.postHTML({
                        url: url,
                        headers: this.HEADER,
                        type: 'afn',
                    });
                }
                var pattern = /eval(.*)/ig;
                var match = pattern.exec(html);
                try {
                    if (match && match.length > 1) {
                        eval(match[1]).replace(new RegExp('[ ]', 'ig'), '').split(';').some(function (item) {
                            if (item.indexOf('.mp4') > 0) {
                                var st = item.replace(new RegExp('(.*)\"(.*)\"', 'ig'), '$2').trim();
                                if (st.indexOf('//') < 0) {
                                    return false;
                                }
                                var streamURL = 'https:' + st;
                                var type = 'mp4';
                                if (streamURL.indexOf('.m3u8') > 0) {
                                    type = 'm3u8';
                                };
                                var resolution = libs.getResolution(streamURL);
                                libs.postMsg(streamURL, type, resolution, quality, 4, false, true, {
                                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                                    'Referer': hostInfo.url,
                                    'Origin': 'https://' + libs.extractHostname(hostInfo.url)
                                });
                                return true;
                            }
                        });
                    };
                } catch (error) {

                };
            }
        };
        $.fn.botStream.hosts.push(mixdrop_co);

        //https://upstream.to/dwia40ilvzwk
        var upstream_to = {
            DOMAIN: 'upstream.to',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var html = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'afn'
                });
                var title = '';
                $(html).each(await function (index, item) {
                    if ($(item).prop("tagName") == 'TITLE') {
                        title = $(item).text();
                        return false;
                    }
                });
                var quality = libs.getQuality(title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };

                var pattern = />eval(.*)/ig;
                var match = pattern.exec(html);
                try {
                    if (match && match.length > 1) {
                        eval(match[1]).split('sources:').some(function (item) {
                            var pp = item.split('],');
                            if (pp.length >= 2) {
                                try {
                                    var streams = eval(pp[0] + ']');
                                    streams.some(function (obj) {
                                        var type = 'mp4';
                                        if (obj.file.indexOf('.m3u8') > 0) {
                                            type = 'm3u8';
                                        };
                                        libs.postMsg(obj.file, type, libs.getResolution(obj.file), quality, 4, true, false, {
                                            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                                            'Referer': hostInfo.url,
                                            'Origin': 'https://' + libs.extractHostname(hostInfo.url)
                                        });
                                    })
                                } catch (error) {

                                }
                            }
                        });
                    };
                } catch (error) {

                };
            }
        };
        $.fn.botStream.hosts.push(upstream_to);

        //https://mystream.to/watch/puhlaymeqzn9
        var mystream_to = {
            DOMAIN: 'mystream.to',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var url = hostInfo.url;
                if (url.indexOf('https://mystream.to/watch') >= 0) {
                    url = url.replace('https://mystream.to/watch/', 'https://embed.mystream.to/');
                }
                var html = await libs.postHTML({
                    url: url,
                    headers: this.HEADER,
                    type: 'web',
                    script: 'var postMsg = ' + this.postMsg + '; var botScript = ' + this.getHTML + '; botScript();'
                });
                var title = '';
                $(html).each(await function (index, item) {
                    if ($(item).prop("tagName") == 'TITLE') {
                        title = $(item).text();
                        return false;
                    }
                });
                var quality = libs.getQuality(title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };

                var file = $(html).find('video > source').attr('src');
                var type = 'mp4';
                if (file.indexOf('.m3u8') > 0) {
                    type = 'm3u8';
                };
                libs.postMsg(file, type, libs.getResolution(file), quality, 4, true, false, {
                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                    'Referer': hostInfo.url,
                    'Origin': 'https://' + libs.extractHostname(hostInfo.url)
                });
            },
            postMsg: function (html) {

                if (typeof jsinterface != 'undefined') {
                    jsinterface.getSomeString(html);
                } else if (typeof window != 'undefined' && typeof window.webkit != 'undefined' && window.webkit && window.webkit.messageHandlers) {
                    window.webkit.messageHandlers.callbackHandler.postMessage(html);
                } else {
                    window.flutter_inappwebview.callHandler("jsinterface", html);
                }
            },
            getHTML: function () {
                if (window.location === window.parent.location) {
                    setTimeout(function () {
                        postMsg('');
                    }, 10000);
                    var timer = setInterval(function () {
                        var video = document.querySelector('video');
                        if (video != null) {
                            clearInterval(timer);
                            var html = document.querySelector('html').innerHTML;
                            postMsg(html);
                        }
                    }, 100);
                }
            }
        };
        $.fn.botStream.hosts.push(mystream_to);

        //https://mstream.xyz/a0qej155xztl
        var mstream_xyz = {
            DOMAIN: 'mstream.xyz',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('https://mstream.xyz/', 'https://mystream.to/watch/')
                });
            }
        };
        $.fn.botStream.hosts.push(mstream_xyz);

        //https://mystream.streamango.to/e/a0qej155xztl
        var mystream_streamango_to = {
            DOMAIN: 'mystream.streamango.to',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('https://mystream.streamango.to/', 'https://mystream.to/watch/')
                });
            }
        };
        $.fn.botStream.hosts.push(mystream_streamango_to);

        //https://playto-vid.com/yuywyi1patf0
        var playto_vid_com = {
            DOMAIN: 'playto-vid.com',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var html = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'afn',
                });
                var title = '';
                $(html).each(await function (index, item) {
                    if ($(item).prop("tagName") == 'TITLE') {
                        title = $(item).text();
                        return false;
                    }
                });
                var quality = libs.getQuality(title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };

                var pattern = />eval(.*)/ig;
                var match = pattern.exec(html);
                try {
                    if (match && match.length > 1) {
                        eval(match[1]).split('sources:').some(function (item) {
                            var pattern = /(.*),preload/ig;
                            var match = pattern.exec(item);
                            try {
                                if (match && match.length > 1) {
                                    var streams = eval(match[1]);
                                    streams.some(function (obj) {
                                        var type = 'mp4';
                                        if (obj.src.indexOf('.m3u8') > 0) {
                                            type = 'm3u8';
                                        };
                                        libs.postMsg(obj.src, type, obj.res, quality, 5, true, false, {
                                            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                                            'Referer': hostInfo.url,
                                            'Origin': 'https://' + libs.extractHostname(hostInfo.url)
                                        });
                                    })
                                };
                            } catch (error) {

                            };
                        });
                    };
                } catch (error) {

                };
            }
        };
        $.fn.botStream.hosts.push(playto_vid_com);

        //https://www.vidbull.tv/embed/7251
        var vidbull_tv = {
            DOMAIN: 'www.vidbull.tv',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var html = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'web',
                    script: 'var getVideoTag = ' + this.getVideoTag + '; var postMsg = ' + this.postMsg + '; var botScript = ' + this.check_ddos + '; botScript();'
                });

                eval(html).some(function (obj) {
                    var type = 'mp4';
                    if (obj.file.indexOf('.m3u8') > 0) {
                        type = 'm3u8';
                    };
                    var quality = libs.getQuality(obj.title);
                    if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                        quality = libs.getQuality(hostInfo.quality);
                    };
                    libs.postMsg(obj.file, type, libs.getResolution(obj.file), quality, 3, true, false, {
                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                        'Referer': hostInfo.url,
                        'Origin': 'https://' + libs.extractHostname(hostInfo.url),
                        'cookie': obj.cookies
                    });
                });
            },
            postMsg: function (json) {

                if (typeof jsinterface != 'undefined') {
                    jsinterface.getSomeString(JSON.stringify(json));
                } else if (typeof window != 'undefined' && typeof window.webkit != 'undefined' && window.webkit && window.webkit.messageHandlers) {
                    window.webkit.messageHandlers.callbackHandler.postMessage(JSON.stringify(json));
                } else {
                    window.flutter_inappwebview.callHandler("jsinterface", json);
                }
            },
            check_ddos: function () {
                if (window.location === window.parent.location) {
                    setTimeout(function () {
                        postMsg({});
                    }, 10000);
                    var timer = setInterval(function () {
                        if (document.querySelector('title').innerText !== 'Just a moment...') {
                            clearInterval(timer);
                            getVideoTag();
                        }
                    }, 100);
                };
            },
            getVideoTag: function () {
                var isjwplayer = true;
                var botstreamplayer = null;
                var title = document.querySelector('title').innerText;
                var timerS = setInterval(function () {
                    if (isjwplayer == true && typeof playerInstance !== 'undefined' && playerInstance.getPlaylist !== undefined && playerInstance.getPlaylist().length > 0 && playerInstance.getPlaylist()[0].sources.length > 0) {
                        playerInstance.play();
                        botstreamplayer = playerInstance;
                        isjwplayer = false;
                    } else if (isjwplayer == true && typeof jwplayer !== 'undefined' && typeof jwplayer().getPlaylist() !== 'undefined' && jwplayer().getPlaylist().length > 0 && jwplayer().getPlaylist()[0].sources.length > 0) {
                        jwplayer().play();
                        botstreamplayer = jwplayer();
                        isjwplayer = false;
                    };

                    if (botstreamplayer != null && typeof botstreamplayer.getPlaylist()[0].allSources !== 'undefined') {
                        var sources = botstreamplayer.getPlaylist()[0].allSources;
                        var results = [];
                        for (var i = 0; i < sources.length; i++) {
                            if (typeof sources[i].file !== 'undefined') {
                                var label = '';
                                if (typeof sources[i].label !== 'undefined') {
                                    label = sources[i].label.replace('p', '');
                                    var parsed = parseInt(label);
                                    label = isNaN(parsed) ? 0 : parsed;
                                };
                                results.push({
                                    file: sources[i].file,
                                    label: label,
                                    title: title,
                                    cookies: document.cookie
                                });
                            }
                        }
                        postMsg(results);
                        clearInterval(timerS);
                        return;
                    };

                    var video = document.getElementsByTagName('video');
                    if (video && video.length > 0) {
                        var srcs = [];
                        var sources = document.querySelectorAll('video > source');
                        if (sources.length > 0) {
                            for (var i = 0; i < sources.length; i++) {
                                var src = sources[i].getAttribute('src');
                                var label = sources[i].getAttribute('label');
                                if (label != null) {
                                    label = label.replace('p', '');
                                    var parsed = parseInt(label);
                                    label = isNaN(parsed) ? 0 : parsed;
                                } else {
                                    label = 0;
                                };
                                if (src != null) {
                                    srcs.push({
                                        file: src,
                                        label: label,
                                        title: title,
                                        cookies: document.cookie
                                    });
                                }
                            }
                        } else if (video[0].hasAttribute('src') == true) {
                            srcs.push({
                                file: video[0].getAttribute('src'),
                                label: 0
                            });
                        };
                        var results = [];
                        for (var i = 0; i < srcs.length; i++) {
                            var item = srcs[i];
                            if (item.file.indexOf('blob:') < 0) {
                                results.push({
                                    file: item.file,
                                    label: item.label,
                                    title: title,
                                    cookies: document.cookie
                                });
                            }
                        };
                        clearInterval(timerS);
                        postMsg(results);
                    }
                }, 100)
            }
        };
        $.fn.botStream.hosts.push(vidbull_tv);

        //https://www.mp4upload.com/embed-7rbmiqae1gzs.html
        var www_mp4upload_com = {
            DOMAIN: 'www.mp4upload.com',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('www.mp4upload.com', 'mp4upload.com')
                });
            }
        };
        $.fn.botStream.hosts.push(www_mp4upload_com);

        //https://mp4upload.com/embed-teilgiuoziaa.html
        var mp4upload_com = {
            DOMAIN: 'mp4upload.com',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var html = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'afn',
                });

                var quality = libs.getQuality('');
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };
                var pattern = /eval(.*)/ig;
                var match = pattern.exec(html);
                try {
                    if (match && match.length > 1) {
                        eval(match[1]).split('sources:').some(function (item) {
                            var pp = item.split('],');
                            if (pp.length >= 2) {
                                try {
                                    var streams = eval(pp[0] + ']');
                                    streams.some(function (obj) {
                                        var type = 'mp4';
                                        var cast = true;
                                        if (obj.src.indexOf('.m3u8') > 0) {
                                            type = 'm3u8';
                                        };
                                        libs.postMsg(obj.src, type, libs.getResolution(obj.src), quality, 4, cast, true, {
                                            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                                            'Referer': hostInfo.url
                                        });
                                    })
                                } catch (error) {

                                }
                            }
                        });
                    };
                } catch (error) {

                };
            }
        };
        $.fn.botStream.hosts.push(mp4upload_com);

        //https://eplayvid.com/watch/4ff6f3f0a61a7cb
        var eplayvid_com = {
            DOMAIN: 'eplayvid.com',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.192 Safari/537.36 OPR/74.0.3911.218',
            },
            getStream: async function (hostInfo, libs) {
                var html = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'afn'
                });
                var title = '';
                $(html).each(await function (index, item) {
                    if ($(item).prop("tagName") == 'TITLE') {
                        title = $(item).text();
                        return false;
                    }
                });

                var quality = libs.getQuality(title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };

                var file = $(html).find('video > source').attr('src');
                var type = 'mp4';
                if (file.indexOf('.m3u8') > 0) {
                    type = 'm3u8';
                };
                libs.postMsg(file, type, libs.getResolution(file), quality, 4, true, false, {
                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                    'Referer': hostInfo.url,
                });
            }
        };
        $.fn.botStream.hosts.push(eplayvid_com);

        // //https://video.opencdn.co/embed/player.html?id=2b75…0564c7434482b4364344f48434c6838454974767a6b72453d
        // var video_opencdn_co = {
        //     DOMAIN: 'video.opencdn.co',
        //     HEADER: {
        //         'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
        //     },
        //     getStream : async function(hostInfo, libs){
        //         var url = hostInfo.url.replace('/embed/player.html','/api/');
        //         this.HEADER.referer = hostInfo.url;
        //         this.HEADER.origin = 'https://' + libs.extractHostname(hostInfo.url);

        //         var ajax = await libs.postHTML({
        //             url: url,
        //             headers: this.HEADER,
        //             type: 'afn'
        //         });
        //         var json = JSON.parse(ajax);
        //         json.some(function(item){
        //             $.fn.botStream.hostStreams.push({
        //                 quality: hostInfo.quality,
        //                 url: item.link
        //             });
        //         });
        //     }
        // };
        // $.fn.botStream.hosts.push(video_opencdn_co);

        //https://streamapi.xyz/connection?link1=jm71N0NRN0NtNDNcjC7~NFjXNI7F7YNI7R7w74NwNANiNCN6N679jj7271NgNjNcNX7v7l7RNjNb75NBN67-Nc7lNVNxNLNhNwNRNPN9NKNSNvNANa7Q7yNS7l76N07BNf764a8742466cf0003e439e5ecb1737bbbfcc527fcc0813bbf27541f7319df2febcjbNyj-Ndjv7lNXNjj8N4Ndj6jsN-7fjGNb7tNvjDNBNANY7kj6N5N87fNeNxNeNe7_7W7y7zNj7t&link2=
        var streamapi_xyz = {
            DOMAIN: 'streamapi.xyz',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                hostInfo.headers['user-agent'] = this.HEADER['user-agent'];
                var html = await libs.postHTML({
                    url: hostInfo.url,
                    headers: hostInfo.headers,
                    type: 'afn',
                });

                var quality = libs.getQuality('');
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = hostInfo.quality;
                };
                var pattern = /eval(.*)/ig;
                var match = pattern.exec(html);
                try {
                    if (match && match.length > 1) {
                        var parts = match[1].split('</script');
                        if (parts.length > 0) {
                            match = /var link="(?:[^"]+)"/ig.exec(eval(parts[0]));
                            if (match && match.length > 0) {
                                var streamURL = libs.refillURL(match[0].replace('var link="', '').replace('"', ''));
                                if (streamURL.indexOf('bmoviesfree.io') > 0) {
                                    streamURL = streamURL.replace('bmoviesfree.io', 'feurl.com');
                                };
                                $.fn.botStream.hostStreams.push({
                                    quality: quality,
                                    url: streamURL
                                });
                            }
                        }

                    };
                } catch (error) {

                };
            }
        };
        $.fn.botStream.hosts.push(streamapi_xyz);

        //https://abcvideo.cc/vleth6mx7lui.html
        var abcvideo_cc = {
            DOMAIN: 'abcvideo.cc',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var jj = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'web',
                    script: 'var postMsg = ' + this.postMsg + '; var botScript = ' + this.getVideoTag + '; botScript();'
                });

                var json = JSON.parse(jj);
                var quality = libs.getQuality('');
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };

                json.some(function (item) {
                    var file = item.file;
                    var type = 'mp4';
                    if (file.indexOf('.m3u8') > 0) {
                        type = 'm3u8';
                    };
                    libs.postMsg(file, type, libs.getResolution(file), libs.getQuality(item.label), 4, true, false, {
                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                        'Referer': hostInfo.url,
                    });
                });
            },
            postMsg: function (json) {

                if (typeof jsinterface != 'undefined') {
                    jsinterface.getSomeString(JSON.stringify(json));
                } else if (typeof window != 'undefined' && typeof window.webkit != 'undefined' && window.webkit && window.webkit.messageHandlers) {
                    window.webkit.messageHandlers.callbackHandler.postMessage(JSON.stringify(json));
                } else {
                    window.flutter_inappwebview.callHandler("jsinterface", json);
                }
            },
            getVideoTag: function () {
                if (window.location === window.parent.location) {
                    setTimeout(function () {
                        postMsg({});
                    }, 10000);
                    var botstreamplayer = null;
                    var timerS = setInterval(function () {
                        if (typeof jwplayer !== 'undefined' && typeof jwplayer().getPlaylist() !== 'undefined' && jwplayer().getPlaylist().length > 0 && jwplayer().getPlaylist()[0].sources.length > 0) {
                            botstreamplayer = jwplayer();
                            var sources = botstreamplayer.getPlaylist()[0].sources;
                            var results = [];
                            for (var i = 0; i < sources.length; i++) {
                                if (typeof sources[i].file !== 'undefined') {
                                    results.push({
                                        file: sources[i].file,
                                        label: sources[i].label
                                    });
                                }
                            }
                            postMsg(results);
                            clearInterval(timerS);
                        };
                    }, 100)
                }
            }
        };
        $.fn.botStream.hosts.push(abcvideo_cc);

        //https://sltube.org/e/NBQe7lBq14rz59bO
        var sltube_org = {
            DOMAIN: 'sltube.org',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var jj = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'web',
                    script: 'var postMsg = ' + this.postMsg + '; var botScript = ' + this.getVideoTag + '; botScript();'
                });

                var json = JSON.parse(jj);
                var quality = libs.getQuality('');
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };

                json.some(function (item) {
                    var file = item.file;
                    var type = 'mp4';
                    if (file.indexOf('.m3u8') > 0) {
                        type = 'm3u8';
                    };
                    libs.postMsg(file, type, libs.getResolution(file), libs.getQuality(item.label), 4, false, false, {
                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                        'Referer': hostInfo.url,
                    });
                });
            },
            postMsg: function (json) {

                if (typeof jsinterface != 'undefined') {
                    jsinterface.getSomeString(JSON.stringify(json));
                } else if (typeof window != 'undefined' && typeof window.webkit != 'undefined' && window.webkit && window.webkit.messageHandlers) {
                    window.webkit.messageHandlers.callbackHandler.postMessage(JSON.stringify(json));
                } else {
                    window.flutter_inappwebview.callHandler("jsinterface", json);
                }
            },
            getVideoTag: function () {
                if (window.location === window.parent.location) {
                    setTimeout(function () {
                        postMsg({});
                    }, 10000);
                    var botstreamplayer = null;
                    var timerS = setInterval(function () {
                        if (typeof jwplayer !== 'undefined' && typeof jwplayer().getPlaylist() !== 'undefined' && jwplayer().getPlaylist().length > 0 && jwplayer().getPlaylist()[0].sources.length > 0) {
                            botstreamplayer = jwplayer();
                            var sources = botstreamplayer.getPlaylist()[0].sources;
                            var results = [];
                            for (var i = 0; i < sources.length; i++) {
                                if (typeof sources[i].file !== 'undefined') {
                                    results.push({
                                        file: sources[i].file,
                                        label: sources[i].label
                                    });
                                }
                            }
                            postMsg(results);
                            clearInterval(timerS);
                        };
                    }, 100)
                }
            }
        };
        $.fn.botStream.hosts.push(sltube_org);

        //https://streamlare.com/e/NBQe7lBq14rz59bO
        var streamlare_com = {
            DOMAIN: 'streamlare.com',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('streamlare.com', 'sltube.org')
                });
            }
        };
        $.fn.botStream.hosts.push(streamlare_com);

        //https://mzzcloud.life/embed-4/oQTDjCCBo1Lv?z=
        var mzzcloud_life = {
            DOMAIN: 'mzzcloud.life',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                this.HEADER.referer = hostInfo.referer;
                var jj = await libs.postHTML({
                    url: hostInfo.referer,
                    headers: this.HEADER,
                    type: 'web',
                    script: 'var postMsg = ' + this.postMsg + '; var botScript = ' + this.getVideoTag + '; botScript("' + hostInfo.url + '");'
                });
                var json = JSON.parse(jj);
                var quality = libs.getQuality('');
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };

                json.some(function (item) {
                    var file = item.file;
                    var type = 'mp4';
                    if (file.indexOf('.m3u8') > 0) {
                        type = 'm3u8';
                    };
                    libs.postMsg(file, type, libs.getResolution(file), libs.getQuality(item.label), 4, false, false, {
                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                        'Referer': hostInfo.url,
                    });
                });
            },
            postMsg: function (json) {

                if (typeof jsinterface != 'undefined') {
                    jsinterface.getSomeString(JSON.stringify(json));
                } else if (typeof window != 'undefined' && typeof window.webkit != 'undefined' && window.webkit && window.webkit.messageHandlers) {
                    window.webkit.messageHandlers.callbackHandler.postMessage(JSON.stringify(json));
                } else {
                    window.flutter_inappwebview.callHandler("jsinterface", json);
                }
            },
            getVideoTag: function (url) {
                if (window.location === window.parent.location) {
                    if (url.indexOf(window.location.hostname) >= 0) {
                        setTimeout(function () {
                            postMsg({ html: document.body.innerHTML });
                        }, 10000);
                        var botstreamplayer = null;
                        var timerS = setInterval(function () {
                            if (typeof jwplayer !== 'undefined' && typeof jwplayer().getPlaylist() !== 'undefined' && jwplayer().getPlaylist().length > 0 && jwplayer().getPlaylist()[0].sources.length > 0) {
                                botstreamplayer = jwplayer();
                                var sources = botstreamplayer.getPlaylist()[0].sources;
                                var results = [];
                                for (var i = 0; i < sources.length; i++) {
                                    if (typeof sources[i].file !== 'undefined') {
                                        results.push({
                                            file: sources[i].file,
                                            label: sources[i].label
                                        });
                                    }
                                }
                                postMsg(results);
                                clearInterval(timerS);
                            };
                        }, 100)
                    } else {
                        setTimeout(function () {
                            window.location.href = url
                        }, 2000);
                    }

                }
            }
        };
        $.fn.botStream.hosts.push(mzzcloud_life);

        //https://vidmovie.xyz/e/d6tft6dnqty7
        var vidmovie_xyz = {
            DOMAIN: 'vidmovie.xyz',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var jj = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'web',
                    script: 'var postMsg = ' + this.postMsg + '; var botScript = ' + this.getVideoTag + '; botScript();'
                });

                var json = JSON.parse(jj);
                var quality = libs.getQuality('');
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };

                json.some(function (item) {
                    var file = item.file;
                    var type = 'mp4';
                    if (file.indexOf('.m3u8') > 0) {
                        type = 'm3u8';
                    };
                    libs.postMsg(file, type, libs.getResolution(file), libs.getQuality(item.label), 4, true, false, {
                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                        'Referer': hostInfo.url,
                    });
                });
            },
            postMsg: function (json) {

                if (typeof jsinterface != 'undefined') {
                    jsinterface.getSomeString(JSON.stringify(json));
                } else if (typeof window != 'undefined' && typeof window.webkit != 'undefined' && window.webkit && window.webkit.messageHandlers) {
                    window.webkit.messageHandlers.callbackHandler.postMessage(JSON.stringify(json));
                } else {
                    window.flutter_inappwebview.callHandler("jsinterface", json);
                }
            },
            getVideoTag: function () {
                if (window.location === window.parent.location) {
                    setTimeout(function () {
                        postMsg({});
                    }, 10000);
                    var botstreamplayer = null;
                    var timerS = setInterval(function () {
                        if (typeof jwplayer !== 'undefined' && typeof jwplayer().getPlaylist() !== 'undefined' && jwplayer().getPlaylist().length > 0 && jwplayer().getPlaylist()[0].sources.length > 0) {
                            botstreamplayer = jwplayer();
                            var sources = botstreamplayer.getPlaylist()[0].sources;
                            var results = [];
                            for (var i = 0; i < sources.length; i++) {
                                if (typeof sources[i].file !== 'undefined') {
                                    results.push({
                                        file: sources[i].file,
                                        label: sources[i].label
                                    });
                                }
                            }
                            postMsg(results);
                            clearInterval(timerS);
                        };
                    }, 100)
                }
            }
        };
        $.fn.botStream.hosts.push(vidmovie_xyz);

        //https://tubesb.com/e/tr3h267kmdjc
        var tubesb_com = {
            DOMAIN: 'tubesb.com',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var jj = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'web',
                    script: 'var postMsg = ' + this.postMsg + '; var botScript = ' + this.getVideoTag + '; botScript();'
                });
                var json = JSON.parse(jj);
                var quality = libs.getQuality('');
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };

                json.some(function (item) {
                    var file = item.file;
                    var type = 'mp4';
                    if (file.indexOf('.m3u8') > 0) {
                        type = 'm3u8';
                    };
                    libs.postMsg(file, type, libs.getResolution(file), libs.getQuality(item.label), 4, true, false, {
                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                        'Referer': hostInfo.url,
                    });
                });
            },
            postMsg: function (json) {

                if (typeof jsinterface != 'undefined') {
                    jsinterface.getSomeString(JSON.stringify(json));
                } else if (typeof window != 'undefined' && typeof window.webkit != 'undefined' && window.webkit && window.webkit.messageHandlers) {
                    window.webkit.messageHandlers.callbackHandler.postMessage(JSON.stringify(json));
                } else {
                    window.flutter_inappwebview.callHandler("jsinterface", json);
                }
            },
            getVideoTag: function () {
                if (window.location === window.parent.location) {
                    setTimeout(function () {
                        postMsg({});
                    }, 10000);
                    var botstreamplayer = null;
                    var timerS = setInterval(function () {
                        if (typeof jwplayer !== 'undefined' && typeof jwplayer().getPlaylist() !== 'undefined' && jwplayer().getPlaylist().length > 0 && jwplayer().getPlaylist()[0].sources.length > 0) {
                            botstreamplayer = jwplayer();
                            var sources = botstreamplayer.getPlaylist()[0].sources;
                            var results = [];
                            for (var i = 0; i < sources.length; i++) {
                                if (typeof sources[i].file !== 'undefined') {
                                    results.push({
                                        file: sources[i].file,
                                        label: sources[i].label
                                    });
                                }
                            }
                            postMsg(results);
                            clearInterval(timerS);
                        };
                    }, 100)
                }
            }
        };
        $.fn.botStream.hosts.push(tubesb_com);

        //https://streamsb.net/embed-d7b66ftkcm8e.html
        var streamsb_net = {
            DOMAIN: 'streamsb.net',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('streamsb.net', 'tubesb.com')
                });
            }
        };
        $.fn.botStream.hosts.push(streamsb_net);

        //https://sbembed.com/d/fkoo8l5p6l48
        var sbembed_com = {
            DOMAIN: 'sbembed.com',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('sbembed.com', 'tubesb.com')
                });
            }
        };
        $.fn.botStream.hosts.push(sbembed_com);

        //https://sbplay1.com/e/5wdcjtb8vsi2
        var sbplay1_com = {
            DOMAIN: 'sbplay1.com',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('sbplay1.com', 'tubesb.com')
                });
            }
        };
        $.fn.botStream.hosts.push(sbplay1_com);

        //https://sbplay2.xyz/e/zvlgi5upgww3?caption_1=https://sub.movie-series.net/top-gun-maverick/top-gun-maverick.v4.vtt&sub_1=English
        var sbplay2_xyz = {
            DOMAIN: 'sbplay2.xyz',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('sbplay2.xyz', 'tubesb.com')
                });
            }
        };
        $.fn.botStream.hosts.push(sbplay2_xyz);

        //https://watchsb.com/e/5wdcjtb8vsi2
        var watchsb_com = {
            DOMAIN: 'watchsb.com',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('watchsb.com', 'tubesb.com')
                });
            }
        };
        $.fn.botStream.hosts.push(watchsb_com);

        //https://sbfull.com/erx5z9xflgzsr.html
        var sbfull_com = {
            DOMAIN: 'sbfull.com',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('sbfull.com/', 'tubesb.com/e/')
                });
            }
        };
        $.fn.botStream.hosts.push(sbfull_com);

        //https://videovard.sx/e/zpgiiwf1cwsr
        var videovard_sx = {
            DOMAIN: 'videovard.sx',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var jj = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'web',
                    script: 'var postMsg = ' + this.postMsg + '; var botScript = ' + this.getVideoTag + '; botScript();'
                });
                var json = JSON.parse(jj);
                var quality = libs.getQuality('');
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };

                json.some(function (item) {
                    var file = item.file;
                    var type = 'mp4';
                    if (file.indexOf('.m3u8') > 0) {
                        type = 'm3u8';
                    };
                    libs.postMsg(file, type, libs.getResolution(file), libs.getQuality(item.label), 4, true, false, {
                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                        'Referer': hostInfo.url,
                    });
                });
            },
            postMsg: function (json) {

                if (typeof jsinterface != 'undefined') {
                    jsinterface.getSomeString(JSON.stringify(json));
                } else if (typeof window != 'undefined' && typeof window.webkit != 'undefined' && window.webkit && window.webkit.messageHandlers) {
                    window.webkit.messageHandlers.callbackHandler.postMessage(JSON.stringify(json));
                } else {
                    window.flutter_inappwebview.callHandler("jsinterface", json);
                }
            },
            getVideoTag: function () {
                if (window.location === window.parent.location) {
                    setTimeout(function () {
                        postMsg({});
                    }, 10000);
                    var botstreamplayer = null;
                    var timerS = setInterval(function () {
                        if (typeof jwplayer !== 'undefined' && typeof jwplayer().getPlaylist() !== 'undefined' && jwplayer().getPlaylist().length > 0 && jwplayer().getPlaylist()[0].sources.length > 0) {
                            botstreamplayer = jwplayer();
                            var sources = botstreamplayer.getPlaylist()[0].sources;
                            var results = [];
                            for (var i = 0; i < sources.length; i++) {
                                if (typeof sources[i].file !== 'undefined') {
                                    results.push({
                                        file: sources[i].file,
                                        label: sources[i].label
                                    });
                                }
                            }
                            postMsg(results);
                            clearInterval(timerS);
                        };
                    }, 100)
                }
            }
        };
        $.fn.botStream.hosts.push(videovard_sx);

        //https://vidcloud9.org/watch?v=gAAAAABh9kcXMaHDoVtQN6vbDAPcmr5Q-4RxgD0rvKok9iMHtUySshbnLRr2b-OAdpcO99OPwrqYywT-i4_9jQ4D79vQqTBp_Otf57lu8wtlZE8iOtYBdVGdPBrtrPlui4RwmrkcUpdrhW0KsThyOZ4mbPv-dpc0CA==
        var vidcloud9_org = {
            DOMAIN: 'vidcloud9.org',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36',
                'referer': 'https://ww5.0123movie.net/'
            },
            getStream: async function (hostInfo, libs) {
                var that = this;
                var jj = await libs.postHTML({
                    url: 'https://ww5.0123movie.net/',
                    headers: that.HEADER,
                    type: 'web',
                    script: 'var postMsg = ' + this.postMsg + '; var botScript = ' + this.getVideoTag + '; botScript("' + hostInfo.url + '");'
                });

                var json = JSON.parse(jj);
                var quality = libs.getQuality('');
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };

                json.some(function (item) {
                    var file = 'https://' + that.DOMAIN + item.file;
                    var type = 'mp4';
                    if (file.indexOf('.m3u8') > 0) {
                        type = 'm3u8';
                    };
                    libs.postMsg(file, type, libs.getResolution(file), libs.getQuality(item.label), 4, true, false, {
                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36',
                        'Referer': hostInfo.url,
                    });
                });
            },
            postMsg: function (json) {

                if (typeof jsinterface != 'undefined') {
                    jsinterface.getSomeString(JSON.stringify(json));
                } else if (typeof window != 'undefined' && typeof window.webkit != 'undefined' && window.webkit && window.webkit.messageHandlers) {
                    window.webkit.messageHandlers.callbackHandler.postMessage(JSON.stringify(json));
                } else {
                    window.flutter_inappwebview.callHandler("jsinterface", json);
                }
            },
            getVideoTag: function (url) {
                if (window.location === window.parent.location) {
                    setTimeout(function () {
                        postMsg({});
                    }, 10000);
                    if (window.location === window.parent.location) {
                        setTimeout(function () {
                            postMsg({});
                        }, 10000);
                        if (window.location.href.indexOf('0123movie') > 0) {
                            setTimeout(function () {
                                window.location.href = url;
                            }, 3000);
                        } else {
                            var timerS = setInterval(function () {
                                if (typeof jwplayer !== 'undefined' && typeof jwplayer().getPlaylist() !== 'undefined' && jwplayer().getPlaylist().length > 0 && jwplayer().getPlaylist()[0].sources.length > 0) {
                                    var sources = jwplayer().getPlaylist()[0].sources;
                                    var results = [];
                                    for (var i = 0; i < sources.length; i++) {
                                        if (typeof sources[i].file !== 'undefined') {
                                            results.push({
                                                file: sources[i].file,
                                                label: sources[i].label
                                            });
                                        }
                                    }
                                    postMsg(results);
                                    clearInterval(timerS);
                                };
                            }, 100)
                        }
                    }
                }
            }
        };
        $.fn.botStream.hosts.push(vidcloud9_org);

        //https://anavids.com/embed-q2s8pm9xh6sc.html
        var anavids_com = {
            DOMAIN: 'anavids.com',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var html = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'afn'
                });
                var title = '';
                $(html).each(await function (index, item) {
                    if ($(item).prop("tagName") == 'TITLE') {
                        title = $(item).text();
                        return false;
                    }
                });
                var quality = libs.getQuality(title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };
                html.split('sources:').some(function (item) {
                    var pp = item.split('}],');
                    if (pp.length >= 1) {
                        try {
                            var streams = eval(pp[0] + '}]');
                            streams.some(function (obj) {
                                var type = 'mp4';
                                var cast = true;
                                if (obj.file.indexOf('.m3u8') > 0) {
                                    type = 'm3u8';
                                };
                                libs.postMsg(obj.file, type, libs.getResolution(obj.file), quality, 4, cast, false, {
                                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                                    'Referer': hostInfo.url,
                                    'Origin': 'https://' + libs.extractHostname(hostInfo.url)
                                });
                            })
                        } catch (error) {

                        }
                    }
                });
            }
        };
        $.fn.botStream.hosts.push(anavids_com);

        //https://vidoo.tv/e/b19qlz8pjzz8
        var vidoo_tv = {
            DOMAIN: 'vidoo.tv',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var html = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'afn'
                });
                var title = '';
                $(html).each(await function (index, item) {
                    if ($(item).prop("tagName") == 'TITLE') {
                        title = $(item).text();
                        return false;
                    }
                });
                var quality = libs.getQuality(title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };
                html.split('sources:').some(function (item) {
                    var pp = item.split('}],');
                    if (pp.length >= 1) {
                        try {
                            var streams = eval(pp[0] + '}]');
                            streams.some(function (obj) {
                                var type = 'mp4';
                                var cast = true;
                                if (obj.file.indexOf('.m3u8') > 0) {
                                    type = 'm3u8';
                                };
                                libs.postMsg(obj.file, type, libs.getResolution(obj.file), quality, 4, cast, false, {
                                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                                    'Referer': hostInfo.url,
                                    'Origin': 'https://' + libs.extractHostname(hostInfo.url)
                                });
                            })
                        } catch (error) {

                        }
                    }
                });
            }
        };
        $.fn.botStream.hosts.push(vidoo_tv);

        //https://vidoo.streamango.to/e/b19qlz8pjzz8
        var vidoo_streamango_to = {
            DOMAIN: 'vidoo.streamango.to',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('vidoo.streamango.to', 'vidoo.tv')
                });
            }
        };
        $.fn.botStream.hosts.push(vidoo_streamango_to);

        //https://oogly.io/wyzqo5558ix6.html
        var oogly_io = {
            DOMAIN: 'oogly.io',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var html = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'afn'
                });
                var title = '';
                $(html).each(await function (index, item) {
                    if ($(item).prop("tagName") == 'TITLE') {
                        title = $(item).text();
                        return false;
                    }
                });
                var quality = libs.getQuality(title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };
                html.split('sources:').some(function (item) {
                    var pp = item.split('}],');
                    if (pp.length >= 1) {
                        try {
                            var streams = eval(pp[0] + '}]');
                            streams.some(function (obj) {
                                var type = 'mp4';
                                var cast = true;
                                if (obj.file.indexOf('.m3u8') > 0) {
                                    type = 'm3u8';
                                };
                                if (obj.file.indexOf('.mpd') > 0) {
                                    return false;
                                };
                                libs.postMsg(obj.file, type, libs.getResolution(obj.file), quality, 4, cast, false, {
                                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                                    'Referer': hostInfo.url
                                });
                            })
                        } catch (error) {

                        }
                    }
                });
            }
        };
        $.fn.botStream.hosts.push(oogly_io);

        //https://streamtape.com/e/ew9MMbm9W8cOQL/9-1-1.S03E18.WEB.X264-ALiGN_.mp4
        var streamtape_com = {
            DOMAIN: 'streamtape.com',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var jj = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'web',
                    script: 'var postMsg=' + this.postMsg + ';var botScript = ' + this.check_ddos + ';botScript();'
                });
                var json = JSON.parse(jj);
                var title = json.title;
                var quality = libs.getQuality(title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };

                var src = json.src;
                var that = this;
                that.HEADER.referer = hostInfo.url;
                var headersjj = await libs.postHTML({
                    url: src,
                    headers: that.HEADER,
                    type: 'afn',
                    selfsign: true,
                    method: 'head'
                });
                var json = JSON.parse(headersjj);
                if (json.hasOwnProperty('location')) {
                    src = json.location;
                } else if (json.hasOwnProperty('Location')) {
                    src = json.Location;
                }
                // var parser = new DOMParser();
                // var doc = parser.parseFromString(html, "text/html");
                // var src = 'https:' + doc.querySelector('#videolink').innerText;

                var type = 'mp4';
                var cast = true;
                if (src.indexOf('.m3u8') > 0) {
                    type = 'm3u8';
                };
                libs.postMsg(src, type, libs.getResolution(src), quality, 4, cast, false, {
                    'User-Agent': libs.randomUserAgent(),
                    'Referer': hostInfo.url
                });
            },
            postMsg: function (json) {

                if (typeof jsinterface != 'undefined') {
                    jsinterface.getSomeString(JSON.stringify(json));
                } else if (typeof window != 'undefined' && typeof window.webkit != 'undefined' && window.webkit && window.webkit.messageHandlers) {
                    window.webkit.messageHandlers.callbackHandler.postMessage(JSON.stringify(json));
                } else {
                    window.flutter_inappwebview.callHandler("jsinterface", json);
                }
            },
            check_ddos: function () {
                if (window.location === window.parent.location) {
                    setTimeout(function () {
                        postMsg({});
                    }, 10000);
                    var timer = setInterval(function () {
                        if (document.querySelector('title').innerText !== 'Just a moment...') {
                            var src = $('#norobotlink').text();
                            if (src.indexOf('//str') == 0) {
                                postMsg({
                                    title: $('title').text(),
                                    src: 'https:' + src
                                });
                                clearInterval(timer);
                            }
                        }
                    }, 100);
                };
            }
        };
        $.fn.botStream.hosts.push(streamtape_com);

        var streamtape_pe = {
            DOMAIN: 'streamtape.pe',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('streamtape.pe', 'streamtape.com')
                });
            }
        };
        $.fn.botStream.hosts.push(streamtape_pe);

        var streamtap_com = {
            DOMAIN: 'streamta.com',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('streamta.com', 'streamtape.com')
                });
            }
        };
        $.fn.botStream.hosts.push(streamtap_com);

        //https://videoko.to/emb.html?tz4nv8158xwy=fs15.videoko.to/i/01/00025/tz4nv8158xwy
        //https://videoko.to/embed-tz4nv8158xwy.html?auto=1
        var videoko_to = {
            DOMAIN: 'videoko.to',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var url = hostInfo.url;
                if (url.indexOf('embed-') < 0) {
                    url = hostInfo.url.replace('/emb.html?', '/embed-');
                    var parts = url.split('=');
                    if (parts.length != 2) {
                        return;
                    };
                    url = parts[0] + '.html?auto=1';
                };
                var html = await libs.postHTML({
                    url: url,
                    headers: this.HEADER,
                    type: 'afn',
                });

                var title = '';
                $(html).each(await function (index, item) {
                    if ($(item).prop("tagName") == 'TITLE') {
                        title = $(item).text();
                        return false;
                    }
                });

                var quality = libs.getQuality(title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };

                var pattern = />eval(.*)/ig;
                var match = pattern.exec(html);
                try {
                    if (match && match.length > 1) {
                        eval(match[1]).split('player.src(').some(function (item) {
                            var ss = item.split('},');
                            if (ss.length == 0) {
                                return false;
                            };

                            try {
                                var stream = eval('(' + ss[0] + '})');
                                if (stream.hasOwnProperty('src')) {
                                    var type = 'mp4';
                                    if (stream.src.indexOf('.m3u8') > 0) {
                                        type = 'm3u8';
                                    };
                                    libs.postMsg(stream.src, type, 720, quality, 4, true, false, {
                                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                                        'Referer': hostInfo.url,
                                        'Origin': 'https://' + libs.extractHostname(hostInfo.url)
                                    });
                                }
                            } catch (error) {

                            };
                        });
                    };
                } catch (error) {

                };
            }
        };
        $.fn.botStream.hosts.push(videoko_to);

        //https://vido.to/embed-6vfbvpkjo4kc.html
        var vido_to = {
            DOMAIN: 'vido.to',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var url = hostInfo.url;
                if (url.indexOf('embed-') < 0) {
                    url = hostInfo.url.replace('.to/', '.to/embed-');
                };
                this.HEADER.referer = url.replace('.html', '').replace('embed-', '');
                var html = await libs.postHTML({
                    url: url,
                    headers: this.HEADER,
                    type: 'afn',
                });

                var title = '';
                $(html).each(await function (index, item) {
                    if ($(item).prop("tagName") == 'TITLE') {
                        title = $(item).text();
                        return false;
                    }
                });

                var quality = libs.getQuality(title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };

                var pattern = />eval(.*)/ig;
                var match = pattern.exec(html);
                try {
                    if (match && match.length > 1) {
                        eval(match[1]).split('sources:').some(function (item) {
                            var ss = item.split('],');
                            if (ss.length == 0) {
                                return false;
                            };

                            try {
                                var streams = eval(ss[0] + ']');
                                streams.some(function (obj) {
                                    var type = 'mp4';
                                    if (obj.file.indexOf('.m3u8') > 0) {
                                        type = 'm3u8';
                                    };
                                    libs.postMsg(obj.file, type, libs.getResolution(obj.file), quality, 4, true, false, {
                                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                                        'Referer': hostInfo.url
                                    });
                                })
                            } catch (error) {

                            };
                        });
                    };
                } catch (error) {

                };
            }
        };
        $.fn.botStream.hosts.push(vido_to);

        //https://jetload.net/e/fGcjDh08eSTr
        var jetload_net = {
            DOMAIN: 'jetload.net',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var html = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'web',
                    script: 'var postMsg = ' + this.postMsg + '; var botScript = ' + this.getHTML + '; botScript();'
                });
                var title = '';
                $(html).each(await function (index, item) {
                    if ($(item).prop("tagName") == 'TITLE') {
                        title = $(item).text();
                        return false;
                    }
                });
                var quality = libs.getQuality(title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };

                var file = $(html).find('video').attr('src');
                var type = 'mp4';
                if (file.indexOf('.m3u8') > 0) {
                    type = 'm3u8';
                };
                libs.postMsg(file, type, libs.getResolution(file), quality, 4, true, false, {
                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                    'Referer': hostInfo.url,
                });
            },
            postMsg: function (html) {

                if (typeof jsinterface != 'undefined') {
                    jsinterface.getSomeString(html);
                } else if (typeof window != 'undefined' && typeof window.webkit != 'undefined' && window.webkit && window.webkit.messageHandlers) {
                    window.webkit.messageHandlers.callbackHandler.postMessage(html);
                } else {
                    window.flutter_inappwebview.callHandler("jsinterface", html);
                }
            },
            getHTML: function () {
                if (window.location === window.parent.location) {
                    setTimeout(function () {
                        postMsg('');
                    }, 10000);
                    var timer = setInterval(function () {
                        var video = document.querySelector('video');
                        if (video != null && video.getAttribute('src') != null && video.getAttribute('src').length > 0) {
                            clearInterval(timer);
                            var html = document.querySelector('html').innerHTML;
                            postMsg(html);
                        }
                    }, 100);
                }
            }
        };
        $.fn.botStream.hosts.push(jetload_net);

        //https://streamzz.to/fMjloZmwxY2d2aWZwcXds
        //https://streamz.vg/wdu?dill5kj72gzw3px5o8vblmsl5v6bmmy05jmb35j6li3p80uw9q8uc0fhvmti9rje
        var streamz_vg = {
            DOMAIN: 'streamz.vg',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var html = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'web',
                    script: 'var postMsg = ' + this.postMsg + '; var botScript = ' + this.getHTML + '; botScript();'
                });
                var title = '';
                $(html).each(await function (index, item) {
                    if ($(item).prop("tagName") == 'TITLE') {
                        title = $(item).text();
                        return false;
                    }
                });
                var quality = libs.getQuality(title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };

                var file = $(html).find('video').attr('src');
                var type = 'mp4';
                if (file.indexOf('.m3u8') > 0) {
                    type = 'm3u8';
                };
                libs.postMsg(file, type, libs.getResolution(file), quality, 4, true, false, {
                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                    'Referer': hostInfo.url,
                });
            },
            postMsg: function (html) {

                if (typeof jsinterface != 'undefined') {
                    jsinterface.getSomeString(html);
                } else if (typeof window != 'undefined' && typeof window.webkit != 'undefined' && window.webkit && window.webkit.messageHandlers) {
                    window.webkit.messageHandlers.callbackHandler.postMessage(html);
                } else {
                    window.flutter_inappwebview.callHandler("jsinterface", html);
                }
            },
            getHTML: function () {
                if (window.location === window.parent.location) {
                    setTimeout(function () {
                        postMsg('');
                    }, 10000);
                    var timer = setInterval(function () {
                        var video = document.querySelector('video');
                        if (video != null && video.getAttribute('src') != null && video.getAttribute('src').length > 0) {
                            clearInterval(timer);
                            var html = document.querySelector('html').innerHTML;
                            postMsg(html);
                        }
                    }, 100);
                }
            }
        };
        $.fn.botStream.hosts.push(streamz_vg);

        var streamzz_to = {
            DOMAIN: 'streamzz.to',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('streamzz.to', 'streamz.vg')
                });
            }
        };
        $.fn.botStream.hosts.push(streamzz_to);

        //https://supervideo.tv/e/svpjzsccc1ah
        var supervideo_tv = {
            DOMAIN: 'supervideo.tv',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var url = hostInfo.url;
                var html = await libs.postHTML({
                    url: url,
                    headers: this.HEADER,
                    type: 'afn',
                });

                var title = '';
                $(html).each(await function (index, item) {
                    if ($(item).prop("tagName") == 'TITLE') {
                        title = $(item).text();
                        return false;
                    }
                });

                var quality = libs.getQuality(title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };

                var pattern = />eval(.*)/ig;
                var match = pattern.exec(html);
                try {
                    if (match && match.length > 1) {
                        eval(match[1]).split('sources:').some(function (item) {
                            var ss = item.split('],');
                            if (ss.length == 0) {
                                return false;
                            };

                            try {
                                var streams = eval(ss[0] + ']');
                                streams.some(function (obj) {
                                    var type = 'mp4';
                                    var cast = false;
                                    if (obj.file.indexOf('.m3u8') > 0) {
                                        type = 'm3u8';
                                        cast = true;
                                    };
                                    var resolution = libs.getResolution(obj.file);
                                    if (obj.hasOwnProperty('label')) {
                                        resolution = obj.label.replace('p', '');
                                    };
                                    libs.postMsg(obj.file, type, resolution, quality, 4, cast, false, {
                                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                                        'Referer': hostInfo.url
                                    });
                                })
                            } catch (error) {

                            };
                        });
                    };
                } catch (error) {

                };
            }
        };
        $.fn.botStream.hosts.push(supervideo_tv);

        //https://vidbm.com/embed-eycrmxuvx3bq.html
        var vidbm_com = {
            DOMAIN: 'vidbm.com',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var jj = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'web',
                    script: 'var postMsg = ' + this.postMsg + '; var botScript = ' + this.getHTML + '; botScript();'
                });
                var json = JSON.parse(jj);
                var quality = libs.getQuality('');
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };

                json.some(function (item) {
                    var file = item.file;
                    var type = 'mp4';
                    if (file.indexOf('.m3u8') > 0) {
                        type = 'm3u8';
                    };
                    libs.postMsg(file, type, item.label, quality, 4, true, false, {
                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                        'Referer': hostInfo.url,
                    });
                })
            },
            postMsg: function (json) {

                if (typeof jsinterface != 'undefined') {
                    jsinterface.getSomeString(JSON.stringify(json));
                } else if (typeof window != 'undefined' && typeof window.webkit != 'undefined' && window.webkit && window.webkit.messageHandlers) {
                    window.webkit.messageHandlers.callbackHandler.postMessage(JSON.stringify(json));
                } else {
                    window.flutter_inappwebview.callHandler("jsinterface", json);
                }
            },
            getHTML: function () {
                if (window.location === window.parent.location) {
                    setTimeout(function () {
                        postMsg({});
                    }, 10000);
                    var timer = setInterval(function () {
                        if (typeof jwplayer !== 'undefined' && typeof jwplayer().getPlaylist() !== 'undefined' && jwplayer().getPlaylist().length > 0 && jwplayer().getPlaylist()[0].sources.length > 0) {
                            var sources = jwplayer().getPlaylist()[0].sources;
                            var results = [];
                            for (var i = 0; i < sources.length; i++) {
                                if (typeof sources[i].file !== 'undefined') {
                                    var label = 720;
                                    if (typeof sources[i].label !== 'undefined') {
                                        label = sources[i].label.replace('p', '');
                                        var parsed = parseInt(label);
                                        label = isNaN(parsed) ? 720 : parsed;
                                    };
                                    results.push({
                                        file: sources[i].file,
                                        label: label
                                    });
                                }
                            }
                            postMsg(results);
                            clearInterval(timer);
                        };
                    }, 100);
                }
            }
        };
        $.fn.botStream.hosts.push(vidbm_com);

        //https://vidshare.tv/embed-s0axonccxulc.html
        var vidshare_tv = {
            DOMAIN: 'vidshare.tv',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var html = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'web',
                    script: 'var postMsg = ' + this.postMsg + '; var botScript = ' + this.getHTML + '; botScript();'
                });
                var title = '';
                $(html).each(await function (index, item) {
                    if ($(item).prop("tagName") == 'TITLE') {
                        title = $(item).text();
                        return false;
                    }
                });
                var quality = libs.getQuality(title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };
                html.split('sources: ').some(function (item) {
                    var pattern = /(.*)],/ig;
                    var match = pattern.exec(item);
                    try {
                        if (match && match.length > 1) {
                            var streams = eval(match[1] + ']');
                            streams.some(function (file) {
                                var type = 'mp4';
                                if (file.indexOf('.m3u8') > 0) {
                                    type = 'm3u8';
                                };
                                libs.postMsg(file, type, libs.getResolution(file), quality, 4, true, false, {
                                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                                    'Referer': hostInfo.url
                                });
                            })
                        };
                    } catch (error) {

                    };
                });
            },
            postMsg: function (html) {

                if (typeof jsinterface != 'undefined') {
                    jsinterface.getSomeString(html);
                } else if (typeof window != 'undefined' && typeof window.webkit != 'undefined' && window.webkit && window.webkit.messageHandlers) {
                    window.webkit.messageHandlers.callbackHandler.postMessage(html);
                } else {
                    window.flutter_inappwebview.callHandler("jsinterface", html);
                }
            },
            getHTML: function () {
                if (window.location === window.parent.location) {
                    setTimeout(function () {
                        postMsg('');
                    }, 10000);
                    var timer = setInterval(function () {
                        if (document.querySelector('#vplayer') != null) {
                            clearInterval(timer);
                            var html = document.querySelector('body').innerHTML;
                            postMsg(html);
                        }
                    }, 100);
                }
            }
        };
        $.fn.botStream.hosts.push(vidshare_tv);

        //https://uqload.com/embed-flofg9kullz1.html
        var uqload_com = {
            DOMAIN: 'uqload.com',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var html = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'afn'
                });
                var title = '';
                $(html).each(await function (index, item) {
                    if ($(item).prop("tagName") == 'TITLE') {
                        title = $(item).text();
                        return false;
                    }
                });
                var quality = libs.getQuality(title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };
                html.split('sources: ').some(function (item) {
                    var pattern = /(.*)],/ig;
                    var match = pattern.exec(item);
                    try {
                        if (match && match.length > 1) {
                            var streams = eval(match[1] + ']');
                            streams.some(function (file) {
                                var type = 'mp4';
                                if (file.indexOf('.m3u8') > 0) {
                                    type = 'm3u8';
                                };
                                libs.postMsg(file, type, libs.getResolution(file), quality, 4, true, false, {
                                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                                    'Referer': hostInfo.url
                                });
                            })
                        };
                    } catch (error) {

                    };
                });
            }
        };
        $.fn.botStream.hosts.push(uqload_com);

        //https://aparat.cam/5wbmbirwm409/NCIS_S17E20_HDTV_x264-SVA.mp4.html
        var aparat_cam = {
            DOMAIN: 'aparat.cam',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var html = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'afn'
                });
                var title = '';
                $(html).each(await function (index, item) {
                    if ($(item).prop("tagName") == 'TITLE') {
                        title = $(item).text();
                        return false;
                    }
                });
                var quality = libs.getQuality(title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };

                html.split('sources: ').some(function (item) {
                    var pattern = /(.*)],/ig;
                    var match = pattern.exec(item);
                    try {
                        if (match && match.length > 1) {
                            var streams = eval(match[1] + ']');
                            streams.some(function (file) {
                                var type = 'mp4';
                                if (file.file.indexOf('.m3u8') > 0) {
                                    type = 'm3u8';
                                };
                                libs.postMsg(file.file, type, libs.getResolution(file.file), quality, 4, true, false, {
                                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                                    'Referer': hostInfo.url
                                });
                            })
                        };
                    } catch (error) {

                    };
                });
            }
        };
        $.fn.botStream.hosts.push(aparat_cam);

        //https://vup.to/o9aqorw09hs6.html
        var vup_to = {
            DOMAIN: 'vup.to',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var url = hostInfo.url;
                var html = await libs.postHTML({
                    url: url,
                    headers: this.HEADER,
                    type: 'afn',
                });

                var title = '';
                $(html).each(await function (index, item) {
                    if ($(item).prop("tagName") == 'TITLE') {
                        title = $(item).text();
                        return false;
                    }
                });

                var quality = libs.getQuality(title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };

                var pattern = />eval(.*)/ig;
                var match = pattern.exec(html);
                try {
                    if (match && match.length > 1) {
                        eval(match[1]).split('sources:').some(function (item) {
                            var ss = item.split('],');
                            if (ss.length == 0) {
                                return false;
                            };

                            try {
                                var streams = eval(ss[0] + ']');
                                streams.some(function (obj) {
                                    var type = 'mp4';
                                    var cast = false;
                                    if (obj.src.indexOf('.m3u8') > 0) {
                                        type = 'm3u8';
                                        cast = true;
                                    };
                                    var resolution = libs.getResolution(obj.src);
                                    if (obj.hasOwnProperty('label')) {
                                        resolution = obj.label.replace('p', '');
                                    };
                                    libs.postMsg(obj.src, type, resolution, quality, 4, cast, false, {
                                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                                        'Referer': hostInfo.url
                                    });
                                })
                            } catch (error) {

                            };
                        });
                    };
                } catch (error) {

                };
            }
        };
        $.fn.botStream.hosts.push(vup_to);

        //https://easyload.io/e/DLX1A5G2XV/Cima_Now_Co_M_Youm_We_Lela_2020_HD_1080_p_mp4
        var easyload_io = {
            DOMAIN: 'easyload.io',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var html = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'web',
                    script: 'var postMsg = ' + this.postMsg + '; var botScript = ' + this.getHTML + '; botScript();'
                });
                var title = '';
                $(html).each(await function (index, item) {
                    if ($(item).prop("tagName") == 'TITLE') {
                        title = $(item).text();
                        return false;
                    }
                });
                var quality = libs.getQuality(title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };

                var file = $(html).find('video').attr('src');
                var type = 'mp4';
                if (file.indexOf('.m3u8') > 0) {
                    type = 'm3u8';
                };
                libs.postMsg(file, type, libs.getResolution(title), quality, 3, true, false, {
                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                    'Referer': hostInfo.url,
                });
            },
            postMsg: function (html) {

                if (typeof jsinterface != 'undefined') {
                    jsinterface.getSomeString(html);
                } else if (typeof window != 'undefined' && typeof window.webkit != 'undefined' && window.webkit && window.webkit.messageHandlers) {
                    window.webkit.messageHandlers.callbackHandler.postMessage(html);
                } else {
                    window.flutter_inappwebview.callHandler("jsinterface", html);
                }
            },
            getHTML: function () {
                if (window.location === window.parent.location) {
                    setTimeout(function () {
                        postMsg('');
                    }, 10000);
                    var timer = setInterval(function () {
                        var video = document.querySelector('video');
                        if (video != null && video.getAttribute('src') != null && video.getAttribute('src').length > 0) {
                            clearInterval(timer);
                            var html = document.querySelector('html').innerHTML;
                            postMsg(html);
                        }
                    }, 100);
                }
            }
        };
        $.fn.botStream.hosts.push(easyload_io);

        //https://thevideobee.to/embed-wly0q4l850v2.html
        var thevideobee_to = {
            DOMAIN: 'thevideobee.to',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var html = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'afn'
                });
                var title = '';
                $(html).each(await function (index, item) {
                    if ($(item).prop("tagName") == 'TITLE') {
                        title = $(item).text();
                        return false;
                    }
                });
                var quality = libs.getQuality(title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };

                html.split('sources:').some(function (item) {
                    var pp = item.split('],');
                    if (pp.length >= 2) {
                        try {
                            var streams = eval(pp[0] + ']');
                            streams.some(function (file) {
                                var type = 'mp4';
                                if (file.indexOf('.m3u8') > 0) {
                                    type = 'm3u8';
                                };
                                libs.postMsg(file, type, libs.getResolution(file), quality, 4, true, false, {
                                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                                    'Referer': hostInfo.url,
                                    'Origin': 'https://' + libs.extractHostname(hostInfo.url)
                                });
                            })
                        } catch (error) {

                        }
                    }
                });
            }
        };
        $.fn.botStream.hosts.push(thevideobee_to);

        //https://voe.sx/evfmj7u64qof
        var voe_sx = {
            DOMAIN: 'voe.sx',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36 OPR/85.0.4341.75',
            },
            getStream: async function (hostInfo, libs) {
                var html = await libs.postHTML({
                    url: hostInfo.url.replace('/d/', '/e/'),
                    headers: this.HEADER,
                    type: 'afn'
                });
                var title = '';
                $(html).each(await function (index, item) {
                    if ($(item).prop("tagName") == 'TITLE') {
                        title = $(item).text();
                        return false;
                    }
                });
                var quality = libs.getQuality(title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };

                html.split('const sources =').some(function (item) {
                    var pp = item.split('};');
                    if (pp.length >= 2) {
                        try {

                            var stream = JSON.parse(pp[0].replaceAll(new RegExp(',\\n(.+){0,1}$', 'g'), '') + '}');
                            var type = 'mp4';
                            if (stream.hls.indexOf('.m3u8') > 0) {
                                type = 'm3u8';
                            };
                            libs.postMsg(stream.hls, type, stream.video_height, quality, 4, true, false, {
                                'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                                'Referer': hostInfo.url,
                                'Origin': 'https://' + libs.extractHostname(hostInfo.url)
                            });
                        } catch (error) {

                        }
                    }
                });
            }
        };
        $.fn.botStream.hosts.push(voe_sx);

        //https://upvid.net/embed-dze31ynfjnet-_-amxjRkhNbXAzSGRmMy9ML00xeHoxdG13NzU3dUsyWWwrWWJsUDBtRDdLVXNDVE92MnNIelJxK1dOWEZxenQ0QnNkSms0L2xxZWFKRAo1OFNDRGt2Y2tVWEI1bzVaaTVXbEZzeE8vblNEbUk2U3N2WmJRL0FIWHZwS2ZaeEREVEpFUk9IR1c2VVQxcHMycWNoa294azZvbk12CkRTUS83Tm9JNnlvcGpiS0ZtSzd0VW1TVGlCV1FiSEN4MFE9PQo=.html?543598247
        var upvid_net = {
            DOMAIN: 'upvid.net',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var jj = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'web',
                    script: 'var postMsg = ' + this.postMsg + '; var botScript = ' + this.getVideo + '; botScript();'
                });
                var json = JSON.parse(jj);

                var type = 'mp4';
                if (json.file.indexOf('.m3u8') > 0) {
                    type = 'm3u8';
                };
                var quality = libs.getQuality(json.title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };
                libs.postMsg(json.file, type, libs.getResolution(json.file), quality, 3, true, false, {
                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                    'Referer': hostInfo.url,
                    'Origin': 'https://' + libs.extractHostname(hostInfo.url)
                });
            },
            postMsg: function (json) {

                if (typeof jsinterface != 'undefined') {
                    jsinterface.getSomeString(JSON.stringify(json));
                } else if (typeof window != 'undefined' && typeof window.webkit != 'undefined' && window.webkit && window.webkit.messageHandlers) {
                    window.webkit.messageHandlers.callbackHandler.postMessage(JSON.stringify(json));
                } else {
                    window.flutter_inappwebview.callHandler("jsinterface", json);
                }
            },
            getVideo: function () {
                if (window.location === window.parent.location) {
                    setTimeout(function () {
                        postMsg({});
                    }, 10000);
                    var timer = setInterval(function () {
                        if (document.querySelector('title').innerText !== 'Just a moment...' &&
                            typeof Code == 'function') {
                            clearInterval(timer);
                            Code();
                        }
                    }, 100);
                    var timer2 = setInterval(function () {
                        var video = document.querySelector('video');
                        if (video != null && video.getAttribute('src') != null && video.getAttribute('src').length > 0) {
                            clearInterval(timer2);
                            var meta = document.querySelector('meta[name="twitter:title"]');
                            var title = meta != null ? meta.getAttribute('content') : '';
                            postMsg({
                                file: video.getAttribute('src'),
                                title: title
                            });
                        }
                    }, 100);
                };
            }
        };
        $.fn.botStream.hosts.push(upvid_net);

        //https://jawcloud.co/embed-rc1vu4bh9tnq.html
        var jawcloud_co = {
            DOMAIN: 'jawcloud.co',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var html = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'afn'
                });
                var title = '';
                $(html).each(await function (index, item) {
                    if ($(item).prop("tagName") == 'TITLE') {
                        title = $(item).text();
                        return false;
                    }
                });
                var quality = libs.getQuality(title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };

                var file = $(html).find('video > source').attr('src');
                var type = 'mp4';
                if (file.indexOf('.m3u8') > 0) {
                    type = 'm3u8';
                };
                libs.postMsg(file, type, libs.getResolution(file), quality, 4, true, false, {
                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                    'Referer': hostInfo.url,
                });
            }
        };
        $.fn.botStream.hosts.push(jawcloud_co);

        //https://yodbox.com/0yvgkpn5xcyd/Superman.and.Lois.S02E14.HDTV.x264-TORRENTGALAXY.mkv.html
        var yodbox_com = {
            DOMAIN: 'yodbox.com',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var html = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'afn'
                });
                var title = '';
                $(html).each(await function (index, item) {
                    if ($(item).prop("tagName") == 'TITLE') {
                        title = $(item).text();
                        return false;
                    }
                });
                var quality = libs.getQuality(title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };

                var file = $(html).find('video > source').attr('src');
                var type = 'mp4';
                if (file.indexOf('.m3u8') > 0) {
                    type = 'm3u8';
                };
                libs.postMsg(file, type, libs.getResolution(file), quality, 4, true, false, {
                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                    'Referer': hostInfo.url,
                });
            }
        };
        $.fn.botStream.hosts.push(yodbox_com);

        //https://movcloud.net/embed/pd-7vbtTX6Uc#poster=
        var movcloud_net = {
            DOMAIN: 'movcloud.net',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var url = 'https://api.' + this.DOMAIN + '/stream/' + hostInfo.url.replace(new RegExp('(.*)/embed\/([0-9a-zA-Z_-]+)(.*){0,1}', 'ig'), '$2');
                this.HEADER.referer = hostInfo.url;
                this.HEADER.origin = 'https://' + libs.extractHostname(hostInfo.url);
                this.HEADER['x-requested-with'] = 'XMLHttpRequest';

                var ajax = await libs.postHTML({
                    url: url,
                    headers: this.HEADER,
                    type: 'afn'
                });
                var json = JSON.parse(ajax);
                if (json.success == false) {
                    return;
                };

                json.data.sources.some(function (item) {
                    var type = 'mp4';
                    if (item.file.indexOf('.m3u8') > 0) {
                        type = 'm3u8';
                    };
                    libs.postMsg(item.file, type, libs.getResolution(item.file), libs.getQuality(hostInfo.quality), 4, true, false, {
                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                        'Referer': hostInfo.url
                    });
                });
            }
        };
        $.fn.botStream.hosts.push(movcloud_net);

        //https://cloud9.to/embed/iy-poTn2x-PX
        var cloud9_to = {
            DOMAIN: 'cloud9.to',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var url = 'https://api.' + this.DOMAIN + '/stream/' + hostInfo.url.replace(new RegExp('(.*)/embed\/([0-9a-zA-Z_-]+)(.*){0,1}', 'ig'), '$2');
                this.HEADER.referer = hostInfo.url;
                this.HEADER.origin = 'https://' + libs.extractHostname(hostInfo.url);
                this.HEADER['x-requested-with'] = 'XMLHttpRequest';

                var ajax = await libs.postHTML({
                    url: url,
                    headers: this.HEADER,
                    type: 'afn'
                });
                var json = JSON.parse(ajax);
                if (json.success == false) {
                    return;
                };

                json.data.sources.some(function (item) {
                    var type = 'mp4';
                    if (item.file.indexOf('.m3u8') > 0) {
                        type = 'm3u8';
                    };
                    libs.postMsg(item.file, type, libs.getResolution(item.file), libs.getQuality(hostInfo.quality), 4, true, false, {
                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                        'Referer': hostInfo.url
                    });
                });
            }
        };
        $.fn.botStream.hosts.push(cloud9_to);

        //https://dood.pm/d/1g4kvglsn3pb
        var dood_pm = {
            DOMAIN: 'dood.pm',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var url = hostInfo.url.replace('/d/', '/e/');
                var jj = await libs.postHTML({
                    url: url,
                    headers: this.HEADER,
                    type: 'web',
                    script: 'var postMsg = ' + this.postMsg + '; var botScript = ' + this.check_ddos + '; botScript();'
                });
                var json = JSON.parse(jj);
                var title = json.title;

                var quality = libs.getQuality(title);
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };

                var file = json.file;
                var type = 'mp4';
                if (file.indexOf('.m3u8') > 0) {
                    type = 'm3u8';
                };
                libs.postMsg(file, type, libs.getResolution(title), quality, 4, true, false, {
                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                    'Referer': hostInfo.url
                });
            },
            postMsg: function (json) {

                if (typeof jsinterface != 'undefined') {
                    jsinterface.getSomeString(JSON.stringify(json));
                } else if (typeof window != 'undefined' && typeof window.webkit != 'undefined' && window.webkit && window.webkit.messageHandlers) {
                    window.webkit.messageHandlers.callbackHandler.postMessage(JSON.stringify(json));
                } else {
                    window.flutter_inappwebview.callHandler("jsinterface", json);
                }
            },
            check_ddos: function () {
                if (window.location === window.parent.location) {
                    setTimeout(function () {
                        postMsg({});
                    }, 20000);
                    var timer = setInterval(function () {
                        if (document.querySelector('title').innerText !== 'Just a moment...' &&
                            typeof dsplayer != 'undefined' && dsplayer.currentSrc().length > 0) {
                            clearInterval(timer);
                            postMsg({
                                file: dsplayer.currentSrc(),
                                title: document.querySelector('title').innerText
                            })
                        }
                    }, 100);
                };
            }
        };
        $.fn.botStream.hosts.push(dood_pm);

        //https://dood.sh/e/n1r4guv8qhdz86rhnhpggoixvjdtsm4
        var dood_sh = {
            DOMAIN: 'dood.sh',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('dood.sh', 'dood.pm').replace('/d/', '/e/')
                });
            }
        };
        $.fn.botStream.hosts.push(dood_sh);

        //https://dood.to/e/n1r4guv8qhdz86rhnhpggoixvjdtsm4
        var dood_to = {
            DOMAIN: 'dood.to',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('dood.watch', 'dood.sh').replace('/d/', '/e/')
                });
            }
        };
        $.fn.botStream.hosts.push(dood_to);

        //https://dood.so/d/t0woy9n1o2d1
        var dood_to = {
            DOMAIN: 'dood.so',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('dood.so', 'dood.sh').replace('/d/', '/e/')
                });
            }
        };
        $.fn.botStream.hosts.push(dood_to);

        //https://dood.watch/e/n1r4guv8qhdz86rhnhpggoixvjdtsm4
        var dood_watch = {
            DOMAIN: 'dood.watch',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('dood.watch', 'dood.sh').replace('/d/', '/e/')
                });
            }
        };
        $.fn.botStream.hosts.push(dood_watch);

        //https://dood.la/e/n1r4guv8qhdz86rhnhpggoixvjdtsm4
        var dood_la = {
            DOMAIN: 'dood.la',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('dood.la', 'dood.sh').replace('/d/', '/e/')
                });
            }
        };
        $.fn.botStream.hosts.push(dood_la);

        //https://doodstream.com/d/ux64lmhap1lr
        var doodstream_com = {
            DOMAIN: 'doodstream.com',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('doodstream.com', 'dood.sh').replace('/d/', '/e/')
                });
            }
        };
        $.fn.botStream.hosts.push(doodstream_com);

        //https://dood.ws/e/lxowjn5y1ogj
        var dood_ws = {
            DOMAIN: 'dood.ws',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                $.fn.botStream.hostStreams.push({
                    quality: hostInfo.quality,
                    url: hostInfo.url.replace('dood.ws', 'dood.sh').replace('/d/', '/e/')
                });
            }
        };
        $.fn.botStream.hosts.push(dood_ws);

        //https://m4ufree.yt/public/dist/index.html?id=482ae42d861f1721efaf357a1329724b
        var m4ufree_yt = {
            DOMAIN: 'm4ufree.yt',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                var key = hostInfo.url.replace('https://m4ufree.yt/public/dist/index.html?id=', '');
                var file = 'https://' + this.DOMAIN + '/hls/' + key + '/' + key + '.m3u8';
                var type = 'mp4';
                if (file.indexOf('.m3u8') > 0) {
                    type = 'm3u8';
                };
                libs.postMsg(file, type, libs.getResolution(file), libs.getQuality(hostInfo.quality), 4, true, false, {
                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                    'Referer': hostInfo.url
                });
            }
        };
        $.fn.botStream.hosts.push(m4ufree_yt);

        //https://play.voxzer.org/watch?v=gAAAAABfHRc_g--UZWGb9GaJc34J0UwJwBJvaSeFO4B11dNx0-8bUORWy1Y9SeEaj7KJy2xYNrAyFcq3BWawfF7iMdRwXfKXfX0uuJRUU43aixzt6ICMGt4K6ufFuIl0-WsNKVUcnMtaR96MG2QHDnAzLcihP6POMw==
        var play_voxzer_org = {
            DOMAIN: 'play.voxzer.org',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                if (hostInfo.hasOwnProperty('referer')) {
                    this.HEADER.referer = hostInfo.referer;
                } else {
                    this.HEADER.referer = 'https://google.com';
                };
                var html = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'afn'
                });

                var code = '';
                var match = /{'code':'(.*?)'}/ig.exec(html);
                if (match == null || match.length == 0) {
                    match = /'gAAAA(.*?)'/ig.exec(html);
                    if (match == null || match.length == 0) {
                        return;
                    }
                    code = 'gAAAA' + match[1];
                } else {
                    code = match[1];
                };

                this.HEADER.referer = hostInfo.url;
                this.HEADER.origin = 'https://' + this.DOMAIN;
                this.HEADER['x-requested-with'] = 'XMLHttpRequest';
                this.HEADER['content-type'] = 'application/json';
                var jj = await libs.postHTML({
                    url: 'https://' + this.DOMAIN + '/data',
                    headers: this.HEADER,
                    type: 'afn',
                    body: `{"code":"${code}"}`,
                    method: 'post'
                });

                if (jj.length == 0) {
                    return;
                }
                var json = JSON.parse(jj);
                if (json.status == false) {
                    return;
                }

                var file = atob(json.url);

                if (file.indexOf('.m3u8') < 0 && file.indexOf('.mp4') < 0) {
                    // $.fn.botStream.hostStreams.push({
                    //     quality: quality,
                    //     url: file
                    // });
                    return;
                };

                if (file.indexOf('https://') != 0) {
                    file = 'https://' + this.DOMAIN + file;
                };
                var type = 'mp4';
                if (file.indexOf('.m3u8') > 0) {
                    type = 'm3u8';
                };
                var quality;
                if (typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                } else {
                    quality = libs.getQuality('');
                };
                libs.postMsg(file, type, libs.getResolution(file), quality, 4, false, false, {
                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                    'Referer': hostInfo.url
                });
            }
        };
        $.fn.botStream.hosts.push(play_voxzer_org);

        //https://player.voxzer.org/view/e027652b04f6855ddc5275e0
        var player_voxzer_org = {
            DOMAIN: 'player.voxzer.org',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                this.HEADER.referer = hostInfo.url;
                var jj = await libs.postHTML({
                    url: hostInfo.url.replace('/view/', '/list/'),
                    headers: this.HEADER,
                    type: 'afn'
                });
                var json = JSON.parse(jj);
                var quality = libs.getQuality('');
                if (quality == null && typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                    quality = libs.getQuality(hostInfo.quality);
                };

                var file = json.link;
                var type = 'mp4';
                if (file.indexOf('.m3u8') > 0) {
                    type = 'm3u8';
                };
                libs.postMsg(file, type, libs.getResolution(file), quality, 4, true, false, {
                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                    'Referer': hostInfo.url,
                });
            }
        };
        $.fn.botStream.hosts.push(player_voxzer_org);

        //https://vidstream.pro/e/26J3N7R23JRD?site=gogoanime.pro
        //https://vidstream.pro/embed/6EJV1XXZNP9M?site=gogoanime.pro
        var vidstream_pro = {
            DOMAIN: 'vidstream.pro',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                this.HEADER.referer = hostInfo.referer;
                var html = await libs.postHTML({
                    url: hostInfo.url,
                    headers: this.HEADER,
                    type: 'afn'
                });

                var skey = '';
                var match = /window.skey = '(.+)';/ig.exec(html);
                if (match != null && match.length > 1) {
                    skey = match[1];
                }

                this.HEADER.referer = hostInfo.url;
                var jj = await libs.postHTML({
                    url: hostInfo.url.replace('/embed/', '/info/') + '?skey=' + skey,
                    headers: this.HEADER,
                    type: 'afn'
                });

                var json = JSON.parse(jj);
                if (json.success == false) {
                    return;
                }

                json.media.sources.some(function (item) {
                    var type = 'mp4';
                    if (item.file.indexOf('.m3u8') > 0) {
                        type = 'm3u8';
                    };
                    var quality;
                    if (typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                        quality = libs.getQuality(hostInfo.quality);
                    } else {
                        quality = libs.getQuality('');
                    };
                    libs.postMsg(item.file, type, libs.getResolution(item.file), quality, 4, false, false, {
                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                        'Referer': hostInfo.url
                    });
                });
            }
        };
        $.fn.botStream.hosts.push(vidstream_pro);

        //https://mcloud2.to/embed/7xpwky?site=gogoanime.pro
        var mcloud2_to = {
            DOMAIN: 'mcloud2.to',
            HEADER: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            },
            getStream: async function (hostInfo, libs) {
                this.HEADER.referer = hostInfo.url;
                var jj = await libs.postHTML({
                    url: hostInfo.url.replace('/e/', '/info/'),
                    headers: this.HEADER,
                    type: 'afn'
                });

                var json = JSON.parse(jj);
                if (json.success == false) {
                    return;
                }

                json.media.sources.some(function (item) {
                    var type = 'mp4';
                    if (item.file.indexOf('.m3u8') > 0) {
                        type = 'm3u8';
                    };
                    var quality;
                    if (typeof hostInfo.quality !== 'undefined' && hostInfo.quality != null && hostInfo.quality.length > 0) {
                        quality = libs.getQuality(hostInfo.quality);
                    } else {
                        quality = libs.getQuality('');
                    };
                    libs.postMsg(item.file, type, libs.getResolution(item.file), quality, 4, false, false, {
                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                        'Referer': hostInfo.url
                    });
                });
            }
        };
        $.fn.botStream.hosts.push(mcloud2_to);
        //#endregion


        //#region TEST STREAM
        //
        // $.fn.botStream.hostStreams.push({
        //     quality: '',//
        //     url: 'https://vidstreaming.io/streaming.php?id=MTIxNzk0&title=Naruto+Shippuuden+%28Dub%29+Episode+500'
        // });
        // $.fn.botStream.hostStreams.push({
        //     quality: '',//
        //     url: 'https://upstream.to/9b3no7f6ith2'
        // });

        // $.fn.botStream.hostStreams.push({
        //     quality: '',
        //     url: 'https://vev.red/4jrlpkqvg6oy'
        // });

        // $.fn.botStream.hostStreams.push({
        //     quality: '',
        //     url: 'https://vev.io/4jrlpkqvg6oy'
        // });

        // $.fn.botStream.hostStreams.push({
        //     quality: '',
        //     url: 'https://vidup.io/56wkxp9zd73z'
        // });
        // $.fn.botStream.hostStreams.push({
        //     quality: '',//
        //     url: 'https://mixdrop.co/e/md1w93mzu883g9'
        // });
        // $.fn.botStream.hostStreams.push({
        //     quality: '',
        //     url: 'https://vidia.tv/g3wyxlc67co6.html'
        // });
        // $.fn.botStream.hostStreams.push({
        //     quality: '',//
        //     url: 'https://www.fembed.com/v/-zmqkhp2r3kq3wn'
        // });
        // $.fn.botStream.hostStreams.push({
        //     quality: '',//
        //     url: 'http://driveproxy.net/drive/eWtsN1MrVGFmSkFPS3ZCaUZRQUNOZz09.html?hls=1'
        // });
        // $.fn.botStream.hostStreams.push({
        //     quality: '',
        //     url: 'https://gounlimited.to/udutp6f7u1c9/9-1-1.S03E18.WEB.X264-ALiGN_.mp4'
        // });
        // $.fn.botStream.hostStreams.push({
        //     quality: '',//
        //     url: 'https://videobin.co/yzruf5coztrr'
        // });
        // $.fn.botStream.hostStreams.push({
        //     quality: '',
        //     url: 'https://prostream.to/embed-r5cmtw0dxe2x.html'
        // });
        // $.fn.botStream.hostStreams.push({
        //     quality: '',//
        //     url: 'https://cloudvideo.tv/embed-65qem6e0gd0h.html'
        // });
        // $.fn.botStream.hostStreams.push({
        //     quality: '',//
        //     url: 'https://streamwire.net/e/buy8rrh4psu4'
        // });
        // $.fn.botStream.hostStreams.push({
        //     quality: '',
        //     url: 'https://onlystream.tv/8vmr4tm2h2y5'
        // });
        // $.fn.botStream.hostStreams.push({
        //     quality: '',//
        //     url: 'https://clipwatching.com/jy6udxrgw3mj/the.bachelor.listen.to.your.heart.s01e05.web.h264-tbs.mkv.html'
        // });
        // $.fn.botStream.hostStreams.push({
        //     quality: '',
        //     url: 'https://vshare.eu/470j85wenili.htm'
        // });
        // $.fn.botStream.hostStreams.push({
        //     quality: '',//
        //     url: 'https://vidoza.net/opp1nme5vsa2.html'
        // });
        // $.fn.botStream.hostStreams.push({
        //     quality: '',//
        //     url: 'https://vidlox.me/7oa718mm20hx'
        // });
        // $.fn.botStream.hostStreams.push({
        //     quality: '',//
        //     url: 'https://vidcloud9.com/streaming.php?id=MzAyMzg2'
        // });
        // $.fn.botStream.hostStreams.push({
        //     quality: '',//
        //     url: 'https://vidcloud.co/v/5da1bd52e8c98/the.walking.dead.s10e02.web.h264-tbs.mp4'
        // });
        // $.fn.botStream.hostStreams.push({
        //     quality: '',//
        //     url: 'https://vidcloud.pro/embed4/5hgz01viet0ww?i=fd35df66c162ba4377f1fa9670d2b924da9aa21cd8f36fea8f5187a57b737e84'
        // });
        // $.fn.botStream.hostStreams.push({
        //     quality: '',//
        //     url: 'https://mystream.to/watch/s5u0mu6xcftm'
        // });
        // $.fn.botStream.hostStreams.push({
        //     quality: '',
        //     url: 'https://playto-vid.com/yuywyi1patf0'
        // });
        // $.fn.botStream.hostStreams.push({
        //     quality: '',
        //     url: 'https://www.vidbull.tv/embed/7251'
        // });
        // $.fn.botStream.hostStreams.push({
        //     quality: '',//
        //     url: 'https://streamapi.xyz/connection?link1=jm71N0NRN0NtNDNcjC7~NFjXNI7F7YNI7R7w74NwNANiNCN6N679jj7271NgNjNcNX7v7l7RNjNb75NBN67-Nc7lNVNxNLNhNwNRNPN9NKNSNvNANa7Q7yNS7l76N07BNf764a8742466cf0003e439e5ecb1737bbbfcc527fcc0813bbf27541f7319df2febcjbNyj-Ndjv7lNXNjj8N4Ndj6jsN-7fjGNb7tNvjDNBNANY7kj6N5N87fNeNxNeNe7_7W7y7zNj7t&link2='
        // });
        // $.fn.botStream.hostStreams.push({
        //     quality: '',//
        //     url: 'https://abcvideo.cc/vleth6mx7lui.html'
        // });
        // $.fn.botStream.hostStreams.push({
        //     quality: '',//
        //     url: 'https://oogly.io/wyzqo5558ix6.html'
        // });
        // $.fn.botStream.hostStreams.push({
        //     quality: '',//
        //     url: 'https://streamtape.com/v/oe3W9RBDdrhJ0Oe/That_Time_I_Got_Reincarnated_as_a_Slime_Subbed_ep25_s2e1.mp4'
        // });
        // $.fn.botStream.hostStreams.push({
        //     quality: '',//
        //     url: 'https://videoko.to/emb.html?tz4nv8158xwy=fs15.videoko.to/i/01/00025/tz4nv8158xwy'
        // });
        // $.fn.botStream.hostStreams.push({
        //     quality: '',//
        //     url: 'https://vido.to/embed-6vfbvpkjo4kc.html'
        // });
        // $.fn.botStream.hostStreams.push({
        //     quality: '',//
        //     url: 'https://jetload.net/e/fGcjDh08eSTr'
        // });
        // $.fn.botStream.hostStreams.push({
        //     quality: '',//
        //     url: 'https://supervideo.tv/e/svpjzsccc1ah'
        // });
        // $.fn.botStream.hostStreams.push({
        //     quality: '',//
        //     url: 'https://www.vidbm.com/embed-vx1yy1m62wm8.html'
        // });
        // $.fn.botStream.hostStreams.push({
        //     quality: '',//
        //     url: 'https://vidshare.tv/embed-s0axonccxulc.html'
        // });
        // $.fn.botStream.hostStreams.push({
        //     quality: '',//
        //     url: 'https://uqload.com/embed-flofg9kullz1.html'
        // });
        // $.fn.botStream.hostStreams.push({
        //     quality: '',//
        //     url: 'https://easyload.io/e/DLX1A5G2XV/Cima_Now_Co_M_Youm_We_Lela_2020_HD_1080_p_mp4'
        // });
        // $.fn.botStream.hostStreams.push({
        //     quality: '',//
        //     url: 'https://thevideobee.to/embed-wly0q4l850v2.html'
        // });
        // $.fn.botStream.hostStreams.push({
        //     quality: '',//
        //     url: 'https://upvid.net/embed-dze31ynfjnet-_-amxjRkhNbXAzSGRmMy9ML00xeHoxdG13NzU3dUsyWWwrWWJsUDBtRDdLVXNDVE92MnNIelJxK1dOWEZxenQ0QnNkSms0L2xxZWFKRAo1OFNDRGt2Y2tVWEI1bzVaaTVXbEZzeE8vblNEbUk2U3N2WmJRL0FIWHZwS2ZaeEREVEpFUk9IR1c2VVQxcHMycWNoa294azZvbk12CkRTUS83Tm9JNnlvcGpiS0ZtSzd0VW1TVGlCV1FiSEN4MFE9PQo=.html?543598247'
        // });
        // $.fn.botStream.hostStreams.push({
        //     quality: '',//
        //     url: 'https://jawcloud.co/embed-rc1vu4bh9tnq.html'
        // });
        // $.fn.botStream.hostStreams.push({
        //     quality: '',//
        //     url: 'https://streamtape.com/e/wJWBgMABM4UWvb/?autostart=true'
        // });
        // $.fn.botStream.hostStreams.push({
        //     quality: '',//
        //     url: 'https://aparat.cam/5wbmbirwm409/NCIS_S17E20_HDTV_x264-SVA.mp4.html'
        // });
        // $.fn.botStream.hostStreams.push({
        //     quality: '',//
        //     url: 'https://voe.sx/evfmj7u64qof'
        // });

        //#endregion

        $.fn.botStream.superstar = {};
        //#region ADD PROVIDERS
        // $.fn.botStream.providers.push(google_com);
        $.fn.botStream.providers.push(srsone_top);
        $.fn.botStream.providers.push(cmovies_fm);
        // $.fn.botStream.providers.push(gomovies_dev);
        // $.fn.botStream.providers.push(fivemovies_to);
        $.fn.botStream.providers.push(fivemovies_cloud);
        // $.fn.botStream.providers.push(watchserieshd_net);
        // $.fn.botStream.providers.push(movies123_net);
        $.fn.botStream.providers.push(vhmovies_net);
        // $.fn.botStream.providers.push(movies123_la);
        // $.fn.botStream.providers.push(ponytok_com);
        // $.fn.botStream.providers.push(kisscartoon_love);
        $.fn.botStream.providers.push(m4ufree_fun);
        // $.fn.botStream.providers.push(seehd_uno);
        $.fn.botStream.providers.push(gogoanime_io);
        // $.fn.botStream.providers.push(gogoanime1_com);
        $.fn.botStream.providers.push(gogoanime_pro);
        $.fn.botStream.providers.push(tinyzone_tv);
        // $.fn.botStream.providers.push(moviesfull123_su);
        // $.fn.botStream.providers.push(ninemovies_yt);
        $.fn.botStream.providers.push(ffmovies_ru);
        $.fn.botStream.providers.push(ww0_0gomovies_org);
        // $.fn.botStream.providers.push(gomovies_band);
        // $.fn.botStream.providers.push(aflamstream_com);
        $.fn.botStream.providers.push(mxplayer_in);
        $.fn.botStream.providers.push(thetamilyogi_com);
        $.fn.botStream.providers.push(khatri_maza_net);
        $.fn.botStream.providers.push(hindilinks4u_to);
        // $.fn.botStream.providers.push(hd15s_com);
        $.fn.botStream.providers.push(moviesub123_io);
        $.fn.botStream.providers.push(free_123movies_com);
        $.fn.botStream.providers.push(lookmovie_ag);
        $.fn.botStream.providers.push(lookmovie_club);
        // $.fn.botStream.providers.push(lookmovie_la);
        // $.fn.botStream.providers.push(moviess123_sc);
        $.fn.botStream.providers.push(pinoymoviess_com);
        $.fn.botStream.providers.push(dramacool_movie);
        $.fn.botStream.providers.push(dailymotion_com);
        $.fn.botStream.providers.push(turkish123_com);
        $.fn.botStream.providers.push(movie0123_net);
        // $.fn.botStream.providers.push(cartoonextra_in);
        // $.fn.botStream.imdbSources.push(api_hdv_fun);
        $.fn.botStream.imdbSources.push(apimdb_net);
        // $.fn.botStream.imdbSources.push(player123_4u_ms);
        $.fn.botStream.imdbSources.push(_2embed_biz);
        $.fn.botStream.imdbSources.push(database_gdriveplayer_me);
        $.fn.botStream.imdbSources.push(gomo_to);
        $.fn.botStream.imdbSources.push(vidsrc_me);
        //#endregion


        $.fn.botStream({
            // movieInfo: {title: 'Fairy Tail', originalTitle: 'フェアリーテイル', year: 2018, show: true, season: 8, episodeNumber: 1, filmImdb: 'tt1528406', episodeImdb: 'tt8747346', episodeNumberSeasons: 278, yearFirstSeason: 2009, language: {name: 'English', code: 'en-AU'}}
            // movieInfo: {
            //     title: 'One Piece',
            //     year: 2019,
            //     show: true,
            //     season: 21,
            //     episodeNumber: 892,
            //     episodeNumberSeasons: 892,
            //     yearFirstSeason: 1999,
            //     filmImdb: 'tt0388629',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     // title: 'Don\'t F**k with Cats: Hunting an Internet Killer',
            //     title: 'One Day at a Time',
            //     year: 2019,
            //     yearFirstSeason: 2017,
            //     show: true,
            //     season: 3,
            //     episodeNumber: 6,
            //     episodeNumberSeasons: 5,
            //     filmImdb: 'tt5339440',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: 'Man like mobeen',
            //     year: 2017,
            //     yearFirstSeason: 2017,
            //     show: true,
            //     season: 1,
            //     episodeNumber: 3,
            //     episodeNumberSeasons: 3,
            //     filmImdb: 'tt7639280',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: 'Shadowhunters',
            //     year: 2019,
            //     yearFirstSeason: 2016,
            //     show: true,
            //     season: 3,
            //     episodeNumber: 1,
            //     episodeNumberSeasons: 5,
            //     filmImdb: 'tt4145054',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: 'Gordon Ramsay&#x27;s 24 Hours to Hell and Back',
            //     year: 2020,
            //     yearFirstSeason: 2018,
            //     show: true,
            //     season: 3,
            //     episodeNumber: 1,
            //     episodeNumberSeasons: 19,
            //     filmImdb: 'tt7768166',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: 'Ninjago: Masters of Spinjitzu',
            //     year: 2016,
            //     yearFirstSeason: 2016,
            //     show: true,
            //     season: 6,
            //     episodeNumber: 3,
            //     episodeNumberSeasons: 3,
            //     filmImdb: 'tt1871731',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: 'Power',
            //     year: 2019,
            //     yearFirstSeason: 2017,
            //     show: true,
            //     season: 6,
            //     episodeNumber: 6,
            //     episodeNumberSeasons: 5,
            //     filmImdb: 'tt3281796',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: 'Money Heist',
            //     year: 2020,
            //     yearFirstSeason: 2017,
            //     show: true,
            //     season: 2,
            //     episodeNumber: 10,
            //     episodeNumberSeasons: 5,
            //     filmImdb: 'tt6468322',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: 'Money Heist',
            //     year: 2020,
            //     yearFirstSeason: 2017,
            //     show: true,
            //     season: 1,
            //     episodeNumber: 6,
            //     episodeNumberSeasons: 6,
            //     filmImdb: 'tt6468322',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: 'Be Water',
            //     year: 2020,
            //     yearFirstSeason: 2020,
            //     show: false,
            //     season: 0,
            //     episodeNumber: 0,
            //     episodeNumberSeasons: 0,
            //     filmImdb: 'tt11394168',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: 'Kevin Hart: I’m a Grown Little Man',
            //     // title: '',
            //     year: 2009,
            //     show: false,
            //     season: 0,
            //     episodeNumber: 0,
            //     filmImdb: 'tt1420554',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: 'Bad Boys for Life',
            //     // title: '',
            //     year: 2020,
            //     show: false,
            //     season: 0,
            //     episodeNumber: 0,
            //     filmImdb: 'tt1502397',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: 'If Loving You is Wrong',
            //     year: 2014,
            //     show: true,
            //     season: 6,
            //     episodeNumber: 10,
            //     filmImdb: 'tt3645318',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: 'ONE PIECE: EPISODE OF EAST BLUE',
            //     // title: '',
            //     year: 2017,
            //     show: false,
            //     season: 0,
            //     episodeNumber: 0,
            //     filmImdb: 'tt11757066',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: 'Naruto Shippuden',
            //     // title: '',
            //     year: 2017,
            //     show: true,
            //     season: 20,
            //     episodeNumber: 10,
            //     episodeNumberSeasons: 415,
            //     filmImdb: 'tt0988824',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: 'Boruto: Naruto Next Generations',
            //     // title: '',
            //     year: 2017,
            //     show: true,
            //     season: 1,
            //     episodeNumber: 25,
            //     episodeNumberSeasons: 25,
            //     filmImdb: 'tt6342474',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: 'Dragon Ball',
            //     // title: '',
            //     year: 1986,
            //     show: true,
            //     season: 10,
            //     episodeNumber: 10,
            //     episodeNumberSeasons: 130,
            //     filmImdb: 'tt0088509',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: 'One-Punch Man',
            //     // title: '',
            //     year: 1986,
            //     show: true,
            //     season: 2,
            //     episodeNumber: 2,
            //     episodeNumberSeasons: 14,
            //     filmImdb: 'tt4508902',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: 'Hunter X Hunter',
            //     year: 2011,
            //     show: true,
            //     season: 2,
            //     episodeNumber: 14,
            //     episodeNumberSeasons: 14,
            //     filmImdb: 'tt2098220',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: 'Slam Dunk',
            //     year: 1993,
            //     show: true,
            //     season: 1,
            //     episodeNumber: 14,
            //     episodeNumberSeasons: 14,
            //     filmImdb: 'tt0965547',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: 'Tom and Jerry The Classic Collection',
            //     year: 1995,
            //     show: true,
            //     season: 1,
            //     episodeNumber: 2,
            //     episodeNumberSeasons: 2,
            //     filmImdb: '',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: 'Baki',
            //     year: 2018,
            //     show: true,
            //     season: 1,
            //     episodeNumber: 2,
            //     episodeNumberSeasons: 2,
            //     filmImdb: 'tt6357658',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: 'Avatar: The Last Airbender',
            //     year: 2005,
            //     show: true,
            //     season: 3,
            //     episodeNumber: 2,
            //     episodeNumberSeasons: 2,
            //     filmImdb: 'tt0417299',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: 'Once Upon a Time... Life',
            //     year: 1987,
            //     show: true,
            //     season: 1,
            //     episodeNumber: 1,
            //     episodeNumberSeasons: 1,
            //     filmImdb: 'tt0284735',
            //     episodeImdb: ''
            // }
            // // movieInfo: {
            //     title: 'Lupin the Third',
            //     // title: '',
            //     year: 2018,
            //     show: true,
            //     season: 5,
            //     episodeNumber: 10,
            //     episodeNumberSeasons: 350,
            //     filmImdb: 'tt0159175',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: 'A Day & A Night',
            //     // title: '',
            //     year: 2020,
            //     show: false,
            //     season: 0,
            //     episodeNumber: 0,
            //     episodeNumberSeasons: 0,
            //     filmImdb: 'tt11051580',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: 'Bhouri',
            //     year: 2017,
            //     show: false,
            //     season: 0,
            //     episodeNumber: 0,
            //     episodeNumberSeasons: 0,
            //     filmImdb: 'tt5978194',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: 'Bigg Boss',
            //     year: 2019,
            //     show: true,
            //     season: 12,
            //     episodeNumber: 12,
            //     episodeNumberSeasons: 12,
            //     filmImdb: 'tt1281973',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: 'Your Honor',
            //     year: 2020,
            //     show: true,
            //     season: 1,
            //     episodeNumber: 5,
            //     episodeNumberSeasons: 5,
            //     filmImdb: 'tt12392470',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: '13 Assassins',
            //     year: 2010,
            //     show: false,
            //     season: 0,
            //     episodeNumber: 0,
            //     episodeNumberSeasons: 0,
            //     filmImdb: 'tt1436045',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: 'Valentino',
            //     year: 2020,
            //     show: true,
            //     season: 1,
            //     episodeNumber: 2,
            //     episodeNumberSeasons: 2,
            //     filmImdb: 'tt11508212',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: 'The Prince',
            //     year: 2020,
            //     show: true,
            //     season: 1,
            //     episodeNumber: 2,
            //     episodeNumberSeasons: 2,
            //     filmImdb: 'tt12228334',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: 'Marvel&#x27;s Daredevil',
            //     year: 2017,
            //     show: true,
            //     season: 3,
            //     episodeNumber: 1,
            //     episodeNumberSeasons: 415,
            //     filmImdb: 'tt3322312',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: 'Dc&#x27;s Legends Of Tomorrow',
            //     year: 2016,
            //     show: true,
            //     season: 5,
            //     episodeNumber: 14,
            //     episodeNumberSeasons: 415,
            //     filmImdb: 'tt4532368',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: 'Neeyum Njanum',
            //     // title: '',
            //     year: 2019,
            //     show: false,
            //     season: 0,
            //     episodeNumber: 0,
            //     filmImdb: 'tt9332680',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: 'First Kill',
            //     year: 2017,
            //     show: false,
            //     season: 0,
            //     episodeNumber: 0,
            //     episodeNumberSeasons: 0,
            //     filmImdb: 'tt5884234',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: 'The bold type',
            //     year: 2017,
            //     show: true,
            //     season: 4,
            //     episodeNumber: 13,
            //     episodeNumberSeasons: 13,
            //     filmImdb: 'tt6116060',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: 'Resurrection: Ertugrul',
            //     year: 2014,
            //     show: true,
            //     season: 1,
            //     episodeNumber: 10,
            //     episodeNumberSeasons: 10,
            //     filmImdb: 'tt4320258',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: 'Digimon Adventure',
            //     year: 2020,
            //     show: true,
            //     season: 1,
            //     episodeNumber: 1,
            //     episodeNumberSeasons: 1,
            //     filmImdb: 'tt11645760',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: 'Four More Shots Please',
            //     year: 2020,
            //     show: true,
            //     season: 2,
            //     episodeNumber: 10,
            //     episodeNumberSeasons: 10,
            //     filmImdb: 'tt8242548',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: 'Mountain Men',
            //     year: 2012,
            //     show: true,
            //     season: 8,
            //     episodeNumber: 5,
            //     episodeNumberSeasons: 5,
            //     filmImdb: 'tt2202488',
            //     episodeImdb: ''
            // }
            // movieInfo: {title: 'Through Night and Day', originalTitle: 'Through Night and Day', year: 2018, show: false, season: 0, episodeNumber: 0, filmImdb: '', episodeImdb: '', episodeNumberSeasons: 0, yearFirstSeason: 0, language: {name: 'English', code: 'en-AU'}}
            // movieInfo: {
            //     title: 'bikeman 2',
            //     year: 2019,
            //     show: false,
            //     season: 0,
            //     episodeNumber: 0,
            //     episodeNumberSeasons: 0,
            //     filmImdb: 'tt11204336',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: 'Kaijudo: Clash of the Duel Masters',
            //     year: 2012,
            //     show: true,
            //     season: 1,
            //     episodeNumber: 2,
            //     episodeNumberSeasons: 2,
            //     filmImdb: 'tt2339980',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: 'Money in the Bank',
            //     year: 2013,
            //     show: false,
            //     season: 0,
            //     episodeNumber: 0,
            //     episodeNumberSeasons: 0,
            //     filmImdb: 'tt2846410',
            //     episodeImdb: ''
            // }
            // movieInfo: {
            //     title: 'Abhay',
            //     year: 2020,
            //     show: true,
            //     season: 2,
            //     episodeNumber: 3,
            //     episodeNumberSeasons: 3,
            //     filmImdb: 'tt9778022',
            //     episodeImdb: ''
            // }
            // movieInfo: {title: 'Air Bud: World Pup', originalTitle: '', year: 2000, show: false, season: 0, episodeNumber: 1, filmImdb: 'tt0161220', episodeImdb: '', episodeNumberSeasons: 27, yearFirstSeason: 2000, language: {name: 'Zulu', code: 'zu'}}
            // movieInfo: {title: 'Scooby-Doo and Scrappy-Doo', originalTitle: '', year: 1979, show: true, season: 1, episodeNumber: 1, filmImdb: 'tt0083475', episodeImdb: '', episodeNumberSeasons: 1, yearFirstSeason: 1979, language: {name: 'Zulu', code: 'zu'}}
            // movieInfo: {title: 'Fresh Off the Boat', originalTitle: '', year: 2019, show: true, season: 2, episodeNumber: 9, filmImdb: 'tt3551096', episodeImdb: '', episodeNumberSeasons: 27, yearFirstSeason: 2015, language: {name: 'Zulu', code: 'zu'}}
            // movieInfo: {title: 'Terror Strike', originalTitle: '', year: 2018, show: false, season: 2, episodeNumber: 9, filmImdb: 'tt7881506', episodeImdb: '', episodeNumberSeasons: 27, yearFirstSeason: 2015, language: {name: 'Zulu', code: 'zu'}}
            // movieInfo: {title: 'My Hero Academia: Heroes Rising', originalTitle: '', year: 2019, show: false, season: 0, episodeNumber: 1, filmImdb: 'tt11107074', episodeImdb: '', episodeNumberSeasons: 27, yearFirstSeason: 2019, language: {name: 'Zulu', code: 'zu'}}
            // movieInfo: {title: 'Teenage Mutant Ninja Turtles', originalTitle: 'Teenage Mutant Ninja Turtles', year: 2012, show: true, season: 1, episodeNumber: 1, filmImdb: 'tt1877889', episodeImdb: '', episodeNumberSeasons: 1, yearFirstSeason: 2012, language: {name: 'English', code: 'en'}}
            // movieInfo: {title: 'Shaggy & Scooby-Doo Get a Clue!', originalTitle: '', year: 2007, show: true, season: 2, episodeNumber: 1, filmImdb: 'tt0850642', episodeImdb: '', episodeNumberSeasons: 1, yearFirstSeason: 2006, language: {name: 'English', code: 'en'}}
            // movieInfo: {title: 'India\'s Most Wanted', originalTitle: '', year: 2019, show: false, season: 2, episodeNumber: 1, filmImdb: 'tt8484942', episodeImdb: '', episodeNumberSeasons: 1, yearFirstSeason: 2006, language: {name: 'English', code: 'en'}}
            // movieInfo: {title: 'Captain Tsubasa J', originalTitle: '', year: 1994, show: true, season: 1, episodeNumber: 1, filmImdb: 'tt0296319', episodeImdb: '', episodeNumberSeasons: 1, yearFirstSeason: 1994, language: {name: 'English', code: 'en'}}
            // movieInfo: {title: 'That Time I Got Reincarnated as a Slime', originalTitle: '', year: 2021, show: true, season: 2, episodeNumber: 1, filmImdb: 'tt9054364', episodeImdb: '', episodeNumberSeasons: 2, yearFirstSeason: 2018, language: {name: 'English', code: 'en'}}
            // movieInfo: {title: 'The Daily Life of the Immortal King', originalTitle: '仙王的日常生活', year: 2020, show: true, season: 1, episodeNumber: 1, filmImdb: '', episodeImdb: '', episodeNumberSeasons: 1, yearFirstSeason: 2020, language: {name: 'English', code: 'en'}}
            // movieInfo: {title: 'Kemono Jihen', originalTitle: '', year: 2021, show: true, season: 1, episodeNumber: 2, filmImdb: 'tt13186542', episodeImdb: '', episodeNumberSeasons: 2, yearFirstSeason: 2021, language: {name: 'English', code: 'en'}}
            // movieInfo: {title: 'Inside Culture with Mary Beard', originalTitle: '', year: 2021, show: true, season: 2, episodeNumber: 1, filmImdb: '', episodeImdb: '', episodeNumberSeasons: 6, yearFirstSeason: 2020, language: {name: 'English', code: 'en'}}
            // movieInfo: {title: 'Mira, Royal Detective', originalTitle: '', year: 2021, show: true, season: 2, episodeNumber: 17, filmImdb: 'tt9357248', episodeImdb: '', episodeNumberSeasons: 17, yearFirstSeason: 2020, language: {name: 'English', code: 'en'}}
            // movieInfo: {title: 'See No Evil', originalTitle: '', year: 2021, show: true, season: 7, episodeNumber: 2, filmImdb: 'tt4189570', episodeImdb: '', episodeNumberSeasons: 7, yearFirstSeason: 2014, language: {name: 'English', code: 'en'}}
            // movieInfo: {title: 'Lucifer', originalTitle: '', year: 2020, show: true, season: 5, episodeNumber: 15, filmImdb: 'tt4052886', episodeImdb: '', episodeNumberSeasons: 7, yearFirstSeason: 2016, language: {name: 'English', code: 'en'}}
            // movieInfo: {title: 'Squid Game', originalTitle: '', year: 2021, show: true, season: 1, episodeNumber: 1, filmImdb: 'tt10919420', episodeImdb: '', episodeNumberSeasons: 7, yearFirstSeason: 2021, language: {name: 'English', code: 'en'}}
            // movieInfo: {title: 'The Deep House', originalTitle: '', year: 2021, show: false, season: 5, episodeNumber: 15, filmImdb: 'tt11686490', episodeImdb: '', episodeNumberSeasons: 7, yearFirstSeason: 2016, language: {name: 'English', code: 'en'}}
            // movieInfo: {title: 'Street Outlaws: No Prep Kings', originalTitle: '', year: 2021, show: true, season: 4, episodeNumber: 15, filmImdb: 'tt9568676', episodeImdb: '', episodeNumberSeasons: 7, yearFirstSeason: 2018, language: {name: 'English', code: 'en'}}
            // movieInfo: {title: 'Goodfellas', originalTitle: '', year: 1990, show: false, season: 4, episodeNumber: 15, filmImdb: 'tt0099685', episodeImdb: '', episodeNumberSeasons: 7, yearFirstSeason: 2018, language: {name: 'English', code: 'en'}}
            // movieInfo: {title: 'Top Gun: Maverick', originalTitle: '', year: 2022, show: false, season: 4, episodeNumber: 15, filmImdb: 'tt1745960', episodeImdb: '', episodeNumberSeasons: 7, yearFirstSeason: 2022, language: {name: 'English', code: 'en'}}
            movieInfo: { title: 'Minions: The Rise of Gru', originalTitle: '', year: 2022, show: false, season: 4, episodeNumber: 15, filmImdb: 'tt5113044', episodeImdb: '', episodeNumberSeasons: 7, yearFirstSeason: 2022, language: { name: 'English', code: 'en' } }
        });
    }
}, 50);
//https://www.uwatchfree.ch/
//https://at123movies.com/123movies/
//https://niter.123movies.online/?sort=latest-added
//http://movie25.123movies.online/?sort=latest-added
//https://worldfree4u.desi/home
//https://www.wco.tv/kaijudo-clash-of-the-duel-masters-season-2-episode-26-the-evolution-will-not-be-televised
//https://www.hindimovies.to/movie/brahms-the-boy-ii-2020-in-hindi/
//https://www.yupptv.com/tvshows/bhajana-batch
//https://www2.series9.ac/film/24-season-9-live-another-day-qpy/watching.html?ep=9
//https://www.zee5.com/global/search
//https://5movies.run/all/movie/all/all/2/1/
//https://afdah.info/watch-movies/68377-godzilla-vs-kong-2021/#
// https://lookmovie.la/watch-series/deceit-season-1-full-episodes-123-free-online-j6xvyrmjoq/xeoj404?WatchNow=1
//https://lookmovie.club/mortal-kombat-legends-battle-of-the-realms-2021/
